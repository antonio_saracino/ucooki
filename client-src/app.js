var cloudinary = cloudinary.Cloudinary.new({
  cloud_name: "ucooki",
  upload_preset: 'viskw4dd',
  api_key: 'EGPN3Jps_7eCc9huvTsYBwwX_RY'
});

var app = angular.module("ucookiApp", [
  'ngRaven',
  'ngAnimate',
  'ui.bootstrap',
  'ui.router',
  'lbServices',
  'ngResource',
  'google.places',
  'ngFileUpload',
  'cloudinary',
  'ngImgCrop',
  'ngToast',
  'angular.filter',
  'smoothScroll',
  'sticky',
  'infinite-scroll',
  'countTo',
  'uiGmapgoogle-maps',
  'frapontillo.bootstrap-switch',
  'angular-loading-bar',
  'ui.select',
  'ngSanitize',
  'timer'
]);

app.config(['$stateProvider', '$urlRouterProvider', 'ngToastProvider', '$locationProvider', 'uiGmapGoogleMapApiProvider', 'uiSelectConfig',
  function ($stateProvider, $urlRouterProvider, ngToastProvider, $locationProvider, uiGmapGoogleMapApiProvider, uiSelectConfig) {

    var defaultTitle = "Ordina piatti artigianali da asporto - Ucooki";
    var defaultDescription = "Gusta ricette artigianali da asporto preparati dai tuoi vicini a pranzo e a cena";
    var defaultImage = "https://ucooki.com/assets/img/logo_600x600.png";

    uiGmapGoogleMapApiProvider.configure({
      key: 'AIzaSyCojJTQZj9E-zXDosZbztgWDwKEZSLnwuA',
      libraries: 'places,geometry',
      language: "it"
    });

    ngToastProvider.configure({
      animation: 'slide' // 'fade'
    });

    uiSelectConfig.theme = 'bootstrap';

    // gestisco il login con passport
    $urlRouterProvider.when('/passport', ['$state', '$location', '$window', 'AuthService', 'TrackerService',
      function ($state, $location, $window, AuthService, TrackerService) {
        var returnUrl = "/";
        var isNew = false;
        var email;

        if ($location.search()) {
          if ($location.search().return) {
            returnUrl = decodeURIComponent($location.search().return);
          }
          if ($location.search().new_user) {
            isNew = ($location.search().new_user === 'true');
          }
          if ($location.search().email) {
            email = decodeURIComponent($location.search().email);
          }

          if ($location.search().access_token && $location.search().user_id) {
            AuthService.passportLogged($location.search().access_token, $location.search().user_id)
              .then(function () {
                // sono stato loggato tramite passport
                $location.search({}).path(returnUrl).replace();

                if (isNew) {
                  // è una nuova iscrizione
                  TrackerService.completeRegistration("facebook", email);
                }
              });

          } else {
            $location.search({}).path(returnUrl).replace();
          }
        }
      }
    ]);

    $urlRouterProvider.when('', '/');
    $urlRouterProvider.otherwise("/");

    $stateProvider
      .state("home", {
        url: "/",
        templateUrl: "components/dynamic-pages/home/home.html",
        data: {
          cssClassnames: 'home',
          title: "Home - " + defaultTitle,
          description: defaultDescription,
          image: defaultImage
        }
      })
      .state("login", {
        url: "/pages/login?return",
        templateUrl: "components/dynamic-pages/login/login.html",
        data: {
          cssClassnames: 'login no-parallax',
          title: "Login - " + defaultTitle,
          description: defaultDescription,
          image: defaultImage
        }
      })
      .state("resetPassword", {
        url: "/pages/reset-password",
        templateUrl: "components/dynamic-pages/reset-password/reset-password.html",
        data: {
          cssClassnames: 'reset-password no-parallax',
          title: defaultTitle,
          description: defaultDescription,
          image: defaultImage
        }
      })
      .state("register", {
        url: "/pages/register",
        templateUrl: "components/dynamic-pages/register/register.html",
        data: {
          cssClassnames: 'register no-parallax',
          title: "Registrazione - " + defaultTitle,
          description: defaultDescription,
          image: defaultImage
        }
      })
      .state("shop", {
        url: '/pages/product/:productId',
        templateUrl: "components/dynamic-pages/shop/shop.html",
        data: {
          cssClassnames: 'shop'
        }
      })
      .state("dashboard", {
        url: '/pages/dashboard',
        templateUrl: "components/dynamic-pages/dashboard/dashboard.html",
        controller: "DashboardCtrl",
        data: {
          cssClassnames: 'dashboard no-parallax',
          title: defaultTitle,
          description: defaultDescription,
          image: defaultImage
        }
      })
      .state("dashboard.personal-info", {
        url: '/personal-info',
        templateUrl: "components/dynamic-pages/dashboard/personal-info/personal-info.html",
        data: {
          cssClassnames: 'dashboard no-parallax',
          title: "Dashboard - Informazioni personali - " + defaultTitle,
          description: defaultDescription,
          image: defaultImage
        }
      })
      .state("dashboard.account-settings", {
        url: '/account-settings',
        templateUrl: "components/dynamic-pages/dashboard/account-settings/account-settings.html",
        data: {
          cssClassnames: 'dashboard no-parallax',
          title: defaultTitle,
          description: defaultDescription,
          image: defaultImage
        }
      })
      .state("dashboard.products", {
        url: '/products',
        templateUrl: "components/dynamic-pages/dashboard/products/products.html",
        data: {
          cssClassnames: 'dashboard no-parallax',
          title: "Dashboard - Gestione Piatti - " + defaultTitle,
          description: defaultDescription,
          image: defaultImage
        }
      })
      .state("dashboard.product-new", {
        url: '/products/new',
        templateUrl: "components/dynamic-pages/dashboard/products/product-detail.html",
        data: {
          cssClassnames: 'dashboard no-parallax',
          title: "Dashboard - Nuovo Piatto - " + defaultTitle,
          description: defaultDescription,
          image: defaultImage
        }
      })
      .state("dashboard.product-detail", {
        url: '/products/:productId',
        templateUrl: "components/dynamic-pages/dashboard/products/product-detail.html",
        data: {
          cssClassnames: 'dashboard no-parallax',
          title: "Dashboard - Gestione Piatto - " + defaultTitle,
          description: defaultDescription,
          image: defaultImage
        }
      })
      .state("dashboard.orders", {
        url: '/orders',
        templateUrl: "components/dynamic-pages/dashboard/orders/orders.html",
        controller: "OrdersCtrl",
        data: {
          cssClassnames: 'dashboard no-parallax',
          title: "Dashboard - Gestione Ordini - " + defaultTitle,
          description: defaultDescription,
          image: defaultImage
        }
      })
      .state("dashboard.admin-products", {
        url: '/admin/products',
        templateUrl: "components/dynamic-pages/dashboard/admin/products/admin-products.html",
        controller: "AdminProductsCtrl",
        data: {
          cssClassnames: 'dashboard no-parallax',
          title: defaultTitle,
          description: defaultDescription,
          image: defaultImage
        }
      })
      .state("dashboard.admin-cooks", {
        url: '/admin/cooks',
        templateUrl: "components/dynamic-pages/dashboard/admin/cooks/admin-cooks.html",
        controller: "AdminCooksCtrl",
        data: {
          cssClassnames: 'dashboard no-parallax',
          title: defaultTitle,
          description: defaultDescription,
          image: defaultImage
        }
      })
      .state("checkout", {
        url: '/pages/checkout',
        templateUrl: "components/dynamic-pages/checkout/checkout.html",
        data: {
          title: "Carrello - " + defaultTitle,
          description: defaultDescription,
          image: defaultImage
        }
      })
      .state("customer", {
        url: '/pages/customer/:customerId',
        templateUrl: "components/dynamic-pages/pages/customer/customer.html"
      })
      .state("contacts", {
        url: '/pages/contacts',
        templateUrl: "components/dynamic-pages/contacts/contacts.html",
        data: {
          cssClassnames: 'no-parallax',
          title: "Contatti - " + defaultTitle,
          description: defaultDescription,
          image: defaultImage
        }
      })
      // .state("subscriber", {
      //   url: '/pages/subscriber',
      //   templateUrl: "components/dynamic-pages/subscriber/subscriber.html",
      //   data: {
      //     cssClassnames: 'no-parallax',
      //     title: defaultTitle,
      //     description: defaultDescription
      //   }
      // })
      // pagine statiche
      .state("404", {
        url: '/pages/404',
        templateUrl: "components/static-pages/404.html",
        data: {
          cssClassnames: 'static no-parallax',
          title: defaultTitle,
          description: defaultDescription,
          image: defaultImage
        }
      })
      .state("terms", {
        url: '/pages/terms-and-conditions',
        templateUrl: "components/static-pages/terms-and-conditions.html",
        data: {
          cssClassnames: 'static no-parallax',
          title: "Termini e condizioni - " + defaultTitle,
          description: defaultDescription,
          image: defaultImage
        }
      })
      .state("privacy", {
        url: '/pages/privacy',
        templateUrl: "components/static-pages/privacy.html",
        data: {
          cssClassnames: 'static no-parallax',
          title: "Privacy - " + defaultTitle,
          description: defaultDescription,
          image: defaultImage
        }
      })
      .state("team", {
        url: '/pages/team',
        templateUrl: "components/static-pages/team.html",
        data: {
          cssClassnames: 'static',
          title: "Il team di Ucooki - " + defaultTitle,
          description: defaultDescription,
          image: defaultImage
        }
      })
      .state("faq", {
        url: '/pages/faq',
        templateUrl: "components/static-pages/faq.html",
        data: {
          cssClassnames: 'static',
          title: "FAQ - " + defaultTitle,
          description: defaultDescription,
          image: defaultImage
        }
      })
      .state("mission", {
        url: '/pages/mission',
        templateUrl: "components/static-pages/mission.html",
        data: {
          cssClassnames: 'static',
          title: "Mission - " + defaultTitle,
          description: defaultDescription,
          image: defaultImage
        }
      })
      .state("how-it-works", {
        url: '/pages/how-it-works',
        templateUrl: "components/static-pages/how-it-works.html",
        data: {
          cssClassnames: 'static',
          title: "Come funziona - " + defaultTitle,
          description: defaultDescription,
          image: defaultImage
        }
      })
      .state("become-a-cooker", {
        url: '/pages/become-a-cooker',
        templateUrl: "components/static-pages/become-a-cooker.html",
        data: {
          cssClassnames: 'static',
          title: "Diventa un Cuoco - " + defaultTitle,
          description: defaultDescription,
          image: defaultImage
        }
      })
      .state("quality-control", {
        url: '/pages/quality-control',
        templateUrl: "components/static-pages/quality-control.html",
        data: {
          cssClassnames: 'static',
          title: "Qualità e controllo - " + defaultTitle,
          description: defaultDescription,
          image: defaultImage
        }
      })
      .state("community", {
        url: '/pages/scopri-la-community-della-cucina-tradizionale-da-asporto',
        templateUrl: "components/static-pages/scopri-la-community.html",
        data: {
          cssClassnames: 'static',
          title: "Gruppo Facebook sulla cucina tradizionale - " + defaultTitle,
          description: defaultDescription,
          image: defaultImage
        }
      });


    // use the HTML5 History API
    $locationProvider.html5Mode(true);
  }
]);

// run blocks
app.run(['$rootScope', '$location', '$state', '$timeout', '$window', 'TrackerService', 'ModalService', 'AuthService', 'UtilService', 'MetadataService',
  function ($rootScope, $location, $state, $timeout, $window, TrackerService, ModalService, AuthService, UtilService, MetadataService) {

    var LOCAL_STORAGE_HIDE_MODAL_REG = "registration-modal-showed";

    $rootScope.metadata = {};

    $rootScope.$on('$stateChangeSuccess',
      function (event, toState, toParams, fromState, fromParams) {
        // invio dati ai tracker
        TrackerService.pageView($location.absUrl());

        // aggiorno le informazioni della live chat
        if ($window.$zopim) {
          $window.$zopim(function () {
            //
            $zopim.livechat.button.show();
            $zopim.livechat.button.setOffsetVerticalMobile(45);

            if ($location.absUrl().indexOf("dashboard") >= 0) {
              // sono nella dashboard
              $zopim.livechat.button.setOffsetVerticalMobile(5);
            }
          });
        }

        // aggiorno titolo, descrizione e immagine
        var title = null;
        var description = null;
        var image = null;

        if (angular.isDefined(toState.data)) {
          title = angular.isDefined(toState.data.title) ? toState.data.title : null;
          description = angular.isDefined(toState.data.description) ? toState.data.description : null;
          image = angular.isDefined(toState.data.image) ? toState.data.image : null;
        }

        if (title) {
          MetadataService.setTitle(title);
        }

        if (description) {
          MetadataService.setDescription(description);
        }

        if (image) {
          MetadataService.setImage(image);
        }

        MetadataService.setCanonical($location.absUrl());

      });

    $rootScope.$on("titlePageChanged", function (event, title) {
      if ($window.$zopim) {
        $window.$zopim(function () {
          // update visitor's path with specific page url and title
          $zopim.livechat.sendVisitorPath({
            url: $location.absUrl(),
            title: title
          });
        });
      }

      $timeout(function() {
        console.log("title updated!");
      }, 0);
    });

    // $rootScope.mouseExit = function () {
    //   $rootScope.openRegistrationModal();
    // }

    $rootScope.$on("userLoginDone", function (event, user) {
      try {
        localStorage.setItem(LOCAL_STORAGE_HIDE_MODAL_REG, true);
      } catch (e) {
        // ok;
      }

      if ($window.$zopim && user) {
        $window.$zopim(function () {
          $zopim.livechat.set({
            name: user.name + " " + user.lastname,
            email: user.email
          });
        });
      }
    });

    $rootScope.openRegistrationModal = function () {

      var registrationShowed = localStorage.getItem(LOCAL_STORAGE_HIDE_MODAL_REG);

      if (!UtilService.isLocalStorageSupported()) {
        // Evitiamo di generare la visualizzazione della finestra modale
        registrationShowed = true;
      }

      if (registrationShowed) {
        return;
      }

      if (!validePath()) {
        return;
      }

      localStorage.setItem(LOCAL_STORAGE_HIDE_MODAL_REG, true);

      if (AuthService.isAuthenticated()) {
        return;
      }

      var modalInstance = ModalService.show({
        templateUrl: 'components/modals/registration/registration-modal.html',
        backdrop: true
      });

      modalInstance.then(function (result) { }, function () { });
    }

    /*********************************************/

    var validePath = function () {
      var login = $state.href("login");
      var register = $state.href("register");

      if ($location.path().indexOf(login) > -1 || $location.path().indexOf(register) > -1) {
        return false;
      }

      return true;

    }

    var loadDefaultMapOptions = function () {
      $rootScope.map = {
        zoom: 15,
        options: {
          scrollwheel: false
        },
        radius: 100,
        stroke: {
          color: '#FF6868',
          weight: 2,
          opacity: 1
        },
        fill: {
          color: '#FF6868',
          opacity: 0.5
        },
        geodesic: false, // optional: defaults to false
        draggable: false, // optional: defaults to false
        clickable: false, // optional: defaults to true
        editable: false, // optional: defaults to false
        visible: true, // optional: defaults to true
        control: {}
      };
    }

    var init = function () {

      loadDefaultMapOptions();

      if (!UtilService.isLocalStorageSupported()) {
        alert("Attenzione, per utilizzare il servizio occorre uscire dalla navigazione anonima. Grazie :)");
      }

      //@todo da sistemare
      if (!AuthService.isAuthenticated()) {
        AuthService.user();
      }

      // if (UtilService.mobileCheck()) {
      //   $timeout(function () {
      //     $rootScope.openRegistrationModal();
      //   }, 30000)
      // }
    }

    init();

  }]);
