angular.module('ucookiApp')
  .factory('AddressService', ['$rootScope', function($rootScope) {

      var s = {};

      s.localStorageKey = "address";

      s.save = function(address) {

				if (!address) {
          s.clear();
        } else {
          localStorage.setItem(s.localStorageKey, JSON.stringify(address));
        }
      };

      s.get = function() {
        var address = localStorage.getItem(s.localStorageKey);
				address = JSON.parse(address);

				return address;
      };

      s.clear = function() {
        localStorage.removeItem(s.localStorageKey);
        console.log("Address removed from localStorage");
      };

      return s;
    }
  ]);
