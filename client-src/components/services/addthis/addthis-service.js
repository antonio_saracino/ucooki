angular.module('ucookiApp')
  .factory('AddthisService', ['$location', '$window', function($location, $window) {

    var s = {};

    s.updateAddthis = function(title, description) {

      if (!$window.addthis_share) {
        console.log("Addthis not exists");
        return;
      } 

      $window.addthis_share.url = $location.absUrl();
			if (title) {
				$window.addthis_share.title = "Ucooki - " + title;
			}

			if (description) {
				$window.addthis_share.description = description;
			}

    }

    return s;
  }]);
