angular.module('ucookiApp')
    .factory('PicturesService', ['$http', 'Upload', function ($http, $upload) {
        var s = {};

        s.uploadUrl = "https://api.cloudinary.com/v1_1/" + cloudinary.config().cloud_name + "/upload";
        s.uploadPreset = cloudinary.config().upload_preset;

        /**
         * uploapPictures
         * @param {string}        folder    cartella cloudinary
         * @param {string}        tags    tags di cloudinary
         * @param  {array}      images    immagini da caricare:
         *                              Object: {
     *                              	dataOriginal: 	FileReference,
     *                              	data: 	        string base64 cropped image,
     *                              	principal: 	    boolean
     *                              }
         * @param  {function}   success callback - parametri: {array} immagini caricate
         * @param  {function}   error   callback - parametri: {array} immagini caricate, {Object} errore
         * @param  (function)   done    callback
         * @return
         */
        s.uploadPictures = function (folder, tags, images, success, error, done) {
            if (!tags || !folder) {
                if (error) error([], {error: "tags and/or folder are not defined"});
                return;
            }

            if (images.length <= 0) {
                if (done) done();
                return;
            }

            var imageManaged = 0;
            var uploadedPictures = [];

            for (var index in images) {
                var picture = images[index];

                picture.progress = 0;
                picture.uploadCropped = $upload.upload({
                    url: s.uploadUrl,
                    fields: {
                        upload_preset: s.uploadPreset,
                        tags: tags + ", cropped",
                        folder: folder
                    },
                    file: picture.data
                }).progress(function (e) {
                    picture.progress = Math.round(((e.loaded * 100.0) / e.total) / 2);
                }).success(function (dataCropped, status, headers, config) {

                    var saved = {
                        id: dataCropped.public_id,
                        url: dataCropped.secure_url,
                        principal: picture.principal,
                        status: 1
                    };

                    uploadedPictures.push(saved);
                    if (success) success(saved);

                }).error(function (data, status, headers, config) {
                    picture.result = data;
                    if (error) error(picture, {error: data});
                }).finally(function () {
                    imageManaged++;

                    if (imageManaged === images.length) {
                        // ho finito
                        if (done) done(uploadedPictures);
                    }
                });
            }
        };

        /**
         * Rimuove le immagini da Cloudinary
         * @param  {array} pictures
         * @return
         */
        s.deletePictures = function (pictures) {
            for (var index in pictures) {
                var picture = pictures[index];
                $http.delete("/cloudinary/pictures/" + picture.id);
                if (picture.idOriginal) {
                    $http.delete("/cloudinary/pictures/" + picture.idOriginal);
                }
            }
        };

        return s;
    }]);
