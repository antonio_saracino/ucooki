angular.module('ucookiApp')
  .factory('AuthService', ['$http', '$rootScope', '$window', '$q', 'Customer', 'LoopBackAuth', 'UtilService', 'CartService', 'TrackerService', 'ErrorTrackerService',
    function ($http, $rootScope, $window, $q, Customer, LoopBackAuth, UtilService, CartService, TrackerService, ErrorTrackerService) {

      var s = {};

      s.user = function () {

        var getMe = function () {

          var id = LoopBackAuth.currentUserId;
          var token = LoopBackAuth.accessTokenId;

          return $http({
            method: 'GET',
            url: 'api/Customers/' + id + '/me?access_token=' + token
          });
        }

        var promise = $q(function (resolve, reject) {

          if (Customer.isAuthenticated()) {
            getMe().then(function (response) {
              userLoggedin(response.data.result);
              resolve(response.data.result);
              TrackerService.login(response.data.result);
            }, function (response) {
              userLoggedout();
              reject(response);
            })
          } else {
            reject("User not autheticated");
          }
        });


        if ($rootScope.user) {
          return $q(function (resolve, reject) {
            resolve($rootScope.user);
          })
        }

        return promise;

      };


      s.login = function (credentials, alertList, success, error) {

        function printError(err) {
          if (err) {
            if (err.data && err.data.error && err.data.error.message) {
              ErrorTrackerService.error(err.data.error.message, err.data);
            } else {
              ErrorTrackerService.error("login error", err);
            }

          }
          var e = err.data.error;

          if (alertList) {
            if (e.code == "LOGIN_FAILED_EMAIL_NOT_VERIFIED") {
              UtilService.addAlert(alertList, {
                msg: "La mail non è stata verificata. Controlla la posta",
                type: 'danger',
                id: "login_error"
              });
            } else {
              UtilService.addAlert(alertList, {
                msg: "Username e/o password errati",
                type: 'danger',
                id: "login_error"
              });
            }
          }
        }

        function errorFn(err) {
          userLoggedout();
          printError(err);
          if (error) error(err.data.error);
        }

        return Customer.login({
          include: ''
        },
          credentials,
          function (data) {

            s.user().then(function (user) {
              if (success) success(user);
            }).catch(function (err) {
              errorFn(err);
            });

          },
          function (err) {
            errorFn(err);
          }
        );
      };

      s.passportLogged = function (token, userId, user) {
        LoopBackAuth.setUser(token, userId, user);
        LoopBackAuth.rememberMe = true;
        LoopBackAuth.save();
        return s.user();
      }

      s.isAuthenticated = function () {
        return !!$rootScope.isAuthenticated;
      }

      s.logout = function () {
        Customer.logout(function () { });
        logout();
      };


      s.test = function (value) {
        var deferred = $q.defer();

        setTimeout(function () {

          if (value > 0) {
            console.log("ok");
            deferred.resolve("resolved");
          } else {
            console.log("ko");
            deferred.reject("rejected");
          }

        }, 1000);

        return deferred.promise;
      }

      /******************************************************/

      var logout = function () {
        $window.location.href = "/";
        LoopBackAuth.clearUser();
        LoopBackAuth.clearStorage();
        CartService.clear();
        userLoggedout();
        ErrorTrackerService.userLogout();
      }

      var userLoggedin = function (user) {
        $rootScope.user = user;
        $rootScope.isAuthenticated = true;
        $rootScope.$emit("userLoginDone", user);
        if (user) {
          ErrorTrackerService.userLogin(user.email, user.id, user.name, user.lastname);
        }

      }

      var userLoggedout = function () {
        delete $rootScope.user;
        $rootScope.isAuthenticated = false;
        $rootScope.$emit("userLogoutDone");
      }

      return s;
    }
  ]);
