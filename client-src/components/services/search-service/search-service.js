angular.module("ucookiApp").factory('SearchService', [function() {

  var s = {};


  s.localStorageKey = "search";

  s.save = function(address, lat, lng) {
    var search = {
      address: address,
      lat: lat,
      lng: lng
    }

    localStorage.setItem(s.localStorageKey, JSON.stringify(search));
  }

  s.get = function() {
    var search = localStorage.getItem(s.localStorageKey);

    if (search) {
      if (/^[\],:{}\s]*$/.test(search.replace(/\\["\\\/bfnrtu]/g, '@')
          .replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']')
          .replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {

        search = JSON.parse(search);
      }
    } else {
				search = {};
		}
    return search;
  }

  return s;

}])
