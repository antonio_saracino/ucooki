angular.module('ucookiApp')
  .factory('GoogleMapService', ['$q', 'uiGmapGoogleMapApi', function($q, uiGmapGoogleMapApi) {

    var s = {};

    s.BOLOGNA_COORD = {
      lat: 44.494887,
      lng: 11.342616
    }
    s.MILANO_COORD = {
      lat: 45.465454,
      lng: 9.186515999999983
    }

    s.decodeAddress = function(place) {

      return $q(function(resolve, reject) {

        uiGmapGoogleMapApi.then(function() {

          var geocoder = new google.maps.Geocoder();

          if (!place) {
            reject({
              reason: 'empty value',
              errorCode: 1
            });
            return;
          }

          if (place.lat && place.lng) {
            geocoder.geocode({
              'location': place
            }, function(results, status) {

              try {

                var result = handler(results, status);
                resolve(result);

              } catch (error) {
                reject(error);
              }


            })
            return;
          }

          if (typeof place.name === "string") {
            place = place.name;
          }


          if (typeof place === "string") {
            // indirizzo diretto
            geocoder.geocode({
              'address': place
            }, function(results, status) {

              try {

                var result = handler(results, status);
                resolve(result);

              } catch (error) {
                reject(error);
              }

            });
          }
        })


      })
    }

    s.getLocation = function(place) {

      return $q(function(resolve, reject) {

        uiGmapGoogleMapApi.then(function() {

          var geocoder = new google.maps.Geocoder();

          if (!place) {
            reject({
              reason: 'empty value',
              errorCode: 1
            });
            return;
          }

          if (typeof place.name === "string") {
            place = place.name;
          }


          if (typeof place === "string") {
            // indirizzo diretto
            geocoder.geocode({
              'address': place
            }, function(results, status) {


              try {

                var result = handler(results, status);
                resolve(result);

              } catch (error) {
                reject(error);
              }

            });

            return;
          }

          // controllo se è un oggetto place
          else if (place && place.geometry) {

            resolve({
              lat: place.geometry.location.lat(),
              lng: place.geometry.location.lng(),
              address: place.formatted_address
            });
          } else {
            reject({
              reason: 'valid result',
              errorCode: 2
            });
          }
        });


      })
    }

    s.getAddressNumber = function(place, cb) {

      uiGmapGoogleMapApi.then(function() {

        if (!place) {
          cb(null);
          return;
        }

        if (!place.address_components) {
          var geocoder = new google.maps.Geocoder();

          geocoder.geocode({
            'address': place
          }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {

              var p = results[0];
              var streetNumber = getAddress(p);
              cb(streetNumber);
              return;

            } else {
              cb(null);
              return;
            }
          });


        } else {
          var streetNumber = getAddress(place);
          cb(streetNumber);
          return;
        }

      });

    }

    s.distanceBetween = function(from, to) {

      return $q(function(resolve, reject) {

        uiGmapGoogleMapApi.then(function() {
          var fromPoint = new google.maps.LatLng(from.lat, from.lng);
          var toPoint = new google.maps.LatLng(to.lat, to.lng);

          resolve(google.maps.geometry.spherical.computeDistanceBetween(fromPoint, toPoint));

        });

      });

    }

    /*****************************************************/


    function getAddress(place) {
      return getComponent(place, 'street_number');
    }

    function getComponent(place, component, short) {
      if (!place.address_components) {
        return null;
      }

      for (var index in place.address_components) {
        var addressComponent = place.address_components[index];
        for (var index in addressComponent.types) {
          var type = addressComponent.types[index];
          if (type === component) {
            if (short) {
              return addressComponent.short_name;
            }
            return addressComponent.long_name;
          }
        }
      }

      return null;
    }


    function handler(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {

        var p = results[0];

        if (p && p.address_components) {

          return {
            formatted_address: p.formatted_address,
            number: getComponent(p, 'street_number'),
            city: getComponent(p, 'locality'),
            province: getComponent(p, 'administrative_area_level_2', true),
            region: getComponent(p, 'administrative_area_level_1'),
            coordinates: {
              lat: p.geometry.location.lat(),
              lng: p.geometry.location.lng()
            }
          };
        } else {
          throw new Error({
            reason: 'no valid result',
            errorCode: 4
          });
        };

      } else {
        throw new Error({
          reason: 'unknown address',
          errorCode: 3
        });
      }
    }

    return s;
  }]);
