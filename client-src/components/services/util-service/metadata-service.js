angular.module('ucookiApp')
  .factory('MetadataService', ['$rootScope', '$window', function ($rootScope, $window) {
    var service = {};

    service.setTitle = function (title) {
      $rootScope.metadata.title = title;
      $rootScope.$emit("titlePageChanged", title);
    };

    service.setDescription = function (description) {
      if (description && description.length > 160) {
        description = description.substring(0, 155).trim() + "...";
      }
      $rootScope.metadata.description = description;
      $rootScope.$emit("descriptionPageChanged", description);
    };

    service.setImage = function (image) {
      $rootScope.metadata.image = image;
      $rootScope.$emit("imagePageChanged", image);
    };

    service.setCanonical = function (url) {
      $rootScope.metadata.canonical = url;
      $rootScope.$emit("canonicalPageChanged", url);
    };

    return service;
  }]);
