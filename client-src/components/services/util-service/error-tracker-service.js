angular.module('ucookiApp')
	.factory('ErrorTrackerService', ['Raven', function (Raven) {

		var service = {};

		service.userLogin = function (email, id, name, lastname) {
			Raven.setUserContext({
				email: email,
				id: id,
				name: name,
				lastname: lastname
			});
		};

		service.userLogout = function () {
			// pulisco i dati
			Raven.setUserContext();
		};

		// level is one of 'info', 'warning', or 'error'
		service.log = function (msg, data, tags, level, disableconsolelog) {

			msg = msg || "-No message-";
			level = level || "info";
			data = data || {};
			tags = tags || {};

			Raven.captureMessage(msg, {
				level: level,
				extra: data,
				tags: tags
			});

			if (!disableconsolelog) {
				if ('info' == level) {
					console.log(msg, data);
				} else if ('warning' == level) {
					console.warn(msg, data);
				} else {
					console.error(msg, data);
				}
			}
		};

		service.error = function (msg, data, tags, disableconsolelog) {
			return service.log(msg, data, tags, 'error', disableconsolelog);
		};

		service.warn = function (msg, data, tags, disableconsolelog) {
			return service.log(msg, data, tags, 'warning', disableconsolelog);
		};

		service.exception = function (error, data, tags, disableconsolelog) {
			error = error || new Error("Error undefined");
			data = data || {};
			tags = tags || {};

			Raven.captureException(error);
			if (!disableconsolelog) {
				console.error("Exception catched: ", error);
			}

		};

		return service;

	}]);
