angular.module('ucookiApp')
  .factory('TrackerService', ['$location', '$window', function ($location, $window) {
    var s = {};
    var isToEsclude = function () {
      //      console.log("domain: ", $location.host());
      if ($location.host().indexOf("dev") > -1
        || $location.host().indexOf("staging") > -1
        || $location.host().indexOf("localhost") > -1) {
        // siamo in fase di test, disabilito l'invio dei dati
        return true;
      }

      return false;
    };


    /*********** flow di registrazione ******************/

    s.completeRegistration = function (provider, email) {

      if (isToEsclude()) {
        // siamo in fase di test, disabilito l'invio dei dati
        return;
      }

      if ($window.fbq) {
        $window.fbq('track', "CompleteRegistration", {
          status: provider,
          email: email
        });
        //        console.log("CompleteRegistration sent to tracker. Values: " ,provider);
      }
      if ($window.mixpanel) {
        $window.mixpanel.alias(email);
        $window.mixpanel.register({
          'Email': email
        });
        $window.mixpanel.track("registration", {
          'Email': email,
          'Provider': provider
        });
        $window.mixpanel.people.set({
          'Provider': provider
        });
        //        console.log("sign up sent to tracker. Values: ", provider, email);
      }
      if ($window.ga) {
        ga('send', 'event', 'registration', 'complete registration', 'email', email);
      }
    };

    /*********** flow di login ******************/

    s.login = function (user) {

      if (isToEsclude()) {
        // siamo in fase di test, disabilito l'invio dei dati
        return;
      }

      if (!user) {
        return;
      }

      if ($window.mixpanel) {
        $window.mixpanel.identify(user.email);
        $window.mixpanel.register({
          'Email': user.email
        });
        mixpanel.people.set({
          "$email": user.email,
          "$name": user.name,
          "$lastname": user.lastname,
          "$last_login": new Date(),
          "gender": user.gender,
          "provider": user.provider
        });
        //        console.log("Login sent. Values: ", user);
      }

      if ($window.ga) {
        ga('send', 'event', 'login', 'login completed', 'email', user.email);
      }
    }

    /*********** flow di navigazione ******************/

    s.pageView = function (url) {

      if (isToEsclude()) {
        // siamo in fase di test, disabilito l'invio dei dati
        return;
      }

      if ($window.fbq) {
        $window.fbq('track', "ViewContent", {
          content_name: url
        });
      }
      if ($window.mixpanel) {
        $window.mixpanel.track("ViewPage", {
          "Url": url
        });
        //        console.log("PageView sent to tracker. Values: ", url);
      }
      if ($window.ga) {
        ga('send', 'event', 'navigation', 'page viewed', 'url', url);
      }
    }

    s.productView = function (url, title, productor) {

      if (isToEsclude()) {
        // siamo in fase di test, disabilito l'invio dei dati
        return;
      }

      if ($window.fbq) {
        $window.fbq('trackCustom', "ViewProduct", {
          content_name: url,
          title: title,
          productor: productor.name + " " + productor.lastname,
          productor_id: productor.id

        });
      }
      if ($window.mixpanel) {
        $window.mixpanel.track("ViewProduct", {
          "Url": url,
          "Title": title,
          "Productor": productor.name + " " + productor.lastname,
          "Productor_id": productor.id
        });
        //        console.log("ViewContent sent to tracker. Values: ", url, title, productor);
      }
      if ($window.ga) {
        ga('send', 'event', 'navigation', 'product viewed', 'url', url);
      }
    }

    /*********** flow di ricerca ******************/

    s.search = function (address) {

      if (isToEsclude()) {
        // siamo in fase di test, disabilito l'invio dei dati
        return;
      }

      if ($window.fbq) {
        $window.fbq('track', "Search", {
          search_string: address.formattedAddress
        });
        // console.log("Search sent to tracker. Values: " + address);
      }
      if ($window.mixpanel) {
        $window.mixpanel.track("Search", address);
        //        console.log("Search sent to tracker. Values: ", address);
      }
      if ($window.ga) {
        ga('send', 'event', 'search', 'first searching', 'address', address);
      }
    }

    s.searchMore = function (address, skip, limit, day, meal) {

      if (isToEsclude()) {
        // siamo in fase di test, disabilito l'invio dei dati
        return;
      }

      if ($window.fbq) {
        $window.fbq('trackCustom', "SearchMore", {
          address: address,
          skip: skip,
          limit: limit,
          day: day,
          meal: meal
        });
        //        console.log("SearchMore sent to tracker. Values: ", address, skip, limit, day, meal);
      }
      if ($window.mixpanel) {
        $window.mixpanel.track("SearchMore", {
          "Address": address,
          "Skip": skip,
          "Limit": limit,
          "Day": day,
          "Meal": meal
        });
        //        console.log("SearchMore sent to tracker. Values: ", address, skip, limit, day, meal);
      }
      if ($window.ga) {
        ga('send', 'event', 'search', 'search more', 'address', address);
      }

    }

    s.searchByDay = function (address, skip, limit, day, meal) {

      if (isToEsclude()) {
        // siamo in fase di test, disabilito l'invio dei dati
        return;
      }

      if ($window.fbq) {
        $window.fbq('trackCustom', "SearchByDay", {
          address: address,
          skip: skip,
          limit: limit,
          day: day,
          meal: meal
        });
        //        console.log("searchByDay sent to tracker. Values: ", address, skip, limit, day, meal);
      }
      if ($window.mixpanel) {
        $window.mixpanel.track("SearchByDay", {
          "Address": address,
          "Skip": skip,
          "Limit": limit,
          "Day": day,
          "Meal": meal
        });
        //        console.log("searchByDay sent to tracker. Values: ", address, skip, limit, day, meal);
      }
      if ($window.ga) {
        ga('send', 'event', 'search', 'search by day', 'address', address);
      }
    }

    /*********** flow di acquisto ******************/

    s.startAddToCart = function (id, value) {

      if (isToEsclude()) {
        // siamo in fase di test, disabilito l'invio dei dati
        return;
      }

      if ($window.fbq) {
        $window.fbq('trackCustom', "StartAddToCart", {
          value: value,
          currency: "EUR",
          content_ids: [id]
        });
        //        console.log("StartAddToCart sent to tracker. Values: ", id, value);
      }
      if ($window.mixpanel) {
        $window.mixpanel.track("StartAddToCart", {
          "Value": value,
          "Currency": "EUR",
          "Content_ids": [id]
        });
        //        console.log("StartAddToCart sent to tracker. Values: ", id, value);
      }
      if ($window.ga) {
        ga('send', 'event', 'ordering', 'start add to cart', 'product id', id);
      }
    }

    s.addToCart = function (id, value) {

      if (isToEsclude()) {
        // siamo in fase di test, disabilito l'invio dei dati
        return;
      }

      if ($window.fbq) {
        $window.fbq('track', "AddToCart", {
          value: value,
          currency: "EUR",
          content_ids: [id]
        });
        //        console.log("AddToCart sent to tracker. Values: ", id, value);
      }
      if ($window.mixpanel) {
        $window.mixpanel.track("AddToCart", {
          "Value": value,
          "Currency": "EUR",
          "Content_ids": [id]
        });
        //        console.log("AddToCart sent to tracker. Values: ", id, value);
      }
      if ($window.ga) {
        ga('send', 'event', 'ordering', 'added to cart', 'product id', id);
      }
    }

    s.initiateCheckout = function (ids, value, numItems) {

      if (isToEsclude()) {
        // siamo in fase di test, disabilito l'invio dei dati
        return;
      }

      if ($window.fbq) {
        $window.fbq('track', "InitiateCheckout", {
          value: value,
          currency: "EUR",
          content_ids: ids,
          num_items: numItems
        });
        //        console.log("InitiateCheckout sent to tracker. Values: ", ids, value, numItems);
      }
      if ($window.mixpanel) {
        $window.mixpanel.track("InitiateCheckout", {
          Value: value,
          Currency: "EUR",
          Content_ids: ids,
          Num_items: numItems
        });
        //        console.log("InitiateCheckout sent to tracker. Values: ", ids, value, numItems);
      }
      if ($window.ga) {
        ga('send', 'event', 'ordering', 'initiate checkout', 'product ids', ids);
      }
    }

    s.purchase = function (ids, value, numItems, orderId, order) {

      if (isToEsclude()) {
        // siamo in fase di test, disabilito l'invio dei dati
        return;
      }

      if ($window.fbq) {
        $window.fbq('track', "Purchase", {
          value: value,
          currency: "EUR",
          content_ids: ids,
          num_items: numItems,
          order_id: orderId
        });
        //        console.log("Purchase sent to tracker. Values: ", ids, value, numItems, orderId);
      }
      if ($window.mixpanel) {

        var coupon = ""
        if (order && order._coupon) {
          coupon = order._coupon.name
        }

        $window.mixpanel.track("Purchase", {
          Value: value,
          Currency: "EUR",
          Content_ids: ids,
          Num_items: numItems,
          Order_id: orderId,
          Coupon: coupon
        });
        $window.mixpanel.people.track_charge(value);
        //        console.log("Purchase sent to tracker. Values: ", ids, value, numItems, orderId);
      }
      if ($window.ga) {
        ga('send', 'event', 'ordering', 'purchase', 'product ids', ids);
      }
    };

    /*********** flow di cancellazione ******************/

    s.deleteMe = function (user) {

      if (isToEsclude()) {
        // siamo in fase di test, disabilito l'invio dei dati
        return;
      }

      if ($window.mixpanel) {
        $window.mixpanel.track("Cancellation");
      }

      if ($window.ga) {
        ga('send', 'event', 'cancellation', 'deleteMe', 'email', user.email);
      }
    };

    return s;
  }]);
