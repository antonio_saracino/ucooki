angular.module('ucookiApp')
  .factory('CartService', ['$rootScope', 'Product', 'Order', 'Customer',
    function($rootScope, Product, Order, Customer) {

      var s = {};

      s.localStorageKey = "cart";

      s.save = function(cart) {

        // aggiorno i prezzi
        for (var index in cart.orders) {

          var order = cart.orders[index];
          order.total = 0;
          order.delivery_price = 0;

          for (var itemIndex in order.items) {
            var item = order.items[itemIndex];
            item.price = item.product.price * item.quantity;
            order.total += item.price;
            if (order.delivery && item.product.delivery_price > order.delivery_price) {
              // considero il prezzo di trasporto più alto fra tutti
              // gli item
              order.delivery_price = item.product.delivery_price;
            }
          }

          order.total += order.delivery_price;
					order.total_grass = order.total;
        }

        if (cart.orders.length <= 0) {
          s.clear();
        } else {
          localStorage.setItem(s.localStorageKey, JSON.stringify(cart));
        }

        $rootScope.$emit("cart-changed", cart);
      };

      s.remove = function(cart, orderIndex, itemIndex) {
        // rimuove un item
        if (!cart) {
          cart = s.get();
        }
        var order = cart.orders[orderIndex];
        if (order) {
          order.items.splice(itemIndex, 1);
        } else {
          console.error("Remove error. unable to find order, index: ", orderIndex);
        }

        if (order.items.length <= 0) {
          cart.orders.splice(orderIndex, 1);
        }

        s.save(cart);
      }

      s.get = function() {
        var cart = localStorage.getItem(s.localStorageKey);

        if (cart) {
          if (/^[\],:{}\s]*$/.test(cart.replace(/\\["\\\/bfnrtu]/g, '@')
              .replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']')
              .replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {

            cart = JSON.parse(cart);
            var ordersToDelete = [];

            for (var index in cart.orders) {
              var order = cart.orders[index];

              if (order.date) {
                order.date = new Date(order.date);
              }

              // dopo un ora elimino l'ordine
              var limitDate = new Date(order.date);
              limitDate.setHours(order.date.getHours() + 1);

              if (new Date() > limitDate) {
                // cancello l'ordine
                console.log("order deleted because too old: ", order);
                ordersToDelete = order;
                continue;
              }

              if (order.delivery_date) {
                order.delivery_date = new Date(order.delivery_date);
              }

              angular.forEach(order.items, function(item, index) {
                item.delivery_date = new Date(item.delivery_date);
              })

            }

            for (var index in ordersToDelete) {
              var order = ordersToDelete[index];
              cart.orders.splice(cart.orders.indexOf(order), 1);
            }


            return cart;
          }
        }

        return {
          orders: []
        }
      };

      s.clear = function() {
        console.log("cart empty");
        localStorage.removeItem(s.localStorageKey);
        $rootScope.$emit("cart-changed");
      };

      return s;
    }
  ]);
