angular.module('ucookiApp')
  .factory('OrderService', function() {

    var s = {};

    s.needFeedback = function(order, cooker) {
      if (!cooker) {
        if (order.status == 3) {
          for (var i in order.items) {
            var item = order.items[i];
            if (!item.rating) {
              // ho un item senza rating
              return true;
            }
          }
        }
      } else {
        if (order.status == 3) {
          if (!order.rating) {
            // ho un order senza voto
            return true;
          }
        }
      }
      return false;
    }

    return s;

  });
