angular.module('ucookiApp')
  .filter('timeRange', function() {
    return function(input) {

      if (!input || (input.endDinner == -1 && input.endLunch == -1)) {
        return null;
      }

      switch (input.day) {
        case 1:
          return "lun";
        case 2:
          return "mar";
        case 3:
          return "mer";
        case 4:
          return "gio";
        case 5:
          return "ven";
        case 6:
          return "sab";
        case 7:
				case 0:
          return "dom";

      }

			return null;

    }
  })
