angular.module('ucookiApp')
  .filter('distance', function() {
    return function(input) {
      if (input >= 1000) {
        return (input / 1000).toFixed(1) + ' km';
      } else {
        return parseInt(input) + ' m';
      }
    }
  })
