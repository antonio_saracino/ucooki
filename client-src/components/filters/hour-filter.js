angular.module('ucookiApp')
.filter('hour', function() {
	return function(input, emptyString) {
		if (!input || input === -1) {

			return emptyString || "";
		}
		input += "";
		return input.substring(0,2) + ":" + input.substring(2,4);
	}
})
