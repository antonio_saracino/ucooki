angular.module('ucookiApp')
    .filter('productStatus', function () {
        return function (status) {

            switch (status) {
                case 0:
                    return "in attesa";
                case 1:
                    return "accettato";
                case 2:
                    return "rifiutato";
            }

            return "";

        }
    })
