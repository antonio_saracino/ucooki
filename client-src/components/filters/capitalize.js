angular.module('ucookiApp')
  .filter('capitalize', function() {
    return function(input) {

			if (!input || input == "") {
				return "";
			}

			var result = "";
			var words = input.toLowerCase().split(' ');

			words.forEach(function(word) {

				word = word.trim();
				if (word.length > 0) {
					result += word.substring(0,1).toUpperCase()+word.substring(1) + " ";
				}

			})

			result = result.trim();
			return result;
    }
  })
