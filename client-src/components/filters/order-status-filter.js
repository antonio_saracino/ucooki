angular.module('ucookiApp')
  .filter('orderStatus', function() {
    return function(status) {

			switch (status) {
				case -1:
					return "errore";
				case 0:
					return "in attesa";
        case 1:
          return "confermato";
        case 2:
          return "pagato";
        case 3:
          return "completato";
        case 4:
          return "rifiutato";
        case 5:
          return "cancellato";
        case 6:
          return "annullato";

      }

			return "";

    }
  })
