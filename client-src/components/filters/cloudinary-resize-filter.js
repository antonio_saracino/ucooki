angular.module('ucookiApp')
  .filter("cloudinaryResize", ["endsWithFilter", function(endsWithFilter) {
    return function(input, options) {

			var url = input.replace("/image/upload/","/image/upload/" + options + "/").replace(/['"]+/g, '');
			if (endsWithFilter(url, ".png")) {
				url = url.replace(".png", ".jpg")
			}
			return url;

    };
  }]);
