angular.module('ucookiApp')
  .filter('cut', function() {
    /**
     * Cut a string,
     * @param  {[type]}     value    - input value
     * @param  {[boolean]}  wordwise - if true, cut only by words bounds
     * @param  {[integer]}  max      - max length of the text, cut to this number of chars
     * @param  {[string]}   tail     - add this string to the input string if the string was cut (default: '...')
     */
    return function(value, wordwise, max, tail) {
      if (!value) return '';

      max = parseInt(max, 10);
      if (!max) return value;
      if (value.length <= max) return value;

      value = value.substr(0, max);
      if (wordwise) {
        var lastspace = value.lastIndexOf(' ');
        if (lastspace != -1) {
          value = value.substr(0, lastspace);
        }
      }

      return value + (tail || ' …');
    };
  });
