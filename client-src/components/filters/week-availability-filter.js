angular.module('ucookiApp')
  .filter('weekAvailability', ['hourFilter', function(hourFilter) {
    return function(input) {

      if (!input || (input.endDinner == -1 && input.endLunch == -1)) {
        return null;
      }

			var lunchTime, dinnerTime;

			if (input.endLunch && input.endLunch != -1) {
				lunchTime = hourFilter(input.startLunch) + " - " + hourFilter(input.endLunch);
			}
			if (input.endDinner && input.endDinner != -1) {
				dinnerTime = hourFilter(input.startDinner) + " - " + hourFilter(input.endDinner);
			}

			if (dinnerTime && lunchTime) {
				return lunchTime + " / " + dinnerTime;
			} else {
				return lunchTime ? lunchTime : dinnerTime;
			}

    }
  }])
