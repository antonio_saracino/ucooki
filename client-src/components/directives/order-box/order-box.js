angular.module('ucookiApp')
  .directive('orderBox', ['$timeout', 'OrderService', function($timeout, OrderService) {

    /**
     * stato ordine:
     * -1: errore,
     *  0: attesa,
     *  1: confermato,
     *  2: pagato,
     *  3: completato,
     *  4: rifiutato,
     *  5: cancellato,
     *  6: annullato
     */

    var controller = ['$scope', '$filter', '$state', function($scope, $filter, $state) {

      $scope.getMessage = function() {

        var orderStatusFilter = $filter('orderStatus');
        var status = orderStatusFilter($scope.order.status);


        if ($scope.order.status == 1 && new Date($scope.order.delivery_date).getTime() < new Date().getTime()) {
          status = "evaso";
        }

        $scope.important = false;

        if (OrderService.needFeedback($scope.order, $scope.cooker)) {
          $scope.important = true;
          return 'lascia un commento';
        }

        if ($scope.order.status == 0) {
          $scope.important = true;
        }

        return status;

      };

      $scope.getIcon = function() {

        switch ($scope.order.status) {
          case -1:
          case 4:
          case 5:
          case 6:
            return "glyphicon-exclamation-sign";
          case 0:
          case 2:
            return "glyphicon-time";
          case 1:
            return "glyphicon-ok";
          case 3:
            if (OrderService.needFeedback($scope.order, $scope.cooker)) {
              return "glyphicon-comment";
            }
        }


        return "glyphicon-ok";

      }

      $scope.getOrderClass = function(status) {

        switch (status) {
          case -1:
          case 4:
          case 5:
          case 6:
            return 'panel-danger';
          case 0:
            return 'panel-info';
          case 1:
            return 'panel-success';
          case 2:
          case 3:
            if (OrderService.needFeedback($scope.order, $scope.cooker)) {
              return 'panel-primary';
            }
        }

        return "panel-default";
      }

      $scope.getItemUrl = function(item) {
        return "https://www.facebook.com/sharer/sharer.php?u=" + $state.href("shop", {
          productId: item.product.id
        }, {
          absolute: true
        });
      }

      //***************************************************//

      function init() {}

      init();
    }];


    return {
      restrict: 'E',
      scope: {
        order: '=',
        rejectButton: '@',
        confirmButton: '@',
        cancelButton: '@',
        cooker: '=',
        buttonPressed: '&onButtonPressed',
        feedbackCooker: '&onCookerFeedback',
        feedbackEater: '&onEaterFeedback',
      },
      templateUrl: '/components/directives/order-box/order-box.html',
      link: function(scope, element, attrs) {

        function updateCss() {
          var clazz = scope.getOrderClass(scope.order.status);
          var header = $(element).find("div.header");
          header.removeClass().addClass("" + clazz + " panel header");
        }

        var hasRegistered = false;
        scope.$watch(function() {
          if (hasRegistered) return;
          hasRegistered = true;
          scope.$$postDigest(function() {
            hasRegistered = false;
            updateCss();
          });
        });


      },
      controller: controller

    }
  }]);
