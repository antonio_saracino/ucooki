angular.module('ucookiApp')
  .directive('productorBox', function() {
    return {
      restrict: 'E',
      scope: {
        productor: '=',
        image: '@',
        link: '@'
      },
      templateUrl: '/components/directives/productor-box/productor-box.html',
			link: function(scope, element, attrs) {

				var url = scope.image;
				if (!url || url=="") {
					url = "assets/img/cooker.svg";
				}

				element.find(".img-circle-container").css("background-image", "url(" + url + ")");

			}

    }
  })
