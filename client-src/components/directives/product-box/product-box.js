angular.module('ucookiApp')
  .directive('productBox', ["$timeout", function ($timeout) {
    return {
      restrict: 'E',
      scope: {
        product: '=',
        cook: '=',
        cookImage: '@',
        image: '@',
        link: '@',
        cookLink: '@',
        detail: '@', //high, medium, low -- default low
        type: '@', // tile, item, item_admin -- default tile
        productChange: '&onProductChange',
        statusChange: '&onStatusChange'
      },
      templateUrl: '/components/directives/product-box/product-box.html',
      link: function (scope, element, attrs) {

        scope.statusChange_aux = function (value, orig) {
          scope.statusChange({product: scope.product, value: value, orig: parseInt(orig)});
        }

        if (angular.isUndefined(scope.type)) {
          scope.type = "tile";
        }

        $timeout(function () {

          var url = scope.image;
          if (!url || url == "") {
            url = "assets/img/empty_photo.svg";
          }

          element.find(".thumb_strip").css("background-image", "url(" + url + ")");

          var cookUrl = "assets/img/cooker.svg";
          if (scope.cook && scope.cook._picture && scope.cook._picture.url) {
            cookUrl = scope.cook._picture.url;
          }
          element.find(".img-circle-container").css("background-image", "url(" + cookUrl + ")");


        });

      }

    }
  }])
