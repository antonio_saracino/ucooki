angular.module('ucookiApp')
  .directive('spinner', function() {
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {

        scope.$on('$stateChangeStart', function() {
					show();
        });

				scope.$on('showSpinner', function() {
					show();
				})

        scope.$on('$stateChangeError', function() {
					hide();
        });

        scope.$on('$stateChangeSuccess', function() {
					hide();
        });

				scope.$on('hideSpinner', function() {
					hide();
        });


        function hide() {
//					console.log("evento ricevuto: hide");
          $('#status').fadeOut(); // will first fade out the loading animation
          $('#preloader').delay(250).fadeOut('slow');
        }

				function show() {
//					console.log("evento ricevuto: show");
					$('#status').fadeIn(0); // will first fade out the loading animation
          $('#preloader').fadeIn(0);
				}
      }
    }
  })
