angular.module('ucookiApp')
  .directive('uRatingBox', function() {
    return {
      restrict: 'E',
      scope: {
        rating: '=rRating',
        avatar: '@rAvatar',
				name: "@rName",
        link: '&rLink'
      },
      templateUrl: '/components/directives/rating-box/rating-box.html'
    }
  })
