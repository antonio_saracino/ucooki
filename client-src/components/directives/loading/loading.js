angular.module('ucookiApp')
  .directive('loading', ['$timeout', function($timeout) {
    return {
      restrict: 'E',
      scope: {
        loading: "=isLoading"
      },
      templateUrl: '/components/directives/loading/loading.html'
    }
  }]);
