angular.module('ucookiApp')
  .controller('FeedbackModalCtrl', [
    '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {

      $scope.data = {}

      $scope.modalOptions = {
        closeButtonText: 'Annulla',
        actionButtonText: 'Conferma',
        headerText: 'Lascia un Feedback',
        bodyTemplate: '/components/modals/feedback/feedback-modal.html',

        ok: function(result) {

          if (!$scope.data.rate) {
            return;
          }
          $uibModalInstance.close($scope.data);
        },

        close: function(result) {
          $uibModalInstance.dismiss('cancel');
        }
      };

      /***********************************************************************/

    }
  ]);
