angular.module('ucookiApp')
  .controller('AddAddressModalCtrl', ['$rootScope','$scope', '$uibModalInstance', '$http', 'AddressService',
    function($rootScope, $scope, $uibModalInstance, $http, AddressService) {

      $scope.data = {}

      $scope.modalOptions = {
        closeButtonText: 'Annulla',
        actionButtonText: 'Conferma',
        headerText: 'Aggiunta Indirizzo',
        bodyTemplate: '/components/modals/add-address/add-address-modal.html',

        ok: function(result) {

          if (!$scope.data.address || !$scope.data.streetNumber) {
            return;
          }

          AddressService.save($scope.data);
          $uibModalInstance.close($scope.data);
        },
        close: function(result) {
          $uibModalInstance.dismiss('cancel');
        }
      };

      $scope.getLocation = function(val) {
        return $http.get('//maps.googleapis.com/maps/api/geocode/json', {
          params: {
            address: val,
            sensor: false,
            components: "country:it"
          }
        }).then(function(response) {

          return response.data.results.map(function(item) {
            return item.formatted_address;
          });
        });
      };

      /***********************************************************************/

      var init = function() {
        if ($rootScope.search && $rootScope.search.address) {
          $scope.data = {
            address: $rootScope.search.address,
            streetNumber: null,
            note: null
          }
        } else {
          var address = AddressService.get();
          if (address) {
            $scope.data = address;
          }
        }
      }

      init();

    }
  ]);
