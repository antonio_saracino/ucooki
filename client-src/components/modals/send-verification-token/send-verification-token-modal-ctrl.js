angular.module('ucookiApp')
  .controller('SendVerificationTokenModalCtrl', [
    '$scope', '$uibModalInstance',
    function($scope, $uibModalInstance) {

      $scope.data = {
        email: ""
      };

      $scope.modalOptions = {
        closeButtonText: 'Annulla',
        actionButtonText: 'Conferma',
        headerText: 'Invia email',
        bodyTemplate: '/components/modals/send-verification-token/send-verification-token-modal.html',

        ok: function(result) {

          if (!$scope.data.email) {
            return;
          }

          $uibModalInstance.close($scope.data.email);
        },
        close: function(result) {
          $uibModalInstance.dismiss('cancel');
        }
      };

    }
  ]);
