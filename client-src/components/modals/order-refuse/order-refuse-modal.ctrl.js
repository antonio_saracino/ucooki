angular.module('ucookiApp')
  .controller('OrderRefuseModalCtrl', [
    '$scope', '$uibModalInstance', function($scope, $uibModalInstance) {

      $scope.data = {}

      $scope.modalOptions = {
        closeButtonText: 'Annulla',
        actionButtonText: 'Conferma',
        headerText: 'Gestione Ordine',
        bodyTemplate: '/components/modals/order-refuse/order-refuse-modal.html',

        ok: function(result) {
          $uibModalInstance.close($scope.data);
        },
        close: function(result) {
          $uibModalInstance.dismiss('cancel');
        }
      };

      /***********************************************************************/

    }
  ]);
