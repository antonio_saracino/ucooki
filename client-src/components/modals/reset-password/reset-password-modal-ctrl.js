angular.module('ucookiApp')
  .controller('ResetPasswordModalCtrl', [
    '$scope', '$uibModalInstance',
    function($scope, $uibModalInstance) {

      $scope.data = {
        email: ""
      }

      $scope.modalOptions = {
        closeButtonText: 'Annulla',
        actionButtonText: 'Conferma',
        headerText: 'Reset Password',
        bodyTemplate: '/components/modals/reset-password/reset-password-modal.html',

        ok: function(result) {

          if (!$scope.data.email) {
            return;
          }

          $uibModalInstance.close($scope.data.email);
        },
        close: function(result) {
          $uibModalInstance.dismiss('cancel');
        }
      };

    }
  ]);
