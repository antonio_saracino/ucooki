angular.module('ucookiApp')
  .controller('OrderFormDialogCtrl', ['$rootScope', '$scope', '$uibModalInstance', '$filter', 'CartService', 'UtilService', 'product', 'alerts', 'GoogleMapService',
    function ($rootScope, $scope, $uibModalInstance, $filter, CartService, UtilService, product, alerts, GoogleMapService) {

      var MAX_DAYS = 14;

      $scope.alerts = alerts;
      $scope.product = product;
      $scope.item = {
        price: 0,
        delivery: false,
        delivery_date: null,
        quantity: $scope.product.min_portion,
        product_id: $scope.product.id,
        note: "",
        product: product
      };

      $scope.getTooltip = function (type) {

        if (type == "delivery") {
          if ($scope.product.delivery) {
            var distance = $filter('distance')($scope.product.productor.productor_info.max_distance);
            return "Chiedi al cuoco di consegnartelo a domicilio (distanza massima " + distance + ")";
          }
          return "Il cuoco non offre il servizio di consegna a domicilio";
        }
      }

      $scope.cart = CartService.get();

      $scope.data = {
        meal: '0'
      }

      $scope.datePickers = {
        deliveryOpened: false
      };

      $scope.dateOptions = {
        startingDay: 1,
        class: 'order-datepicker'
      };

      $scope.modalOptions = {
        closeButtonText: 'Annulla',
        actionButtonText: 'Conferma',
        headerText: 'Dettagli Ordine',
        bodyTemplate: '/components/modals/order-form/order-form-modal.html',

        ok: function (result) {

          if (!validateItem($scope.item)) {
            return;
          }
          $uibModalInstance.close($scope.item);
        },
        close: function (result) {
          $uibModalInstance.dismiss('cancel');
        }
      };
      $scope.minDate = new Date();
      $scope.maxDate = new Date($scope.minDate).setDate($scope.minDate.getDate() + MAX_DAYS);
      $scope.timeSlots = [];
      $scope.noticeCalendar = []; // definisce una lista di oggetti formati dal giorno di produzione e giorno di preavviso necessario per quella prenotazione per il pranzo e la cena (almeno uno dei due)

      $scope.showDeliveryDatePicker = function () {
        $scope.datePickers.deliveryOpened = true;
      };

      $scope.calculateDisabledDate = function (date, mode) {
        var dayOfWeek = date.getDay();
        var timeRange = $scope.product._timeRanges[italianizeDayOfWeek(dayOfWeek) - 1];

        // not available this date
        if (timeRange.startLunch <= -1 && timeRange.startDinner <= -1) {
          return true;
        }

        if (!timeRange.startLunch && !timeRange.startDinner) {
          return true;
        }

        // console.log(timeRange);
        if ($scope.isLunchAvailable(date)) {
          return false;
        }

        if ($scope.isDinnerAvailable(date)) {
          return false;
        }

        return true;
      };

      $scope.clickOnDelivery = function () {

      }

      $scope.getDayClass = function (date, mode) {
        if (mode === 'day') {

          var dayOfWeek = date.getDay();
          var timeRange = $scope.product._timeRanges[italianizeDayOfWeek(dayOfWeek) - 1];

          // not available this date
          if (timeRange.startLunch <= -1 && timeRange.startDinner <= -1) {
            return '';
          }

          if (!timeRange.startLunch && !timeRange.startDinner) {
            return '';
          }

          // console.log(timeRange);
          if ($scope.isLunchAvailable(date) || $scope.isDinnerAvailable(date)) {
            return 'selectable';
          }

          return '';
        }
      };

      $scope.mealSelected = function () {
        var timeRange = $scope.product._timeRanges[italianizeDayOfWeek($scope.item.delivery_date.getDay()) - 1];
        var now = new Date();

        var productionDay = new Date($scope.item.delivery_date);
        var startDay = new Date(productionDay.getTime());
        var endDay = new Date(productionDay.getTime());

        if ($scope.data.meal === 'lunch') {

          startDay.setHours(getHour(timeRange.startLunch), getMinute(timeRange.startLunch));
          endDay.setHours(getHour(timeRange.endLunch), getMinute(timeRange.endLunch));
          $scope.timeSlots = createSlotList(startDay, endDay);

        } else { // dinner

          startDay.setHours(getHour(timeRange.startDinner), getMinute(timeRange.startDinner));
          endDay.setHours(getHour(timeRange.endDinner), getMinute(timeRange.endDinner));
          $scope.timeSlots = createSlotList(startDay, endDay);
        }

        // imposto di default il primo che trovo
        if ($scope.timeSlots.length > 0) {
          $scope.slotSelected($scope.timeSlots[0]);
        }

      };

      $scope.dateSelected = function (date) {

        // resetto l'item
        $scope.data.meal = '0';
      }

      $scope.slotSelected = function (slot) {
        $scope.item.delivery_date.setHours(getHour(slot), getMinute(slot));

      }

      $scope.getCurrentTimeRange = function () {
        return date2TimeRange($scope.item.delivery_date);
      }

      $scope.isLunchAvailable = function (date) {
        var now = new Date();
        var notice = getNoticePerDate(date);
        if (!notice || !notice.lunch) {
          return false;
        }
        return now <= notice.lunch.noticeDay;
      };

      $scope.isDinnerAvailable = function (date) {
        var now = new Date();
        var notice = getNoticePerDate(date);
        if (!notice || !notice.dinner) {
          return false;
        }
        return now <= notice.dinner.noticeDay;
      };

      $scope.closeAlert = function (index) {
        return UtilService.closeAlert($scope.alerts, index);
      }

      $scope.checkDeliveryDistance = function () {

        if ($rootScope.search && $rootScope.search.filters && $rootScope.search.filters.lat && $rootScope.search.filters.lng) {
          GoogleMapService.distanceBetween($rootScope.search.filters, $scope.product.productor.kitchen_location).then(function (distance) {
            if (distance > $scope.product.productor.productor_info.max_distance) {
              UtilService.addAlert($scope.alerts, {
                type: 'danger',
                msg: 'Attenzione, l\'indirizzo che hai indicato in fase di ricerca sembra essere troppo lontano, il cuoco potrebbe rifiutare la richiesta.'
              });
            }

          });

        }


      }

      //******************************* Funzioni private **********************************//

      var validateItem = function (item) {
        return $scope.data.meal != '0';
      }

      var createNoticeCalendar = function () {

        var now = new Date();

        for (var index = 0; index < MAX_DAYS; index++) {
          var timeRange = $scope.product._timeRanges[index % 7];
          var week = index < 7 ? 1 : Math.ceil(index / 6);
          var notice = {}; // sarà costituito da due oggetti (almeno uno dei due): lunch e dinner che a loro volta conterranno giorno di produzione e giorno di preavviso
          if (!timeRange) {
            continue;
          }

          if (!timeRange.endLunch && !timeRange.endDinner) {
            continue;
          }

          if (timeRange.endLunch == -1 && timeRange.endDinner == -1) {
            continue;
          }

          var productionDay = getNextDayOFWeek(now, timeRange.day, week);

          // aggiorno l'orario all'ultima fascia disponibile per il pranzo
          if (timeRange.endLunch && timeRange.endLunch != -1) {
            // abbiamo la fasce del pranzo
            var lunch = {};
            var lastHour = getHour(timeRange.endLunch);
            var lastMinute = getMinute(timeRange.endLunch);
            var lunchProductionDay = new Date(productionDay.getTime());
            lunchProductionDay.setHours(lastHour, lastMinute, 0, 0);
            // sottraggo le ore di preavviso all'ultima ora utile per la consegna
            var noticeDay = new Date(lunchProductionDay.getTime());
            noticeDay.setHours(lunchProductionDay.getHours() - $scope.product.notice_time);
            lunch.productionDay = lunchProductionDay;
            lunch.noticeDay = noticeDay;
            notice.lunch = lunch;
          }

          // aggiorno l'orario all'ultima fascia disponibile per la cena
          if (timeRange.endDinner && timeRange.endDinner != -1) {
            // abbiamo la fasce della cena
            var dinner = {};
            var lastHour = getHour(timeRange.endDinner);
            var lastMinute = getMinute(timeRange.endDinner);
            var dinnerProductionDay = new Date(productionDay.getTime());
            dinnerProductionDay.setHours(lastHour, lastMinute, 0, 0);
            // sottraggo le ore di preavviso all'ultima ora utile per la consegna
            var noticeDay = new Date(dinnerProductionDay.getTime());
            noticeDay.setHours(dinnerProductionDay.getHours() - $scope.product.notice_time);
            dinner.productionDay = dinnerProductionDay;
            dinner.noticeDay = noticeDay;
            notice.dinner = dinner;
          }

          $scope.noticeCalendar.push(notice);
        }
      }

      var getNoticePerDate = function (date) {

        if (!date) {
          return null;
        }

        for (var index = 0; index < $scope.noticeCalendar.length; index++) {
          var notice = $scope.noticeCalendar[index];
          if (notice.lunch && notice.lunch.productionDay.getDate() == date.getDate()) {
            return notice;
          }

          if (notice.dinner && notice.dinner.productionDay.getDate() == date.getDate()) {
            return notice;
          }
        }

        return null;
      }

      /**
       * Restituisce il prossimo giorno della settimana oppure oggi se corrisponde al giorno desiderato.
       * DayOfWeek è un valore che va da 0 a 7, dove 0 e 7 sono domenica e 1 è lunedì.
       * Week se presente indica quante settimane andare avanti, se nullo viene considerata una settimana
       */
      var getNextDayOFWeek = function (date, dayOfWeek, week) {

        var week = week || 1;

        if (week == 1 && date.getDay() == dayOfWeek) {
          // se sto cercando all'interno di una settimana ed oggi corrisponde al giorno che sto cercando
          // allora va bene "oggi"
          return date;
        }
        var resultDate = new Date(date.getTime());
        if (week && week > 1) {
          resultDate.setDate(resultDate.getDate() + (7 * (week - 1))); // passo alla settimana successiva
        }
        resultDate.setDate(resultDate.getDate() + ((7 + dayOfWeek - date.getDay()) % 7));
        return resultDate;
      }

      var getHour = function (timeRange) {
        if (timeRange && timeRange != -1) {
          timeRange += "";
          return parseInt(timeRange.substr(0, 2));
        }
        return null;
      }

      var getMinute = function (timeRange) {
        if (timeRange && timeRange != -1) {
          timeRange += "";
          return parseInt(timeRange.substr(2, 4));
        }
        return null;
      }

      var convertTimeIntoMinutes = function (hours, minutes) {
        var total = 0;
        if (hours) {
          total += hours * 60;
        }

        if (minutes) {
          total += minutes
        }
        return total;
      }

      /**
       * Crea l'elenco delle fasce orarie disponibili considerando la data attuale.
       * StartDay rappresenta la data iniziale della fascia di produzione, mentre endDay rappresenta
       * quella di fine
       */
      var createSlotList = function (startDay, endDay) {
        var slotList = [];
        var now = new Date();
        if (!startDay || !endDay) {
          return slotList;
        }

        var currentDay = startDay;
        while (currentDay <= endDay) {
          var noticeDay = new Date(currentDay);
          noticeDay.setHours(noticeDay.getHours() - $scope.product.notice_time);
          if (now <= noticeDay) {
            // quindi facciamo in tempo a fare l'ordinazione per questo slot
            slotList.push(date2TimeRange(currentDay));
          }
          // passiamo avanti di mezz'ora
          currentDay.setMinutes(currentDay.getMinutes() + 30);
        }

        return slotList;
      }

      var date2TimeRange = function (date) {
        var timeRange = "";
        var m = "" + date.getMinutes();
        var h = "" + date.getHours();
        if (m == "0") {
          m = "00";
        }

        timeRange = h + m;

        return timeRange;
      }

      var italianizeDayOfWeek = function (dayOfWeek) {
        if (dayOfWeek === 0) {
          return 7;
        } else {
          return dayOfWeek;
        }
      };

      var start = function () {
        $scope.product._timeRanges.sort(function (a, b) {
          if (a.day < b.day) {
            return -1;
          } else {
            return 1;
          }
        });

        createNoticeCalendar();
      };

      start();

    }
  ]);
