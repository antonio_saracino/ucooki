angular.module('ucookiApp').controller('CropperModalCtrl', [
    '$scope', '$uibModalInstance', 'image',
    function($scope, $uibModalInstance, file) {
        $scope.data = {};
        $scope.data.image;
        $scope.data.croppedImage = null;


        $scope.modalOptions = {
            closeButtonText: 'Annulla',
            actionButtonText: 'Conferma Foto',
            headerText: 'Ritaglia Foto',
            bodyTemplate: '/components/modals/cropper-modal/cropper-modal.html',
            windowTopClass: 'large-modal'
        };

        $scope.modalOptions.ok = function() {
            var result = {};
            result.imageCropped = $scope.data.croppedImage;
            result.imageOriginal = $scope.data.image;
            $uibModalInstance.close(result);
        };

        $scope.modalOptions.close = function() {
            $uibModalInstance.dismiss('cancel');
        };

        var initImage = function() {

            var reader = new FileReader();
            reader.onload = function(evt) {
                $scope.$apply(function($scope){
                    $scope.data.image = evt.target.result;
                });
            };

            reader.readAsDataURL(file);
        };

        var start = function() {
            if(file === null) {
                $uibModalInstance.dismiss('cancel');
                return;
            }

            initImage();
        };

        start();

    }
]);
