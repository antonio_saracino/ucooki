angular.module('ucookiApp')
  .controller('AddPhoneModalCtrl', [
    '$scope', '$uibModalInstance', '$http', 'AddressService', 'GoogleMapService', 'phone',
    function($scope, $uibModalInstance, $http, AddressService, GoogleMapService, phone) {

      $scope.data = {}
			$scope.data.phone = phone;

      $scope.modalOptions = {
        closeButtonText: 'Annulla',
        actionButtonText: 'Conferma',
        headerText: 'Aggiunta Numero',
				alertMessage: '',
        bodyTemplate: '/components/modals/add-phone/add-phone-modal.html',

        ok: function(result) {

          if (!$scope.data.phone || isNaN($scope.data.phone) || $scope.data.phone.toString().length < 7 ) {
						$scope.alert = 'alert-warning';
						$scope.modalOptions.alertMessage = "Inserisci un numero valido";
            return;
          }

          $uibModalInstance.close($scope.data.phone);
        },
        close: function(result) {
          $uibModalInstance.dismiss('cancel');
        }
      };

    }
  ]);
