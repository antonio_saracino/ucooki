/**
 * Created by Marco on 19/09/15.
 */
angular.module("ucookiApp")
  .controller("UserDetailsCtrl", ['$scope', '$state', '$stateParams', 'Customer', 'GoogleMapService', '$filter', 'AddthisService', 'MetadataService', 'ErrorTrackerService',
    function ($scope, $state, $stateParams, Customer, GoogleMapService, $filter, AddthisService, MetadataService, ErrorTrackerService) {

      $scope.loading = false;
      $scope.user = {};
      $scope.products = {};

      $scope.formatDate = function (date) {
        return date.getDate() + "/" + date.getMonth() + 1 + "/" + date.getFullYear();
      };

      $scope.getPrincipalPicUrl = function (product) {

        var filter = $filter('cloudinaryResize');

        for (var index in product._pictures) {
          if (product._pictures[index].principal === true) {
            return filter(product._pictures[index].url, 'h_450');
          }
        }
        return null;
      };

      $scope.getProductorUrl = function (rating) {
        return $state.href("customer", {
          customerId: rating.productor_id
        }, {
          absolute: true
        });
      }

      $scope.showOtherRatings = function () {
        $scope.allRatings = true;
      }

      //*************************** HELPER FUNCTION *********************************//

      var getUserDetails = function () {
        $scope.loading = true;

        Customer.findById({
            id: $stateParams.customerId
          },
          function (user) {
            $scope.user = user;

            if (user._picture && user._picture.url) {
              MetadataService.setImage(user._picture.url);
            }

            AddthisService.updateAddthis(user.name, user.description);

            if (user.cooker) {

              GoogleMapService.decodeAddress(user.kitchen_location_info.address).then(function (result) {
                user.kitchen_location_info.city = result.city;
                user.kitchen_location_info.region = result.region;

                MetadataService.setDescription(user.description);
                MetadataService.setTitle(user.kitchen_location_info.city + " - " + user.name + " " + user.lastname + " - Cuoco per Ucooki");

              });

              $scope.current = {
                center: {
                  latitude: user.kitchen_location.lat,
                  longitude: user.kitchen_location.lng
                }
              }

              loadProducts(0, 99);
            } else {
              MetadataService.setDescription(user.description);
              MetadataService.setTitle(user.name + " " + user.lastname + " - Amante della cucina tradizionale di Ucooki");
              $scope.loading = false;
            }
          },
          function (err) {
            console.log(err);
            $scope.loading = false;
          }
        );
      };

      function loadProducts(offset, limit) {
        var filter = {
          limit: limit,
          where: {
            status: 1,
            active: true
          }
        };

        if (offset > 0) {
          filter.offset = offset;
        }

        if (!$scope.user.id) {
          return;
        }

        $scope.loading = true;

        Customer.products({
            filter: filter,
            id: $scope.user.id
          },
          function (products) {
            $scope.products = products;
            //            console.log("products: ", products);
            $scope.loading = false;
          },
          function (err) {
            console.error("error reading data: ");
            console.error(err);
            $scope.loading = false;
          });
      };

      var init = function () {

        if (!$stateParams.customerId) {
          ErrorTrackerService.warn("Try to find a costumer with null id. ");
          $state.go("home");
          return;
        }

        getUserDetails();
      };

      init();
    }
  ]);
