/**
 * Created by nailon on 07/09/15.
 */
angular.module("ucookiApp")
  .controller("LoginCtrl", ['$rootScope', '$scope', '$state', '$http', '$location', '$window', 'Customer', 'AuthService', 'ngToast', 'ModalService', 'smoothScroll','ErrorTrackerService',
    function ($rootScope, $scope, $state, $http, $location, $window, Customer, AuthService, ngToast, ModalService, smoothScroll, ErrorTrackerService) {

      $scope.loading = false;
      $scope.return = "";
      $scope.credentials = {};

      $scope.login = function () {
        $scope.loading = true;
        var element = document.getElementById("login-container");
        smoothScroll(element, {offset: 100});
        $scope.alerts = [];

        if (AuthService.isAuthenticated()) {
          $location.search({}).path($scope.return).replace();
          return;
        }

        $scope.credentials.username = $scope.credentials.username ? $scope.credentials.username.toLowerCase().trim() : "";

        return AuthService.login($scope.credentials, $scope.alerts, function (user) {

          $location.search({}).path($scope.return).replace();

        }, function (error) {
          $scope.loading = false;
          console.error(error);

          if (error.code == "LOGIN_FAILED_EMAIL_NOT_VERIFIED") {
            ngToast.create({
              className: 'danger',
              content: "La tua email non è stata ancora verificata.<br> Se non hai ricevuto la mail di verifica richiedila cliccando sul link in basso.",
              timeout: 5000
            });

          } else {
            ngToast.create({
              className: 'danger',
              content: "Email o password errate",
              timeout: 5000
            });
          }
        });
      }

      $scope.requestPassword = function () {
        var modalInstance = ModalService.showModal({
          controller: 'ResetPasswordModalCtrl'
        });

        modalInstance.then(function (email) {
          console.log("email:", email);
          Customer.resetPassword({
            email: email
          }, function (result) {
            ngToast.create({
              className: 'success',
              content: "E' stata inviata una mail all'indirizzo che hai indicato <br> Controlla la casella di posta elettronica.",
              timeout: 5000
            });
          }, function (err) {
            ngToast.create({
              className: 'danger',
              content: "Si è verificato un errore durante l'invio della mail",
              timeout: 5000
            });
          });
        }, function () {
        });

      }

      $scope.sendVerificationToken = function () {
        var modalInstance = ModalService.showModal({
          controller: 'SendVerificationTokenModalCtrl'
        });

        modalInstance.then(function (email) {
          Customer.sendTokenAgain({
            email: email
          }, function (result) {
            var msg = "La tua mail non risulta essere disattiva<br>Se hai problemi ad effettuare il login contattaci.";
            if (result.result) {
              msg = "Richiesta inviata con successo <br> Controlla la casella di posta elettronica.";
            }
            ngToast.create({
              className: 'success',
              content: msg,
              timeout: 5000
            });
          }, function (err) {
            ngToast.create({
              className: 'danger',
              content: "Si è verificato un errore durante l'invio della mail di verifica",
              timeout: 5000
            });
            ErrorTrackerService.error("Error occurred while sending a new verification token email. ", err);
          });
        }, function () {
        });

      };

      $scope.getPassportReturnUrl = function () {
        return encodeURIComponent($scope.return);
      }

      $scope.isAuthenticated = function () {
        return AuthService.isAuthenticated();
      }

      /**************************************************************/

      var init = function () {

        $scope.return = $state.href('home');

        if ($location.search()) {
          if ($location.search().return) {
            $scope.return = decodeURIComponent($location.search().return);
          }
        }

        AuthService.user().then(function () {
          $location.search({}).path($scope.return);
        }).catch(function () {

        });

      };


      init();


    }
  ]);
