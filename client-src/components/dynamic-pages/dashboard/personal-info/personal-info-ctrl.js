/**
 * Created by me on 18/10/15.
 */
angular.module("ucookiApp")

    .controller("PersonalInfoCtrl", ['$scope', '$location', '$rootScope', '$filter', 'Customer', '$state', 'GoogleMapService', 'AuthService', 'ModalService', 'PicturesService', 'ngToast', 'smoothScroll', 'ErrorTrackerService', 'TrackerService',
        function ($scope, $location, $rootScope, $filter, Customer, $state, GoogleMapService, AuthService, ModalService, PicturesService, ngToast, smoothScroll, ErrorTrackerService, TrackerService) {

            const DISTANCE_DEFAULT = 2000;

            $scope.loading = false;
            $scope.message = {
                address: "Inserisci il tuo indirizzo di casa",
                distance: "Seleziona la distanza massima per l'eventuale trasporto. La consegna non è obbligatoria, per ogni piatto potrai specificare se è disponibile oppure no."
            }
            $scope.products = {};
            $scope.address = "";
            $scope.errorSearch = false;
            $scope.errorMessage = "";
            $scope.data = {}

            $scope.distances = [
                {value: 500, name: '500 m'},
                {value: 1000, name: '1 Km'},
                {value: 2000, name: '2 Km'},
                {value: 3000, name: '3 Km'},
                {value: 4000, name: '4 Km'},
                {value: 5000, name: '5 Km'}
            ];

            $scope.currentMap = {
                center: {}
            }

            $scope.autocompleteOptions = {
                componentRestrictions: {
                    country: 'it'
                }
            };

            $scope.save = function () {

                ngToast.dismiss();

                var folder = $scope.data.user.email;
                var tags = "profile-picture";
                var pictures = [];
                if ($scope.data.user._picture !== undefined) {
                    if ($scope.data.user._picture.status === 0) {
                        pictures[0] = $scope.data.user._picture;
                    }
                }

                $scope.loading = true;
                //      var element = document.getElementById("angular-container");
                //      smoothScroll(element);

                PicturesService.uploadPictures(folder, tags, pictures, null, function (image, error) {
                    error.image = image;
                    ErrorTrackerService.error("Error occurred while uploading product image.", error);
                    ngToast.create({
                        className: 'danger',
                        content: "Errore nel salvataggio dell'immagine profilo",
                        timeout: 5000
                    });

                    $scope.loading = false;
                }, function (images) {

                  if (images && images.length > 0) {
                    $scope.data.user._picture = images[0];
                  }

                  $scope.loading = false;
                  saveUser($scope.data.user);

                });
            };

            $scope.preparePicForUpload = function (picture) {
                if (picture === null) {
                    return;
                }
                ModalService.showModal({
                    controller: 'CropperModalCtrl',
                    resolve: {
                        image: function () {
                            return picture;
                        }
                    }
                }).then(
                    function (result) {
                        $scope.data.user._picture = {
                            data: result.imageCropped,
                            dataOriginal: result.imageOriginal,
                            principal: true,
                            status: 0
                        };
                    }
                );
            };

            /* Modify the look and fill of the dropzone when files are being dragged over it */
            $scope.dragOverClass = function ($event) {
                var items = $event.dataTransfer.items;
                var hasFile = false;
                if (items != null) {
                    for (var i = 0; i < items.length; i++) {
                        if (items[i].kind == 'file') {
                            hasFile = true;
                            break;
                        }
                    }
                } else {
                    hasFile = true;
                }
                return hasFile ? "dragover" : "dragover-err";
            };

            $scope.removeNewPicture = function () {
                $scope.data.user._picture = null;
            };

            $scope.setPictureStatus = function (status) {
                $scope.data.user._picture.status = status;
            };

            $scope.addAddress = function () {
                var modalInstance = ModalService.showModal({
                    controller: 'AddAddressModalCtrl'
                });

                modalInstance.then(function (data) {

                    if (data) {

                        GoogleMapService.getLocation(data.address)
                            .then(function (value) {

                                $scope.data.user.kitchen_location = {
                                    lat: value.coordinates.lat,
                                    lng: value.coordinates.lng
                                };

                                $scope.currentMap.center = {
                                    latitude: value.coordinates.lat,
                                    longitude: value.coordinates.lng
                                }

                                $scope.data.user.kitchen_location_info = {
                                    address: value.formatted_address,
                                    street_number: data.streetNumber,
                                    note: data.note
                                }

                            }).catch(function (error) {
                            console.log("location error: ", error);

                            ngToast.create({
                                className: 'danger',
                                content: 'Inserisci un indirizzo valido',
                                timeout: 5000
                            });
                        });
                    }

                    console.log("$scope.data.user:", $scope.data.user);

                }, function () {
                });
            };

            $scope.destroyMe = function() {
              ModalService.showModal({
                  backdrop: true
                },
                {
                  bodyText: "Sicuro di voler eliminare il tuo account? La cancellazione è irreversibile!",
                  headerText: "Continuare?",
                  closeButtonText: 'Annulla',
                  actionButtonText: 'Si, addio!'
                }).then(
                function () {
                  // ok, voglio eliminare tutto
                  Customer.deleteMe({id: $scope.data.user.id}, function() {
                    TrackerService.deleteMe($scope.data.user);
                    AuthService.logout();
                  }, function(error) {
                    ErrorTrackerService.error("Error occurred while deleting customer", error);
                  });
                },
                function () {
                  // no, ferma tutto, ho cambiato idea
                });
            };

            $scope.getProfileLink = function() {
              return $state.href('customer', {customerId: $scope.data.user.id}, {absolute: true});
            };

            /************************ Private functions *********************************/

            var saveUser = function (user) {

                if (!user) {
                    ErrorTrackerService.debug("Try to save null user");
                    return;
                }

                if (!validate(user)) {
                    return;
                }

                if (user._picture) {
                    if (user._picture.status === 2) {
                        var pictures = [];
                        pictures[0] = user._picture;
                        PicturesService.deletePictures(pictures);
                        user._picture = {};
                    }
                }
                var cap = $filter("capitalize");
                user.name = cap(user.name);
                user.lastname = cap(user.lastname);

                $scope.loading = true;

                // salvo le informazioni
                Customer.prototype$updateAttributes({
                        id: user.id
                    }, user,
                    function (result) {
                        $scope.loading = false;
                        console.log("salvato: ", result);
                        angular.copy(user, $scope.user);
                        $scope.userForm.$setPristine();

                        ngToast.create({
                            className: 'success',
                            content: "Le informazioni sono state <br>correttamente salvate",
                            timeout: 5000
                        });

                    },
                    function (err) {
                        $scope.loading = false;
                        err.user = user;
                        ErrorTrackerService.error("Error occurred during customer updating.", err);

                        ngToast.create({
                            className: 'danger',
                            content: "Errore nel salvataggio",
                            timeout: 5000
                        });

                    });

            };

            var validate = function (user) {

                if (!user) {
                    console.error("Empty user");
                    return false;
                }

                if (!user.name || !user.lastname) {
                    ngToast.create({
                        className: 'danger',
                        content: "Nome e cognome non possono essere vuoti",
                        timeout: 5000
                    });
                    return false;
                }

                if (user.cooker && !user.kitchen_location_info) {
                    ngToast.create({
                        className: 'danger',
                        content: "Inserisci il tuo indirizzo",
                        timeout: 5000
                    });
                    return false;

                }

                if (!user.phone || isNaN(user.phone)) {
                    ngToast.create({
                        className: 'danger',
                        content: "Inserisci un numero di telefono valido",
                        timeout: 5000
                    });
                    return false;

                }

                return true;

            }

            var init = function () {

                if (!AuthService.isAuthenticated()) {
                    $state.go("login", {
                        return: encodeURIComponent($location.url())
                    });
                    return;
                }


                if ($scope.user._picture && !$scope.user._picture.id) {
                    // @todo da sistemare
                    // se l'utente ha una foto non valida non la considero
                    $scope.user._picture = undefined;
                }

                // clonazione user
                $scope.data.user = angular.copy($scope.user);

                // valore di defaul di distanza
                if (!$scope.data.user.productor_info) {
                    $scope.data.user.productor_info = {
                        max_distance: DISTANCE_DEFAULT
                    }
                }

                if ($scope.data.user.kitchen_location) {
                    $scope.currentMap.center = {
                        latitude: $scope.data.user.kitchen_location.lat,
                        longitude: $scope.data.user.kitchen_location.lng
                    }
                }

                // variabile per la prosecuzione del cambio pagina anche
                // se la form è stata modificata
                $scope.dirtyContinue = false;


                // controllo che non venga cambiata pagina per sbaglio
                $scope.$on("$stateChangeStart", function (event, next, current) {

                    if (!$scope.userForm.$dirty || $scope.dirtyContinue) {
                        // posso tranquillamente uscire
                        return;
                    }

                    ModalService.showModal({
                        backdrop: true
                    }, {
                        bodyText: "Non hai salvato le modifiche, sicuro di voler cambiare pagina?",
                        headerText: "Continuare?",
                        closeButtonText: 'Annulla',
                        actionButtonText: 'Si, cambia pagina'
                    }).then(
                        function () {
                            // ok, voglio andare avanti
                            $scope.dirtyContinue = true;
                            $state.go(next.name);
                        },
                        function () {
                            // no, ferma tutto, voglio salvare le modifiche
                        }
                    );

                    event.preventDefault();
                });

            };

            init();
        }
    ])
