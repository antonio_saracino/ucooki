angular.module("ucookiApp")
  .controller('OrdersCtrl', ['$scope', '$state', '$location', '$rootScope', 'Order', 'Customer', 'Product', 'ModalService', 'AuthService', 'ngToast', 'OrderService', 'ErrorTrackerService',
    function ($scope, $state, $location, $rootScope, Order, Customer, Product, ModalService, AuthService, ngToast, OrderService, ErrorTrackerService) {

      $scope.loading = true;
      $scope.oldestLoading = false;
      $scope.user = $rootScope.user;
      $scope.status = false;
      $scope.timeout = new Date();
      $scope.timeout.setHours($scope.timeout.getHours() + 12);
      $scope.data = {
        timeout : $scope.timeout.getTime()
      };


      /**
       * stato ordine:
       * -1: errore,
       *  0: attesa,
       *  1: confermato,
       *  2: pagato,
       *  3: completato,
       *  4: rifiutato,
       *  5: cancellato,
       *  6: annullato
       */

      $scope.tab = 1;

      $scope.openTab = function (tab) {
        $scope.tab = tab;
      };


      $scope.buttonPressed = function (type, order) {
        if (!order) {
          return;
        }

        var cooker = userIsCooker(order);

        if (cooker) {
          // se siamo un cooker
          if (type == 'reject' && order.status == 0) {
            // sto rifiutando un ordine
            var modalInstance = ModalService.showModal({
              controller: 'OrderRefuseModalCtrl'
            });

            modalInstance.then(function (data) {
              if (data.note) {
                order.productor_note = data.note;
              }
              save(order, 4);
            }, function () { });


          } else if (type == 'confirm' && order.status == 0) {
            // sto accettando l'ordine
            ModalService.showModal({}, {
              closeButtonText: 'Annulla',
              actionButtonText: 'Continua',
              headerText: 'Gestione Ordine',
              bodyText: 'Stai accettando l\'ordine, vuoi procedere?'
            }).then(function () {
              save(order, 1);
            });
          } else if (type == 'cancel' && order.status == 1) {
            // sto annullando un ordine già confermato
            var modalInstance = ModalService.showModal({
              controller: 'OrderRefuseModalCtrl'
            });

            modalInstance.then(function (data) {
              if (data.note) {
                order.productor_note = data.note;
              }
              save(order, 6);
            }, function () { });
          }
        } else {
          // siamo un eater
          if (type == 'cancel' && order.status == 0) {
            // ho deciso di cancellare un ordine
            ModalService.showModal({}, {
              closeButtonText: 'Annulla',
              actionButtonText: 'Continua',
              headerText: 'Gestione Ordine',
              bodyText: 'Stai cancellando l\'ordine, vuoi procedere?'
            }).then(function () {
              save(order, 5);
            });
          }
        }
      }

      $scope.isRejectEnable = function (order) {

        if (!order) {
          return 'disable';
        }
        var cooker = userIsCooker(order);
        switch (order.status) {
          case 0:
            if (cooker) {
              return 'enable';
            }
        }

        return 'disable';
      }

      $scope.isConfirmEnable = function (order) {

        if (!order) {
          return 'disable';
        }
        var cooker = userIsCooker(order);
        switch (order.status) {
          case 0:
            if (cooker) {
              return 'enable';
            }
        }

        return 'disable';
      }

      $scope.isCancelEnable = function (order) {

        if (!order) {
          return 'disable';
        }
        var cooker = userIsCooker(order);
        switch (order.status) {
          case 0:
            if (!cooker) {
              return 'enable';
            }
            break;
          case 1:
            if (cooker) {

              var now = new Date();
              var delivery = new Date(order.delivery_date);

              if (delivery.getTime() - now.getTime() >= 1000 * 60 * 3) {
                // ultimo momento utile per cancellare l'ordine
                return 'enable';
              }

            }
        }

        return 'disable';
      }

      $scope.changeTab = function () {
        ngToast.dismiss();
      }

      $scope.feedbackEater = function (item, order) {

        var itemCopy = angular.copy(item);

        var modalInstance = ModalService.showModal({
          controller: 'FeedbackModalCtrl'
        });

        modalInstance.then(function (data) {

          if (data) {

            // completiamo le informazioni del rating sul prodotto
            itemCopy.rating = {};
            itemCopy.rating.rate = data.rate;
            itemCopy.rating.message = data.message;
            itemCopy.rating.buyer_id = $scope.user.id;
            itemCopy.rating.order_id = order.id;

            Product.prototype$__create__ratings({
              id: itemCopy.product_id
            }, itemCopy.rating, function (result) {
              console.log("rating saved: ", result);
              angular.copy(itemCopy, item);
            }, function (err) {
              ErrorTrackerService.error("Error occurred during eater feedback saving.", err);
              ngToast.create({
                className: 'danger',
                content: "Errore durante il salvataggio del feedback",
                timeout: 5000
              });
            })

          }

        }, function () { });
      }

      $scope.feedbackCooker = function (order) {

        var orderCopy = angular.copy(order);

        var modalInstance = ModalService.showModal({
          controller: 'FeedbackModalCtrl'
        });

        modalInstance.then(function (data) {

          if (data) {

            // completiamo le informazioni del rating sull'eater
            orderCopy.rating = {};
            orderCopy.rating.rate = data.rate;
            orderCopy.rating.message = data.message;
            orderCopy.rating.productor_id = $scope.user.id;
            orderCopy.rating.order_id = order.id;

            Customer.prototype$__create__ratings({
              id: orderCopy.buyer_id
            }, orderCopy.rating, function (result) {
              console.log("order rating saved: ", result);
              angular.copy(orderCopy, order);
            }, function (err) {
              ErrorTrackerService.error("Error occurred during cooker feedback saving.", err);
              ngToast.create({
                className: 'danger',
                content: "Errore durante il salvataggio del feedback",
                timeout: 5000
              });
            })

          }

        }, function () { });
      }

      $scope.loadOldestOrders = function () {

        $scope.oldestLoading = true;

        Order.getOldestOrders({ userId: $scope.user.id }, function (result) {
          $scope.oldestLoading = false;
          $scope.receivedOrdersOld = result.orders.received;
          $scope.sentOrdersOld = result.orders.sent;
        });

      }


      //************************************************//

      var save = function (order, status) {

        $scope.loading = true;

        if (!order) {
          return;
        }

        var copyOrder = angular.copy(order);
        copyOrder.status = status;

        Order.upsert(copyOrder, function (result) {
          console.log("saved: ", result);
          angular.copy(copyOrder, order);

          $scope.loading = false;
          ngToast.create({
            className: 'success',
            content: "Il tuo ordine è stato correttamente aggiornato",
            timeout: 5000
          });

        }, function (err) {
          $scope.loading = false;
          ErrorTrackerService.error("Error occurred during order saving.", err);
          ngToast.create({
            className: 'danger',
            content: "Errore durante il salvataggio",
            timeout: 5000
          });
        });

      }

      var userIsCooker = function (order) {
        if (!order) {
          return false;
        }

        if (order.productor_id == $scope.user.id) {
          return true;
        }

        return false;
      }

      var init = function () {

        if (!AuthService.isAuthenticated()) {
          $state.go("login", {
            return: encodeURIComponent($location.url())
          });
          return;
        }

        $scope.loading = true;

        if (!$scope.user.cooker) {
          $scope.openTab(2);
        }

        Order.getLatestOrders({ userId: $scope.user.id }, function (result) {
          $scope.loading = false;

          if (result && result.orders) {

            if (result.orders.received) {
              result.orders.received.sort(function (a, b) {
                return a.status - b.status;
              });
              $scope.receivedOrders = result.orders.received;
            }

            if (result.orders.sent) {
              result.orders.sent.sort(function (a, b) {
                return a.status - b.status;
              });
              $scope.sentOrders = result.orders.sent;
            }
          }
        });

      }

      init();

    }
  ]);
