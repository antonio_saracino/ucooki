angular.module("ucookiApp")
	.controller('DashboardCtrl', ['$scope', '$rootScope', '$state',
		function ($scope, $rootScope, $state) {

			$scope.user = $rootScope.user;

			$scope.goto = function(state) {
				$state.go(state);
			}

		}
	]);
