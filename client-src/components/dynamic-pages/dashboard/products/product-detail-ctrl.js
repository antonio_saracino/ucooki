/**
 * Created by Marco on 19/09/15.
 */
angular.module("ucookiApp")

    .controller("ProductDetailCtrl", ['$scope', '$rootScope', '$stateParams', '$state', '$location', '$window', 'Product', 'PicturesService', 'ModalService', 'AuthService', 'smoothScroll', 'ngToast', 'ErrorTrackerService',
        function ($scope, $rootScope, $stateParams, $state, $location, $window, Product, PicturesService, ModalService, AuthService, smoothScroll, ngToast, ErrorTrackerService) {

            $scope.message = {
                price: "Ti consigliamo di proporre un costo che sia economicamente vantaggioso se confrontato con la ristorazione classica. Occasionalmente Ucooki potrebbe proporre sconti agli utenti fino al 20%",
                tag: "Inserisci i tag che meglio rappresentano il tuo piatto, più saranno specifici più sarà facile trovarlo.",
                saleType: "Il metodo consigliato è a 'porzioni' per la maggior parte dei piatti, a 'peso' invece è indicato per torte, biscotti, etc.",
                weight: {
                    min: "Quantità minima che si può ordinare espressa in Kg.",
                    max: "Quantità massima che si può ordinare espressa in Kg."
                },
                notice: "Consigliamo di tenere il tempo di preavviso più basso possibile, l'ideale è riuscire a preparare un piatto in 3/4 ore magari ottimizzando la preparazione (congelando parte del preparato per esempio). Se non fosse possibile avere tempi stretti l'ideale è avere un preavviso di 12 o in casi estremi di 24 ore."
            }

            $scope.tags = ['primo', 'secondo', 'dolce', 'vegetariano', 'piatto unico', 'pesce', 'carne', 'pasta fresca', 'tradizionale', 'vegano', 'leggero', 'regionale', 'ligure', 'piemontese', 'valdostano', 'lombardo', 'veneto', 'altoatesino', 'friulano', 'emiliano romagnolo', 'toscano', 'marchigiano', 'umbro', 'abruzzese', 'molisano', 'laziale', 'campano', 'lucano', 'calabrese', 'pugliese', 'siciliano', 'sardo'];
            $scope.tags.sort();

            $scope.loading = false;
            $scope.user = $rootScope.user;
            $scope.product = {
                active: true,
                min_portion: 1,
                max_portion: 10,
                notice_time: 1,
                delivery: true,
                delivery_price: 0,
                sale_type: 0,
                weight: 0.5,
                tags: [],
                _ingredients: [],
                _pictures: [],
                _timeRanges: [
                    {
                        id: 1,
                        day: 1,
                        startLunch: -1,
                        endLunch: -1,
                        startDinner: -1,
                        endDinner: -1
                    },
                    {
                        id: 2,
                        day: 2,
                        startLunch: -1,
                        endLunch: -1,
                        startDinner: -1,
                        endDinner: -1
                    }, {
                        id: 3,
                        day: 3,
                        startLunch: -1,
                        endLunch: -1,
                        startDinner: -1,
                        endDinner: -1
                    }, {
                        id: 4,
                        day: 4,
                        startLunch: -1,
                        endLunch: -1,
                        startDinner: -1,
                        endDinner: -1
                    }, {
                        id: 5,
                        day: 5,
                        startLunch: -1,
                        endLunch: -1,
                        startDinner: -1,
                        endDinner: -1
                    }, {
                        id: 6,
                        day: 6,
                        startLunch: -1,
                        endLunch: -1,
                        startDinner: -1,
                        endDinner: -1
                    }, {
                        id: 7,
                        day: 7,
                        startLunch: -1,
                        endLunch: -1,
                        startDinner: -1,
                        endDinner: -1
                    }]
            };

            $scope.lunchItems = [-1, 1100, 1130, 1200, 1230, 1300, 1330, 1400, 1430, 1500];
            $scope.dinnerItems = [-1, 1800, 1830, 1900, 1930, 2000, 2030, 2100, 2130, 2200];
            $scope.saleTypes = [{
                id: 0,
                name: 'a porzioni'
            }, {
                id: 1,
                name: 'a peso'
            }];
            $scope.deliveryPrices = [{
                id: 0,
                name: 'gratuito'
            }, {
                id: 1,
                name: '1€'
            }];

            var validate = function (product) {

                var error = false;

                if (!product) {
                    return false;
                }

                if (!product.name) {

                    ngToast.create({
                        className: 'danger',
                        content: "Inserisci un nome",
                        timeout: 5000
                    });
                    error = true;
                }

                if (!product.description) {
                    ngToast.create({
                        className: 'danger',
                        content: "Inserisci una descrizione",
                        timeout: 5000
                    });
                    error = true;

                }

                if (product.delivery && (product.delivery_price == undefined || product.delivery_price == null)) {
                    ngToast.create({
                        className: 'danger',
                        content: "Inserisci il costo di trasporto, oppure disattiva la spedizione",
                        timeout: 5000
                    });
                    error = true;
                }

                if (!product.price) {
                    ngToast.create({
                        className: 'danger',
                        content: "Inserisci il costo unitario (porzione o peso)",
                        timeout: 5000
                    });
                    error = true;
                }

                if (product.sale_type == 0 && !product.min_portion) {
                    ngToast.create({
                        className: 'danger',
                        content: "Inserisci il numero di porzioni minime",
                        timeout: 5000
                    });
                    error = true;
                }

                if (product.sale_type == 0 && !product.max_portion) {
                    ngToast.create({
                        className: 'danger',
                        content: "Imposta il numero di porzioni massime",
                        timeout: 5000
                    });
                    error = true;
                }

                if (product.sale_type == 0 && product.min_portion > product.max_portion) {
                    ngToast.create({
                        className: 'danger',
                        content: "Imposta il numero di porzioni massime superiore al minimo",
                        timeout: 5000
                    });
                    error = true;
                }

                if (product.sale_type == 1 && product.weight <= 0) {
                    ngToast.create({
                        className: 'danger',
                        content: "Imposta il peso minimo del prodotto",
                        timeout: 5000
                    });
                    error = true;
                }

                if (!product.notice_time || product.notice_time <= 0) {
                    ngToast.create({
                        className: 'danger',
                        content: "Imposta il preavviso di cui hai bisogno",
                        timeout: 5000
                    });
                    error = true;
                }

                if (!product.tags || product.tags.length <= 0) {
                    ngToast.create({
                        className: 'danger',
                        content: "Inserisci almeno un tag",
                        timeout: 5000
                    });
                    error = true;
                }

                if (!validateIngredients(product)) {
                    error = true;
                }
                if (!validateTimeRanges(product)) {
                    error = true;
                }

                return !error;
            }

            var validateIngredients = function (product) {

                // pulisco l'array da ingredienti nulli
                for (var index = product._ingredients.length - 1; index >= 0; index--) {
                    if (!product._ingredients[index].name) {
                        product._ingredients.splice(index, 1);
                    }
                }

                if (product._ingredients.length <= 0) {
                    ngToast.create({
                        className: 'danger',
                        content: "Inserisci almeno un ingrediente",
                        timeout: 5000
                    });
                    return false;
                }

                return true;
            }

            var validateTimeRanges = function (product) {

                var found = false; // cerco almeno un intervallo valido
                for (var index = 0; index < product._timeRanges.length; index++) {
                    var range = product._timeRanges[index];

                    if (!(range.startLunch <= 0 && range.endLunch <= 0)) {
                        // se non sono entrambi nulli
                        // verifico che se siano diversi da -1 e il primo sia precedente al secondo
                        if (range.startLunch <= 0 || range.endLunch <= 0 || range.startLunch >= range.endLunch) {
                            ngToast.create({
                                className: 'danger',
                                content: "Verifica gli intervalli della Tabella Disponibilità",
                                timeout: 8000
                            });
                            // ne ho trovato uno non valido.
                            return false;
                        } else {
                            found = true;
                        }
                    }

                    if (!(range.startDinner <= 0 && range.endDinner <= 0)) {
                        // se non sono entrambi nulli
                        // verifico che se siano diversi da -1 e il primo sia precedente al secondo
                        if (range.startDinner <= 0 || range.endDinner <= 0 || range.startDinner >= range.endDinner) {
                            ngToast.create({
                                className: 'danger',
                                content: "Verifica gli intervalli della Tabella Disponibilità",
                                timeout: 8000
                            });

                            // ne ho trovato uno non valido.
                            return false;
                        } else {
                            found = true;
                        }
                    }
                }

                if (!found) {
                    ngToast.create({
                        className: 'danger',
                        content: "Inserisci almeno un giorno valido<br>nella Tabella Disponibilità",
                        timeout: 5000
                    });
                    return false;
                }

                return true;
            }

            $scope.showProducts = function () {
                $state.go("dashboard.products");
            }

            $scope.getRangeValues = function (day, meal) {

                var oppositeMeal = "startLunch";
                switch (meal) {
                    case 'startLunch':
                        oppositeMeal = 'endLunch';
                        break;
                    case 'endLunch':
                        oppositeMeal = 'startLunch';
                        break;
                    case 'startDinner':
                        oppositeMeal = 'endDinner';
                        break;
                    case 'endDinner':
                        oppositeMeal = 'startDinner';
                        break;
                }

                var oppositeSelection = $scope.product._timeRanges[day][oppositeMeal];
                if (oppositeSelection == -1) {
                    // caso standard, non ho fatto alcuna scelta
                    if (meal == "startLunch") {
                        // pranzo - elimino l'ultimo valore
                        return $scope.lunchItems.slice(0, $scope.lunchItems.length - 1);
                    } else if (meal == "endLunch") {
                        // pranzo - elimino il primo valore
                        var arr = $scope.lunchItems.slice(2, $scope.lunchItems.length);
                        arr.splice(0, 0, -1);
                        return arr;
                    } else if (meal == "startDinner") {
                        // cena - elimino l'ultimo valore
                        return $scope.dinnerItems.slice(0, $scope.dinnerItems.length - 1);
                    } else {
                        // cena - elimino il primo valore
                        var arr = $scope.dinnerItems.slice(2, $scope.dinnerItems.length);
                        arr.splice(0, 0, -1);
                        return arr;
                    }
                } else {
                    // ho già fatto una selezione sull'opposto
                    var result;
                    if (meal == "startLunch") {
                        result = $scope.lunchItems.filter(isSmallerThan(oppositeSelection));
                    } else if (meal == "startDinner") {
                        result = $scope.dinnerItems.filter(isSmallerThan(oppositeSelection));
                    } else if (meal == "endLunch") {
                        result = $scope.lunchItems.filter(isBiggerThan(oppositeSelection));
                    } else { // endDinner
                        result = $scope.dinnerItems.filter(isBiggerThan(oppositeSelection));
                    }

                    // aggiungo il "non selezionato"
                    if (result[0] != -1) {
                        result.splice(0, 0, -1);
                    }
                    return result;
                }
            }

            $scope.saveRequest = function (product) {
                ngToast.dismiss();

                if (!validate(product)) {
                    return
                }

                if ($scope.user && $scope.user.admin) {
                    $scope.saveProduct(product);
                    return;
                }

                ModalService.showModal({
                    backdrop: true
                }, {
                    bodyText: "Salvando le modifiche consenti a Ucooki di migliorare la tua scheda e quindi di correggere alcune informazioni o di sostituire le foto. Sei d'accordo?",
                    headerText: "Invio proposta piatto",
                    closeButtonText: 'Assolutamente no',
                    actionButtonText: 'Si, grazie'
                }).then(
                    function () {
                        // ok, voglio andare avanti
                        $scope.saveProduct(product);
                    },
                    function () {
                        // no, ferma tutto, non voglio salvare le modifiche
                    });
            }

            $scope.saveProduct = function (product) {

                $scope.loading = true;

                var element = document.getElementById("angular-container");
                smoothScroll(element, {offset: 100});

                if (!product.customer_id) {
                    product.customer_id = $scope.user.id;
                }

                var pictures = managePictures(product._pictures);

                var folder = product.productor ? product.productor.email : $scope.user.email;
                var tags = product.productor ? product.productor.email : $scope.user.email;

                PicturesService.uploadPictures(folder, tags, pictures.toSave,
                    function(saved) {
                        console.log("Image saved: ", saved);
                    },
                    function (pictures, err) {
                        err.pictures = pictures;
                        ErrorTrackerService.error("Error occurred while updating product images.", err);
                        ngToast.create({
                            className: 'danger',
                            content: "Errore durante il salvataggio delle immagini",
                            timeout: 5000
                        });
                    }, function (savedPictures) {
                        product._pictures = [];

                        Array.prototype.push.apply(product._pictures, savedPictures);
                        Array.prototype.push.apply(product._pictures, pictures.others);

                        var principalPresent = false;
                        for (var index in product._pictures) {
                            var picture = product._pictures[index];
                            if (picture.principal) {
                                principalPresent = true;
                                break;
                            }
                        }

                        if (!principalPresent && $scope.product._pictures.length > 0) {
                            $scope.setPrincipalPicture(product._pictures[0]);
                        }

                        Product.upsert(product, function (product) {

                            $scope.product = product;
                            PicturesService.deletePictures(pictures.toDelete);
                            $scope.productForm.$setPristine();
                            $scope.loading = false;
                            ngToast.create({
                                className: 'success',
                                content: "Piatto salvato correttamente",
                                timeout: 5000
                            });

                        }, function (err) {

                            ngToast.create({
                                className: 'danger',
                                content: "Errore durante il salvataggio",
                                timeout: 5000
                            });
                            $scope.loading = false;
                            err.product = product;
                            ErrorTrackerService.error("Error occurred while saving product.", err);
                        });
                    });
            };

            $scope.requestDeleteProduct = function (product) {
                ModalService.showModal({
                        backdrop: true
                    },
                    {
                        bodyText: "Sicuro di voler eliminare questo piatto?",
                        headerText: "Continuare?",
                        closeButtonText: 'Annulla',
                        actionButtonText: 'Si, eliminalo'
                    }).then(
                    function () {
                        // ok, voglio eliminare il piatto
                        $scope.deleteProduct(product);
                    },
                    function () {
                        // no, ferma tutto, ho cambiato idea
                    });
            };

            $scope.deleteProduct = function (product) {

                if (!product.id) {
                    $state.go("dashboard.products");
                    return;
                }

                $scope.loading = true;
                var element = document.getElementsByClassName("box")[0];
                smoothScroll(element, {offset: 100});

                PicturesService.deletePictures(product._pictures);

                Product.deleteById({
                    id: product.id
                }, function () {
                    ngToast.create({
                        className: 'success',
                        content: "Piatto cancellato correttamente",
                        timeout: 5000
                    });
                    $scope.productForm.$setPristine();
                    $window.history.back();
                }, function (err) {
                    ngToast.create({
                        className: 'danger',
                        content: "Errore durante la cancellazione del piatto",
                        timeout: 5000
                    });
                    $scope.loading = false;
                    err.product = product;
                    ErrorTrackerService.error("Error occurred while deleting product.", err);
                });

            };

            $scope.addIngredient = function () {
                var maxId = 1;
                for (var index in $scope.product._ingredients) {
                    if ($scope.product._ingredients[index].id > maxId) {
                        maxId = $scope.product._ingredients[index].id;
                    }
                }

                $scope.product._ingredients.push({
                    id: maxId + 1,
                    name: "",
                    description: ""
                });
            };

            $scope.removeIngredient = function (ingredient) {
                $scope.product._ingredients.splice(
                    $scope.product._ingredients.indexOf(ingredient),
                    1
                );
            };

            $scope.updateTimeRange = function (index, type, value) {
                $scope.product._timeRanges[index][type] = value;
                $scope.productForm.$setDirty();
            };

            /**
             * Start Picture
             *
             */

            $scope.preparePicForUpload = function (picture) {
                if (picture === null) {
                    return;
                }
                ModalService.showModal({
                    controller: 'CropperModalCtrl',
                    resolve: {
                        image: function () {
                            return picture;
                        }
                    }
                }).then(
                    function (result) {
                        $scope.productForm.$setDirty();
                        $scope.product._pictures.push({
                            data: result.imageCropped,
                            dataOriginal: result.imageOriginal,
                            principal: false,
                            status: 0
                        });
                    }
                );
            };

            /* Modify the look and fill of the dropzone when files are being dragged over it */
            $scope.dragOverClass = function ($event) {
                var items = $event.dataTransfer.items;
                var hasFile = false;
                if (items != null) {
                    for (var i = 0; i < items.length; i++) {
                        if (items[i].kind == 'file') {
                            hasFile = true;
                            break;
                        }
                    }
                } else {
                    hasFile = true;
                }
                return hasFile ? "dragover" : "dragover-err";
            };

            $scope.removeNewPicture = function (pictures, picture) {
                $scope.productForm.$setDirty();
                if (picture.status === 0) {
                    pictures.splice(pictures.indexOf(picture), 1);
                }
            };


            $scope.setPictureStatus = function (picture, status) {
                $scope.productForm.$setDirty();
                picture.status = status;
            };

            $scope.setPrincipalPicture = function (picture) {
                $scope.productForm.$setDirty();
                for (var index in $scope.product._pictures) {
                    var pic = $scope.product._pictures[index];
                    pic.principal = false;
                }

                picture.principal = true;
            };

            $scope.createTag = function (tag) {
                return tag.trim();
            };

            $scope.getListWeight = function (minWeight) {

                var maxWeight = [];
                for (var i = 1; i <= 10; i++) {
                    option = {
                        id: i,
                        value: parseFloat((minWeight * i).toFixed(1))
                    }
                    maxWeight.push(option);
                }

                return maxWeight;

            }

            $scope.getWeightIdentifier = function (weight) {
                if (weight && weight.id) {
                    return weight.id;
                }

                return weight;

            }

            $scope.onSaleTypeChange = function () {
                $scope.product.min_portion = 1;
            }


            /*************************** Utility **********************************/

            function isBiggerThan(minValue) {
                return function (value) {
                    return value > minValue;
                }
            }

            function isSmallerThan(maxValue) {
                return function (value) {
                    return value < maxValue;
                }
            }

            /**
             * getPicturesByStatus
             * @param  {array}  pictures
             * @param  {int}    status   Può essere: 0 nuovo, 1 caricato, 2 in eliminazione
             * @return {array}
             */
            var getPicturesByStatus = function (pictures, status) {
                if (pictures.length <= 0 || !status) {
                    return [];
                }

                var result = [];
                for (var index in pictures) {
                    if (pictures[index].status === status) {
                        result.push(pictures[index]);
                    }
                }

                return result;
            };

            /**
             * @param  {array} pictures
             * @return {object}
             */
            var managePictures = function (pictures) {
                var toDelete = [];
                var toSave = [];
                var others = [];

                for (var index in pictures) {
                    var pic = pictures[index]
                    switch (pic.status) {
                        case 0: // nuova immagine
                            toSave.push(pic);
                            break;
                        case 1: // immagine già caricata
                            others.push(pic);
                            break;
                        case 2: // da eliminare
                            toDelete.push(pic);
                            break;
                    }
                }

                return {
                    toDelete: toDelete,
                    toSave: toSave,
                    others: others
                }
            };

            /**
             * End Picture
             */

            var init = function () {
                if (!$rootScope.isAuthenticated) {
                    $state.go("login", {
                        return: encodeURIComponent($location.url())
                    });
                    return;
                }

                var element = document.getElementById("angular-container");
                smoothScroll(element, {offset: 100});

                $scope.loading = true;

                if ($stateParams.productId) {
                    $scope.product = Product.findById({
                        id: $stateParams.productId
                    }, function (product) {
                        console.log("product: ", $scope.product);
                        $scope.loading = false;
                    }, function (err) {
                        console.log("Product not found: ", err);
                        $state.go("dashboard.products");
                    });
                } else {
                    $scope.loading = false;
                }


                // variabile per la prosecuzione del cambio pagina anche
                // se la form è stata modificata
                $scope.dirtyContinue = false;


                // controllo che non venga cambiata pagina per sbaglio
                $scope.$on("$stateChangeStart", function (event, next, current) {

                    console.log("$scope.productForm.$dirty: ", $scope.productForm.$dirty);

                    if (!$scope.productForm.$dirty || $scope.dirtyContinue) {
                        // posso tranquillamente uscire
                        return;
                    }

                    ModalService.showModal({
                        backdrop: true
                    }, {
                        bodyText: "Non hai salvato, sicuro di voler cambiare pagina?",
                        headerText: "Continuare?",
                        closeButtonText: 'Annulla',
                        actionButtonText: 'Si, cambia pagina'
                    }).then(
                        function () {
                            // ok, voglio andare avanti
                            $scope.dirtyContinue = true;
                            $state.go(next.name);
                        },
                        function () {
                            // no, ferma tutto, voglio salvare le modifiche
                        });

                    event.preventDefault();
                });

            };

            init();

        }
    ]);
