/**
 * Created by me on 18/10/15.
 */
angular.module("ucookiApp")
  .controller("ProductsManageCtrl", ['$scope', '$rootScope', '$location', '$window', '$filter', 'Customer', 'Product', '$state', 'AuthService', 'ngToast','ErrorTrackerService',
    function ($scope, $rootScope, $location, $window, $filter, Customer, Product, $state, AuthService, ngToast, ErrorTrackerService) {

      $scope.loading = false;
      $scope.products = {};

      $scope.currentPage = 1;
      $scope.itemsPerPage = 999;
      $scope.totalProducts = 0;

      $scope.pageChanged = function () {
        var skip = $scope.currentPage > 1 ? $scope.itemsPerPage * ($scope.currentPage - 1) : 0;
        loadProducts(skip, $scope.itemsPerPage);
      };

      $scope.saveProduct = function (product) {
        if (!product) {
          return;
        }

        ngToast.dismiss();
        Product.upsert(product, function (product) {

          $scope.product = product;

        }, function (err) {
          err.product = product;
          ErrorTrackerService.error("Error occurred while saving product.", err);
          ngToast.create({
            className: 'danger',
            content: "Errore durante il salvataggio",
            timeout: 5000
          });
        });
      }


      $scope.getPrincipalPicUrl = function (images) {

        if (!images || images.length <= 0 || !images[0]) {
          return null;
        }

        var filter = $filter('cloudinaryResize');

        for (var index in images) {
          if (images[index].principal === true) {
            return filter(images[index].url, 'h_450,q_80');
          }
        }
        return null;
      };


      $scope.getProductPerLine = function () {
        var width = $window.innerWidth;
        if (width >= 1200) {
          return 3;
        }

        if (width >= 990) {
          return 2;
        }

        return 1;
      };

      $scope.getProfileLink = function() {
        return $state.href('customer', {customerId: $scope.user.id}, {absolute: true});
      };


      /*************************** Utility **********************************/

      function loadProducts(offset, limit) {
        var filter = {
          limit: limit,
          order: 'name'
        };

        if (offset > 0) {
          filter.offset = offset;
        }

        if (!$scope.user.id) {
          return;
        }
        $scope.loading = true;

        Customer.products({
          filter: filter,
          id: $scope.user.id
        },
          function (products) {
            $scope.products = products;
            // console.log("products: ", products);
            $scope.loading = false;
          },
          function (err) {
            ErrorTrackerService.error("Error occurred while loading products.", err);
            $scope.loading = false;
          });
      };


      var start = function () {
        if (!AuthService.isAuthenticated()) {
          $state.go("login", {
            return: encodeURIComponent($location.url())
          });
          return;
        }

        if (!$scope.user.cooker) {
          $state.go("dashboard.personal-info");
          return;
        }

        loadProducts(0, $scope.itemsPerPage);

        Customer.products.count({
          id: $scope.user.id
        },
          function (data) {
            $scope.totalProducts = data.count;
          },

          function (err) {
            ErrorTrackerService.error("Error occurred while counting products.", err);
          }
        );
      };

      start();

    }
  ])
