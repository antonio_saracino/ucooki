angular.module("ucookiApp")
    .controller('AdminCooksCtrl', ['$scope', '$filter', '$http', 'Customer', 'AuthService', 'ngToast', 'smoothScroll',
        function ($scope, $filter, $http, Customer, AuthService, ngToast, smoothScroll) {

            $scope.loading = false;
            $scope.cooks = {};
            $scope.data = {
                location: null
            }

            $scope.pagination = {
                currentPage: 1,
                itemsPerPage: 10,
                totalCooks: 0
            };

            $scope.filter = {
                certificated: null,
                text: null,
                lat: null,
                lng: null,
                distance: null,
                limit: $scope.pagination.itemsPerPage,
                skip: 0
            }

            $scope.pageChanged = function () {

                $scope.filter.skip = $scope.pagination.currentPage > 1 ? $scope.pagination.itemsPerPage * ($scope.pagination.currentPage - 1) : 0;
                loadCooks($scope.filter);
            };

            $scope.getPrincipalPicUrl = function (cook) {

                if (!cook || !cook._picture || !cook._picture.url) {
                    return "assets/img/cooker.svg";
                }

                return cook._picture.url;
            };

            $scope.getBackgroundImage = function(cook) {
                return {'background-image' : "url("+$scope.getPrincipalPicUrl(cook)+")"};
            }

            $scope.onLocationSelect = function (item, model, label) {
                if (item) {
                    $scope.filter.lat = item.geometry.location.lat;
                    $scope.filter.lng = item.geometry.location.lng;
                }
            }

            $scope.onLocationChange = function (value) {
                if (!value) {
                    // ho cancellato la location
                    $scope.filter.lat = null;
                    $scope.filter.lng = null;
                }
            }

            $scope.getLocation = function (val) {
                return $http.get('//maps.googleapis.com/maps/api/geocode/json', {
                    params: {
                        address: val,
                        sensor: false,
                        components: "country:it"
                    }
                }).then(function (response) {
                    return response.data.results.map(function (item) {
                        return item;
                    });
                });
            };

            $scope.refreshSearch = function () {
                $scope.filter.skip = 0;
                $scope.pagination.currentPage = 1;
                loadCooks($scope.filter);
            }

            $scope.saveProduct = function (cook) {
                if (!cook) {
                    return;
                }

                ngToast.dismiss();
                Customer.update({ where: { id: cook.id } }, {"productor_info.certificated" : cook.productor_info.certificated}, function (result) {

                    // tutto ok!!!
                    console.log(result);

                }, function (err) {
                    console.error(err);
                    ngToast.create({
                        className: 'danger',
                        content: "Errore durante il salvataggio",
                        timeout: 5000
                    });
                });
            }

            /*************************** Utility **********************************/

            function loadCooks(params) {

                // status, active, lat, lng, distance, limit, skip
                // console.log("parametri: ", params);
                $scope.loading = true;

                Customer.getCookList(params,
                    function (response) {
                        $scope.cooks = response.result.cooks;
                        $scope.pagination.totalCooks = response.result.total;
                        console.log("result: ", response.result);
                        $scope.loading = false;
                        var element = document.getElementById("cook-list");
                        smoothScroll(element, { offset: 100 });
                    },
                    function (err) {
                        console.error("error reading data: ");
                        console.error(err);
                        $scope.loading = false;
                    });
            };

            var init = function () {
                if (!AuthService.isAuthenticated()) {
                    $state.go("login", {
                        return: encodeURIComponent($location.url())
                    });
                    return;
                }

                loadCooks($scope.filter);
            }

            init();

        }
    ])