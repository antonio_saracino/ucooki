angular.module("ucookiApp")
  .controller('AdminProductsCtrl', ['$scope', '$filter', '$http', 'Product', 'AuthService', 'ngToast', 'smoothScroll', 'ModalService',
    function ($scope, $filter, $http, Product, AuthService, ngToast, smoothScroll, ModalService) {

      $scope.loading = false;
      $scope.docs = {};
      $scope.data = {
        location: null
      }

      $scope.pagination = {
        currentPage: 1,
        itemsPerPage: 10,
        totalProducts: 0
      };

      $scope.filter = {
        status: 0,
        active: null,
        featured: null,
        text: null,
        lat: null,
        lng: null,
        distance: null,
        day: null,
        meal: null,
        limit: $scope.pagination.itemsPerPage,
        skip: 0
      }

      $scope.pageChanged = function () {

        $scope.filter.skip = $scope.pagination.currentPage > 1 ? $scope.pagination.itemsPerPage * ($scope.pagination.currentPage - 1) : 0;
        loadProducts($scope.filter);
      };

      $scope.getPrincipalPicUrl = function (images) {

        if (!images || images.length <= 0 || !images[0]) {
          return null;
        }

        var filter = $filter('cloudinaryResize');

        for (var index in images) {
          if (images[index].principal === true) {
            return filter(images[index].url, 'h_450,q_80');
          }
        }
        return null;
      };

      $scope.onLocationSelect = function (item, model, label) {
        if (item) {
          $scope.filter.lat = item.geometry.location.lat;
          $scope.filter.lng = item.geometry.location.lng;
        }
      }

      $scope.onLocationChange = function (value) {
        if (!value) {
          // ho cancellato la location
          $scope.filter.lat = null;
          $scope.filter.lng = null;
        }
      }

      $scope.getLocation = function (val) {
        return $http.get('//maps.googleapis.com/maps/api/geocode/json', {
          params: {
            address: val,
            sensor: false,
            components: "country:it"
          }
        }).then(function (response) {
          return response.data.results.map(function (item) {
            return item;
          });
        });
      };

      $scope.refreshSearch = function () {
        $scope.filter.skip = 0;
        $scope.pagination.currentPage = 1;
        loadProducts($scope.filter);
      }

      $scope.saveProduct = function (product) {
        if (!product) {
          return;
        }

        ngToast.dismiss();
        var p = angular.copy(product);
        delete p._id;
        Product.update({ where: { id: product._id } }, p, function (product) {

          // tutto ok!!!

        }, function (err) {
          console.error(err);
          ngToast.create({
            className: 'danger',
            content: "Errore durante il salvataggio",
            timeout: 5000
          });
        });
      }

      $scope.requestUpdateStatus = function (product, value, orig) {

        if (value == orig) {
          return;
        }

        var msg = "Sicuro di voler attivare questo piatto?";
        switch (value) {
          case 0:
            msg = "Sicuro di voler mettere in attesa il piatto?";
            break;
          case 2:
            msg = "Sicuro di voler rifiutare questo piatto?";
            break;
        }

        ModalService.showModal({
          backdrop: true
        },
          {
            bodyText: msg,
            headerText: "Continuare?",
            closeButtonText: 'Annulla',
            actionButtonText: 'Si, sicuro'
          }).then(
          function () {
            // ok, voglio cambiare lo stato del piatto
            $scope.updateStatus(product);
          },
          function () {
            // no, ferma tutto, ho cambiato idea
            product.status = orig;
          });
      }

      $scope.updateStatus = function (product) {
        if (!product) {
          return;
        }

        ngToast.dismiss();
        Product.changeStatus({ id: product._id, status: product.status }, function (product) {

          $scope.product = product;

        }, function (err) {
          console.error(err);
          ngToast.create({
            className: 'danger',
            content: "Errore durante il salvataggio",
            timeout: 5000
          });
        });
      }

      /*************************** Utility **********************************/

      function loadProducts(params) {

        // status, active, lat, lng, distance, limit, skip
        // console.log("parametri: ", params);
        $scope.loading = true;

        Product.getList(params,
          function (response) {
            $scope.docs = response.result.docs;
            $scope.pagination.totalProducts = response.result.total;
            console.log("result: ", response.result);
            $scope.loading = false;
            var element = document.getElementById("product-list");
            smoothScroll(element, { offset: 100 });
          },
          function (err) {
            console.error("error reading data: ");
            console.error(err);
            $scope.loading = false;
          });
      };

      var init = function () {
        if (!AuthService.isAuthenticated()) {
          $state.go("login", {
            return: encodeURIComponent($location.url())
          });
          return;
        }

        loadProducts($scope.filter);
      }

      init();

    }
  ])
