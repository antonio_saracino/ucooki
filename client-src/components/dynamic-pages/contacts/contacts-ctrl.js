angular.module('ucookiApp')
  .controller('ContactsCtrl', ['$scope', '$location', '$http', '$state', 'ngToast', 'smoothScroll', function($scope, $location, $http, $state, ngToast, smoothScroll) {

    $scope.loading = false;
    $scope.data = {};

    $scope.submit = function() {

      if (!$scope.data.name) {
        ngToast.create({
          className: 'danger',
          content: "Inserisci il nome",
          timeout: 5000
        });
        return;
      }

      if (!$scope.data.email) {
        ngToast.create({
          className: 'danger',
          content: "Inserisci una email valida",
          timeout: 5000
        });
        return;
      }

      if (!$scope.data.message) {
        ngToast.create({
          className: 'danger',
          content: "Inserisci un messaggio",
          timeout: 5000
        });
        return;
      }

      $scope.loading = true;
      var element = document.getElementById('contacts');
      smoothScroll(element, {offset : 100});

      $http.post("/send-message", {
          name: $scope.data.name,
          email: $scope.data.email,
          phone: $scope.data.phone,
          msg: $scope.data.message
        })
        .then(function(result) {
          $scope.loading = false;
          ngToast.create({
            className: 'success',
            content: "Messaggio inviato, grazie :)",
            timeout: 5000
          });
          $state.go("home");
        }).catch(function(err) {
          $scope.loading = false;
          ngToast.create({
            className: 'danger',
            content: "Si è verificato un errore durante l'invio",
            timeout: 5000
          });
          console.error(err);
        });
    };
  }]);
