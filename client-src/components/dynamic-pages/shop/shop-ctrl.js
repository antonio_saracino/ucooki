angular.module("ucookiApp")
  .controller("ShopCtrl", ['$scope', '$rootScope', '$state', '$window', '$stateParams', '$filter', '$location', 'CartService', 'Customer', 'Product', 'ModalService', 'SearchService', 'ngToast', 'smoothScroll', 'AddthisService', 'TrackerService', 'GoogleMapService', 'MetadataService', 'ErrorTrackerService',
    function ($scope, $rootScope, $state, $window, $stateParams, $filter, $location, CartService, Customer, Product, ModalService, SearchService, ngToast, smoothScroll, AddthisService, TrackerService, GoogleMapService, MetadataService, ErrorTrackerService) {

      $scope.message = {
        distanceOnMap: "Clicca e verifica la distanza sulla mappa"
      };
      $scope.cart = CartService.get();
      $scope.showMap = false;
      $scope.loading = false;
      $scope.product = {};
      $scope.productor = {};
      $scope.slides = [];
      $scope.alerts = [];

      $scope.addToCart = function (product, productor) {

        $scope.alerts = [];

        if ($rootScope.user && $rootScope.user.id == productor.id) {
          ngToast.create({
            className: 'info',
            content: "Non puoi ordinare i tuoi stessi piatti",
            timeout: 5000
          });
          return;
        }

        // If the product was already added, change only item.quantity
        if ($scope.cart.orders) {

          for (var i in $scope.cart.orders) {
            var order = $scope.cart.orders[i];
            for (var ii in order.items) {
              var item = order.items[ii];
              if (item.product_id === product.id) {
                if (item.quantity + 1 > product.max_portion) {
                  ngToast.create({
                    className: 'info',
                    content: "Raggiunto numero massimo di porzioni",
                    timeout: 5000
                  });
                  return;
                }
                item.quantity++;
                CartService.save($scope.cart);

                TrackerService.addToCart($scope.product.id, $scope.product.price);

                gotToCart();
                ngToast.create({
                  className: 'success',
                  content: "Prodotto aggiunto al carrello",
                  timeout: 5000
                });

                return;
              }
            }
            ;

          }
          ;
        }

        TrackerService.startAddToCart($scope.product.id, $scope.product.price);

        var modalInstance = ModalService.showModal({
          controller: 'OrderFormDialogCtrl',
          resolve: {
            product: function () {
              return product;
            },
            alerts: function () {
              return getAlerts(product);
            }
          }
        });

        modalInstance.then(function (item) {
          if (!item) {
            return;
          }
          //          console.log("item selezionato: ", item);

          if (!$scope.cart.orders || $scope.cart.orders.length <= 0) {
            $scope.cart.orders = [{
              items: [],
              delivery: false,
              delivery_date: null,
              date: null,
              delivery_price: 0,
              total: 0,
              buyer_id: null,
              productor_id: null
            }]
          }

          // prendo il primo, per ora gestisco solo un ordine
          var order = $scope.cart.orders[0];



          order.items.push(item);
          correctOrder(order, item);
          updateCart($scope.cart, product, productor);

          TrackerService.addToCart($scope.product.id, $scope.product.price);

        }, function () {
        })

      };

      $scope.getPortions = function(item) {
        var portions = [];

        for (var i = item.product.min_portion; i <= item.product.max_portion; i++) {
          portions.push(i);
        }

        return portions;
      };

      $scope.onQuantityChange = function (item, index) {

        if (isNaN(item.quantity)) {
          item.quantity = item.product.min_portion;
        }

        if (item.quantity > item.product.max_portion) {
          item.quantity = item.product.max_portion;
        }

        if (item.quantity < item.product.min_portion) {
          item.quantity = item.product.min_portion;
        }

        CartService.save($scope.cart);
      };

      $scope.removeItem = function (orderIndex, itemIndex) {
        CartService.remove($scope.cart, orderIndex, itemIndex);
      }

      $scope.clearCart = function () {
        CartService.clear();
      };

      $rootScope.$on("cart-changed", function (cart) {
        $scope.cart = CartService.get();
      });

      $scope.onPictureClick = function (picture) {
        console.log("picture: ", picture);
      }

      $scope.getHeaderImage = function () {

        var filter = $filter('cloudinaryResize');

        if ($scope.product && $scope.product._pictures) {
          for (var i = 0; i < $scope.product._pictures.length; i++) {
            var picture = $scope.product._pictures[i];
            if (picture.principal == true) {
              return "url(" + filter(picture.url, 'h_650,q_80') + ")";
            }
          }
        }

        return "";
      };

      $scope.checkout = function () {
        if (!$scope.cart || !$scope.cart.orders) {
          return;
        }

        var ids = [];
        var numItems = 0;
        var total = 0;

        for (var i in $scope.cart.orders) {
          var order = $scope.cart.orders[i];
          total += order.total;
          for (var ii in order.items) {
            var item = order.items[ii];
            ids.push(item.product.id);
            numItems++;
          }
        }

        TrackerService.initiateCheckout(ids, total, numItems);
      }

      $scope.getBuyerUrl = function (rating) {
        return $state.href("customer", {
          customerId: rating.buyer_id
        }, {
          absolute: true
        });
      }

      $scope.showOtherRatings = function () {
        $scope.allRatings = true;
      }

      $scope.getWeight = function (product, portion) {
        if (!product || !product.weight || !portion) {
          return null;
        }
        return parseFloat((product.weight * portion).toFixed(1));
      };

      $scope.toggleMap = function () {
        $scope.showMap = !$scope.showMap;
      };

      //*************************** HELPER FUNCTION *********************************//

      var gotToCart = function () {

        var width = $window.innerWidth;

        if (width < 992) {
          var element = document.getElementById('cart_box');
          smoothScroll(element, {offset: 100});
        }
      }

      var getProduct = function (productId, success, error) {
        $scope.loading = true;
        $scope.product = Product.findById({
            id: productId
          },
          function (product) {
            for (var index in product._pictures) {
              $scope.slides.push({
                image: product._pictures[index].url
              });
            }
            if (success) {
              success(product);
            }
            // console.log(product);
            $scope.loading = false;
          },
          function (err) {
            console.log(err);

            if (error) {
              error(err);
            }
            $scope.loading = false;
          });
      };

      var getAlerts = function (product) {

        var alerts = [];
        if ($scope.cart.orders.length > 0) {
          // considero solo il primo
          var order = $scope.cart.orders[0];
          // If different productor, show a dialog for confirmation
          if (order.productor_id !== product.customer_id) {
            alerts = [{
              id: "productor_different",
              msg: "Ordinando da un altro cuoco i prodotti attualmente nel carrello verranno cancellati",
              type: "danger"
            }];
          } else if (order.items && order.items.length > 0) {
            var item = order.items[0];
            alerts = [{
              id: "different-dates",
              msg: "Se si sceglie una data diversa dal " + $filter('date')(item.delivery_date, "d MMM 'alle' HH:mm") + " verranno eliminati gli ordini attualmente all'interno del carrello",
              type: "danger"
            }]
          }
        }

        return alerts;
      }

      var correctOrder = function (order, lastItem) {
        if (!order || !order.items || order.items.length <= 0 || !lastItem) {
          return;
        }

        var delivery = false;
        var justPickup = false;
        // var orderCopy = angular.copy(order);

        if (lastItem && order.items.length > 1) {
          // cancello tutti gli item che hanno un orario differente
          // dall'ultimo ordine inserito oppure appartengono ad un altro cuoco
          for (var index = order.items.length -1; index >= 0; index--) {
            var item = order.items[index];

            if (item.delivery_date.getTime() != lastItem.delivery_date.getTime() ||
                item.product.productor.id != lastItem.product.productor.id) {
              // rimuovo l'item
              order.items.splice(index, 1);
            }
          }
        }

        delivery = lastItem.delivery;

        for (var index in order.items) {
          // verifico che sia consistente il tipo di spedizione
          var item = order.items[index];
          if (!item.product.delivery) {
            // in questo caso il piatto può essere
            // solo ritirato, quindi tutto l'ordine
            // diventa solo ritiro
            justPickup = true;
            break;
          }
        }

        if (justPickup || !delivery) {
          // l'ordine diventa da ritirare
          order.delivery = false;
          for (var index in order.items) {
            var item = order.items[index];
            item.delivery = false;
          }
        } else if (delivery) {
          // l'ordine diventa da trasporto
          order.delivery = true;
          for (var index in order.items) {
            var item = order.items[index];
            item.delivery = true;
          }
        }

        // genero l'id
        for (var index in order.items) {
          // verifico che sia consistente il tipo di spedizione
          var item = order.items[index];
          item.id = index;
        }

      }

      var updateCart = function (cart, product, productor) {

        gotToCart();

        for (var index in cart.orders) {

          var order = cart.orders[index];

          order.total = 0;
          order.delivery_price = 0;
          order.delivery_date = null;
          order.date = new Date();
          order.productor_id = productor.id;
          if ($rootScope.user) {
            // se siamo loggati
            order.buyer_id = $rootScope.user.id;
          }
          if (!order.delivery) {
            // è previsto il ritiro --> imposto l'indirizzo del cooker
            order.location = productor.kitchen_location_info;
          } else {
            delete order.location;
          }

          var item = order.items[0]; // ne prendo uno qualsiasi
          if (item) {
            order.delivery_date = item.delivery_date;
          }
        }

        CartService.save($scope.cart);

      }

      var init = function () {

        $scope.buyer_search_address = SearchService.get().address;

        getProduct($stateParams.productId, function (product) {

          for (var i in product._pictures) {
            if (product._pictures[i].principal) {
              MetadataService.setImage(product._pictures[i].url);
              break;
            }
          }

          $scope.productor = product.productor;

          GoogleMapService.decodeAddress(product.productor.kitchen_location_info.address).then(function (result) {
            product.productor.kitchen_location_info.city = result.city;
            product.productor.kitchen_location_info.region = result.region;

            MetadataService.setDescription(product.description);
            MetadataService.setTitle(product.productor.kitchen_location_info.city + " - " + product.name + " - Ucooki");

          });

          $scope.current = {
            center: {
              latitude: product.productor.kitchen_location.lat,
              longitude: product.productor.kitchen_location.lng
            }
          };

          AddthisService.updateAddthis(product.name, product.description);
          TrackerService.productView($location.absUrl(), product.name, product.productor);


        }, function (err) {
          ErrorTrackerService.warn("Error occurred during product loading.", err);
          $state.go("home");
        });

      };

      init();

    }
  ]);
