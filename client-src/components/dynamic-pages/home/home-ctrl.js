/**
 * Created by nailon on 07/09/15.
 */
angular.module("ucookiApp")
  .controller("HomeProductCtrl", ['$scope', '$http', '$rootScope', '$window', 'Product', 'Customer', 'Order', '$q', 'GoogleMapService', 'CartService', 'SearchService', 'ngToast', 'smoothScroll', '$filter', '$timeout', 'TrackerService', 'ModalService', 'uiGmapGoogleMapApi', 'ErrorTrackerService',
    function ($scope, $http, $rootScope, $window, Product, Customer, Order, $q, GoogleMapService, CartService, SearchService, ngToast, smoothScroll, $filter, $timeout, TrackerService, ModalService, uiGmapGoogleMapApi, ErrorTrackerService) {

      $scope.fetching = false;
      $scope.statistics = {};
      $scope.default = {};
      $rootScope.disabled = false;


      $scope.autocompleteOptions = {
        componentRestrictions: {
          country: 'it'
        }
      };

      $scope.getPrincipalPicUrl = function (images) {

        if (!images || images.length <= 0 || !images[0]) {
          return null;
        }

        var filter = $filter('cloudinaryResize');

        for (var index in images) {
          if (images[index].principal === true) {
            return filter(images[index].url, 'h_450,q_80');
          }
        }
        return null;
      };

      $scope.searchMore = function () {

        console.log("fetching");

        $scope.fetching = true;
        var lat = $scope.lat;
        var lng = $scope.lng;
        var address = $rootScope.search.address;
        if (!lat || !lng || !address) {
          var search = SearchService.get();
          lat = search.lat;
          lng = search.lng;
          address = search.address;
        }

        TrackerService.searchMore(address, $rootScope.products.length, $scope.itemsPerPage, $rootScope.homeData.filter.day, $rootScope.homeData.filter.meal);

        loadProducts($rootScope.products.length, $scope.itemsPerPage, lat, lng, $rootScope.homeData.filter.day, $rootScope.homeData.filter.meal, function (products) {
          if (!products || products.length <= 0) {
            $scope.disabled = true;
            $scope.fetching = false;
          }
          if (products) {
            $rootScope.products = $rootScope.products.concat(products);
            $timeout(function () {
              $scope.fetching = false;
            })
          }
        });
      }

      $scope.searchByDay = function () {

        showLoading();
        var lat = $scope.lat;
        var lng = $scope.lng;
        var address = $rootScope.search.address;
        if (!lat || !lng || !address) {
          var search = SearchService.get();
          lat = search.lat;
          lng = search.lng;
          address = search.address;
        }

        TrackerService.searchByDay(address, $rootScope.products.length, $scope.itemsPerPage, $rootScope.homeData.filter.day, $rootScope.homeData.filter.meal);

        $scope.disabled = false;
        loadProducts(0, $scope.itemsPerPage, lat, lng, $rootScope.homeData.filter.day, $rootScope.homeData.filter.meal, function (products) {
          if (products) {
            $rootScope.products = products;
            $timeout(function () {
              hideLoading();
            })
          } else {
            hideLoading();
          }
        });
      }

      $scope.searchProducts = function (address) {

        function handlerLoadProducts(value) {
          updatePlace(value.formatted_address);

          $scope.lat = value.coordinates.lat;
          $scope.lng = value.coordinates.lng;

          SearchService.save(value.formatted_address, $scope.lat, $scope.lng);
          TrackerService.search(value);

          var element = document.getElementById('product-result');
          smoothScroll(element, {offset: 100});

          $scope.fetching = true;
          $scope.disabled = false;
          loadProducts(0, $scope.itemsPerPage, $scope.lat, $scope.lng, $scope.homeData.filter.day, $scope.homeData.filter.meal, function (products) {
            if (products) {
              $rootScope.products = products;
            }

            $timeout(function () {
              $scope.fetching = false;
            })
          });
        }

        ngToast.dismiss();

        $rootScope.products = undefined;

        if (!address || address == "") {
          // ricerca di default
          handlerLoadProducts($scope.default);
          return;

        }

        GoogleMapService.getLocation(address).then(function (value) {

          handlerLoadProducts(value);

        }).catch(function (error) {
          ErrorTrackerService.log('unable to make a search: address invalid.', error);
          updatePlace("");
          $scope.homeData.searchDone = false;

          ngToast.create({
            className: 'danger',
            content: "Ricerca non riuscita.<br>L'indirizzo è corretto?",
            timeout: 5000
          });

          return;
        })
      };

      $scope.getItemPerLine = function () {
        var width = $window.innerWidth;

        if (width >= 1200) {
          return 3;
        }

        if (width >= 990) {
          return 2;
        }

        return 1;
      }

      $scope.getDistanceScroll = function () {
        var width = $window.innerWidth;

        if (width >= 1200) {
          return 3;
        }

        if (width >= 990) {
          return 5;
        }

        return 10;
      }

      $scope.getTrackProductId = function (row) {
        var id = "row_"

        for (var i in row) {
          id += row[0].product._id + "_";
        }
        return id;
      }

      $scope.getTrackCookId = function (row) {
        var id = "row_"

        for (var i in row) {
          id += row[0].id + "_";
        }
        return id;
      }


      /******************************************************/

      function loadProducts(skip, limit, lat, lng, day, meal, cb) {

        var params = {
          lat: lat,
          lng: lng,
          skip: skip,
          limit: limit,
          day: day,
          meal: meal
        };

        $rootScope.search.filters = {
          lat: lat,
          lng: lng,
          meal: meal,
          date: $rootScope.homeData.filter.date
        };

        $scope.noMore = false;

        Product.findProductNear(params,
          function (result) {
            $scope.homeData.searchDone = true;
            var products = result.result.docs;
            if (products.length <= 0) {
              $scope.noMore = true;
            }

            cb(products);
          },
          function (err) {
            ErrorTrackerService.error("error while find product near", err);
            $scope.homeData.searchDone = false;
            cb(null);
          });
      }

      function loadFeaturedProducts() {

        var now = moment().tz("Europe/Rome");
        var meal = "lunch";
        var day = 0;
        var msg = "I nostri consigli oggi a pranzo";
        if (now.hour() > 10 && now.hour() <= 19) {
          // cerchiamo per cena
          meal = "dinner"
          msg = "I nostri consigli stasera a cena";
        } else if (now.hour() > 19) {
          // andiamo a domani
          day = 1;
          msg = "I nostri consigli domani a pranzo";
        }

        var lat = $scope.default.coordinates.lat;
        var lng = $scope.default.coordinates.lng

        var params = {
          lat: lat,
          lng: lng,
          limit: 6,
          day: day,
          meal: meal
        }

        Product.findFeaturedNear(params, function (result) {

          var docs = result.result.docs;

          if (result.result.day <= -1) {
            // non ho trovato piatto disponibili per il giorno selezionato
          } else {
            $rootScope.featuredMessage = msg;
          }

          $rootScope.featuredProducts = docs;

        });
      }

      function getItemPerPage() {
        var width = $window.innerWidth;

        if (width >= 1200) {
          return 24;
        }

        return 12;
      }

      function getCooks() {

        var lat = $scope.default.coordinates.lat;
        var lng = $scope.default.coordinates.lng

        var filter = {
          lat: lat,
          lng: lng,
          limit: 6
        }

        Customer.getActiveCookList(filter, function (response) {

          $rootScope.cooks = response.result.cooks;

        }, function (err) {
          ErrorTrackerService.error("Error occurred while loading cooks", err);
        });

      }

      function createFilter() {
        var filters = [{
          day: -1,
          meal: null,
          name: "Scegli quando mangiare",
          date: null
        }];
        moment.locale("it");
        var now = moment().tz("Europe/Rome");
        // creo la lista di giorni da filtrare.
        // la variabile day indica di quanti giorni vogliamo andare
        // in avanti
        for (var day = 0; day <= 7; day++) {

          var current = now.clone().add(day, "day");
          var name = current.format("dddd D");

          if (day == 0) {
            name = "Oggi";
          } else if (day == 1) {
            name = "Domani";
          }

          if (day != 0 || now.hour() <= 15) {
            // è inutile cercare per pranzo se ora è già troppo tardi
            filters.push({
              day: day,
              meal: "lunch",
              name: name + " a pranzo",
              date: current.unix()
            });
          }

          if (day != 0 || now.hour() <= 22) {
            // è inutile cercare per cena se ora è già troppo tardi
            filters.push({
              day: day,
              meal: "dinner",
              name: name + " a cena",
              date: current.unix()
            });
          }

        }

        return filters;

      }

      function showLoading() {
        $rootScope.$broadcast("showSpinner");
      }

      function hideLoading() {
        $rootScope.$broadcast("hideSpinner");
      }

      function updatePlace(address) {
        $('#pac-input').val(address);
        $rootScope.search.address = address;
      }

      function fetchStatistics() {
        Customer.count(function (result) {
          $scope.statistics.customers = result.count;
        }, function (err) {
          console.error(err);
        });

        Order.countDishes(function (result) {
          $scope.statistics.dishes = result.count;
        }, function (err) {
          console.error(err);
        })

        Product.count(function (result) {
          $scope.statistics.products = result.count;
        }, function (err) {
          console.error(err);
        })
      }

      function loadDefaultCoordinates() {

        $scope.default = {
          city: "Bologna",
          province: "Bo",
          region: "Emilia-Romagna",
          formatted_address: "Bologna, Italia",
          coordinates: GoogleMapService.BOLOGNA_COORD
        }


        return $q(function (resolve, reject) {

          function coordinatesResolve(result) {
            $timeout.cancel(timeout);
            resolve($scope.default);
          }

          function errorHandler(error) {
            console.error("Error occurred during coordinates loading: ", error);
            coordinatesResolve($scope.default);
            console.log("Default coordinates: ", $scope.default);
          }

          var options = {
            enableHighAccuracy: false,
            timeout: 20000
          };

          if ($window.navigator.geolocation) {
            $window.navigator.geolocation.getCurrentPosition(function (position) {

              console.log(position);

              var coordinates = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
              }

              GoogleMapService.decodeAddress(coordinates).then(function (result) {

                $scope.default = result;
                coordinatesResolve($scope.default);

                console.log("coordinates received: ", $scope.default);

              }, function (error) {
                errorHandler(error);
              });

            }, function (error) {
              errorHandler(error);
            }, options);
          } else {
            errorHandler(new Error("Geolocation doesn't work"));
          }


          var timeout = $timeout(function () {
            errorHandler(new Error("Unable to get a response (firefox-bug???)"));
          }, options.timeout + 10);

        });
      }


      init = function () {

        // fetchStatistics();

        loadDefaultCoordinates().then(function (result) {
          loadFeaturedProducts();

          if (!$rootScope.cooks) {
            getCooks();
          }
        });

        $rootScope.homeData = $rootScope.homeData || {};
        $rootScope.search = $rootScope.search || {};

        if (!$rootScope.search.address) {
          updatePlace("");
        } else {
          updatePlace($rootScope.search.address);
        }

        $scope.itemsPerPage = getItemPerPage();

        if (!$rootScope.homeData.filter) {
          // valore di dafault
          $rootScope.homeData.filters = createFilter();
          $rootScope.homeData.filter = $rootScope.homeData.filters[0];
        }

        uiGmapGoogleMapApi.then(function (maps) {
          // abilito il campo di ricerca
          $window.gmapFunctions();
        });

      }

      init();

    }
  ]);
