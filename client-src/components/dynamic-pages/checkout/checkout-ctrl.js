angular.module('ucookiApp')
  .controller('CheckoutCtrl', ['$scope', '$rootScope', '$state', '$location', 'CartService', 'Order', 'Coupon', 'AuthService', 'ngToast', 'smoothScroll', 'Customer', 'ModalService', 'TrackerService', 'ErrorTrackerService', 'GoogleMapService',
    function ($scope, $rootScope, $state, $location, CartService, Order, Coupon, AuthService, ngToast, smoothScroll, Customer, ModalService, TrackerService, ErrorTrackerService, GoogleMapService) {

      $scope.loading = false;
      $scope.loadingCoupon = false;
      $scope.forceQuit = false;
      $scope.cart = CartService.get();

      $scope.deleteItem = function (orderIndex, itemIndex) {
        CartService.remove($scope.cart, orderIndex, itemIndex);
        manageCart();
      };

      $scope.ckalerts = [];

      $scope.confirmOrder = function () {
        $scope.ckalerts = [];
        console.log($scope.cart);

        var now = new Date();

        for (var i in $scope.cart.orders) {

          var order = $scope.cart.orders[i];

          // dopo un'ora elimino l'ordine
          var limitDate = new Date(order.date);
          limitDate.setHours(order.date.getHours() + 1);

          if (now > limitDate) {
            // scarto l'ordine perchè troppo vecchio
            ngToast.create({
              className: 'danger',
              content: 'L\'ordine non è più valido, è passato troppo tempo :(',
              timeout: 10000
            });
            continue;
          }

          // prima di salvare verifico se l'utente è loggato
          if (!order.buyer_id) {
            // se siamo loggati
            if (!$rootScope.user) {
              ErrorTrackerService.error("User not logged in.");
              ngToast.create({
                className: 'danger',
                content: 'Errore durante il salvataggio',
                timeout: 5000
              });
              return;
            } else {
              order.buyer_id = $rootScope.user.id;
              saveOrder(order);
            }
          } else {
            saveOrder(order);
          }

        }

        CartService.clear();
        manageCart();

      };

      $scope.addAddress = function (index) {
        var modalInstance = ModalService.showModal({
          controller: 'AddAddressModalCtrl'
        });

        modalInstance.then(function (data) {

          var order = $scope.cart.orders[index];
          if (data) {
            order.location = {
              address: data.address,
              street_number: data.streetNumber,
              note: data.note
            }


          }

        }, function () { });
      }

      $scope.addPhone = function () {
        var modalInstance = ModalService.showModal({
          controller: 'AddPhoneModalCtrl',
          resolve: {
            phone: function () {
              return $scope.user.phone;
            }
          }
        });

        modalInstance.then(function (phone) {

          var user = {
            phone: phone
          }

          $scope.numberLoading = true;

          Customer.prototype$updateAttributes({
            id: $scope.user.id
          }, user,
            function (result) {
              $scope.numberLoading = false;
              console.log("salvato: ", result);
              $scope.user.phone = phone;

              ngToast.create({
                className: 'success',
                content: "Il numero è stato <br>correttamente salvato",
                timeout: 5000
              });

            },
            function (err) {
              $scope.numberLoading = false;
              err.phone = phone;
              ErrorTrackerService.error("Error occurred while saving phone number.", err);
              ngToast.create({
                className: 'danger',
                content: "Errore nel salvataggio",
                timeout: 5000
              });

            });

        }, function () { });
      }

      $scope.useCoupon = function (code) {
        if (!code) {
          return;
        }

        $scope.loadingCoupon = true;

        Coupon.find({
          filter: {
            where: {
              name: code.toUpperCase()
            }
          }
        },
          function (result) {

            $scope.loadingCoupon = false;

            if (!result[0]) {
              ngToast.create({
                className: 'info',
                content: 'Il coupon inserito non è valido',
                timeout: 5000
              });
              return;
            }

            $scope.coupon = result[0];
            for (var i in $scope.cart.orders) {
              var order = $scope.cart.orders[i];

              order._coupon = $scope.coupon;
              order.total = order.total - (order.total * (order._coupon.discount / 100));
            }

          },
          function (err) {
            $scope.loadingCoupon = false;
            err.coupon = code;
            ErrorTrackerService.error("Error occurred while finding coupon.", err);
            ngToast.create({
              className: 'danger',
              content: 'Si è verificato un errore durante la ricerca del coupon',
              timeout: 5000
            });
          });
      }

      $scope.getAlertMessage = function (order) {
        if (!$rootScope.user) {
          return "";
        }
        var dataToInsert = "";
        if (!order.location) {
          dataToInsert = "l'indirizzo";
        }
        if (!$rootScope.user.phone) {
          dataToInsert += dataToInsert.length <= 0 ? "il telefono" : " ed il telefono";
        }

        return "Attenzione! per completare l'ordine inserire " + dataToInsert;
      }

      /****************************************************************/

      var saveOrder = function (order) {
        $scope.loading = true;

        var element = document.getElementsByTagName("body")[0];
        smoothScroll(element, { offset: 100 });

        Order.create(order, function (result) {
          console.log("saved: ", result);

          var ids = [];
          for (var ii in order.items) {
            var item = order.items[ii];
            ids.push(item.product.id);
          }

          TrackerService.purchase(ids, order.total, order.items.length, result.id, order);

          ngToast.create({
            className: 'success',
            content: 'Il tuo ordine è stato correttamente inviato.<br>Aspetta la conferma del cuoco entro la prossima ora.',
            timeout: 5000
          });

          $scope.loading = false;
          $scope.orderSent = true;

        }, function (err) {
          err.order = order;
          ErrorTrackerService.error("Error occurred while order creation.", err);
          ngToast.create({
            className: 'danger',
            content: 'Errore durante il salvataggio',
            timeout: 5000
          });
          $scope.loading = false;
        });
      }

      var manageCart = function () {

        $scope.cart = CartService.get();

        if ($scope.cart && $scope.cart.orders && $scope.cart.orders.length > 0) {
          // verifico che il prodotto non abbia consumatore = produttore
          var orders = angular.copy($scope.cart.orders);
          orders.forEach(function (order, index) {
            if (order.productor_id == $rootScope.user.id) {
              // elimino l'ordine
              ngToast.create({
                className: 'danger',
                content: 'Non puoi ordinare i tuoi stessi piatti',
                timeout: 10000
              });
              $scope.cart.orders.splice(index, 1);
            }
          })

          CartService.save($scope.cart);
          console.log($scope.cart);
        }

      }

      var init = function () {

        if (!AuthService.isAuthenticated()) {

          AuthService.user().then(function () {
            $scope.ckalerts = [];
          }, function () {
            $state.go("login", {
              return: encodeURIComponent($location.url())
            });
          });
        } else {

          manageCart();


          // controllo che non venga abbandonato l'ordine per sbaglio
          $scope.$on("$stateChangeStart", function (event, next, current) {

            if ($scope.cart.orders <= 0 || $scope.forceQuit) {
              // posso tranquillamente uscire
              return;
            }

            ModalService.showModal({
              backdrop: true
            }, {
                bodyText: "Non hai completato l'ordine, sicuro di voler cambiare pagina?",
                headerText: "Continuare?",
                closeButtonText: 'Annulla',
                actionButtonText: 'Si, cambia pagina'
              }).then(
              function () {
                // ok, voglio andare avanti
                $scope.forceQuit = true;
                $state.go(next.name, current);
              },
              function () {
                // no, ferma tutto, voglio salvare le modifiche
              });

            event.preventDefault();
          });


        }
      }

      init();

    }
  ]);
