angular.module('ucookiApp')
  .controller('SubscriberCtrl', ['$scope', '$location', '$http', '$state', 'ngToast', 'Subscriber', 'smoothScroll', 'GoogleMapService',
    function ($scope, $location, $http, $state, ngToast, Subscriber, smoothScroll, GoogleMapService) {

      $scope.loading = false;
      $scope.data = {
        cooker: false
      };

      $scope.submit = function () {

        if (!$scope.data.name) {
          ngToast.create({
            className: 'danger',
            content: "Inserisci il nome",
            timeout: 5000
          });
          return;
        }

        if (!$scope.data.email) {
          ngToast.create({
            className: 'danger',
            content: "Inserisci una email valida",
            timeout: 5000
          });
          return;
        }

        if ($scope.data.email != $scope.data.confirm) {
          ngToast.create({
            className: 'danger',
            content: "Le email non corrispondono",
            timeout: 5000
          });
          return;
        }

        if (!$scope.data.city) {
          ngToast.create({
            className: 'danger',
            content: "Inserire una città",
            timeout: 5000
          });
          return;
        }

        $scope.loading = true;

        var element = document.getElementById('subscriber');
        smoothScroll(element, { offset: 100 });

        GoogleMapService.getLocation($scope.data.city)
          .then(function (value) {

            if (!value.city || !value.province) {
              $scope.loading = false;
              ngToast.create({
                className: 'danger',
                content: "Città non riconosciuta",
                timeout: 5000
              });
              return;
            }

            var subscriber = {
              name: $scope.data.name,
              email: $scope.data.email.toLowerCase(),
              city: value.city,
              province: value.province,
              cooker: $scope.data.cooker
            }

            Subscriber.save(subscriber, function (subscriber) {

              $scope.loading = false;
              ngToast.create({
                className: 'success',
                content: "Registrazione avvenuta con successo, grazie :)",
                timeout: 5000
              });
              $state.go("home");


            }, function (err) {

              var msg = "Si è verificato un errore durante la registrazione";
              if (err.status == 422) {
                msg = "Email già registrata";
              }

              $scope.loading = false;
              ngToast.create({
                className: 'danger',
                content: msg,
                timeout: 5000
              });
              console.error(err);
            });
          }).catch(function (error) {
            $scope.loading = false;
            console.log("location error: ", error);

            ngToast.create({
              className: 'danger',
              content: 'Inserisci una città valida',
              timeout: 5000
            });
          });

      };

      $scope.getLocation = function (val) {
        return $http.get('//maps.googleapis.com/maps/api/geocode/json', {
          params: {
            address: val,
            sensor: false,
            types: '(cities)',
            components: "country:it"
          }
        }).then(function (response) {
          return response.data.results.map(function (item) {
            return item.formatted_address;
          });
        });
      };

    }]);
