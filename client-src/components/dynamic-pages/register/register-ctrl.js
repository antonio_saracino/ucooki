/**
 * Created by nailon on 07/09/15.
 */
angular.module("ucookiApp")
  .controller("RegisterCtrl", ['$scope', '$state', 'Customer', 'ngToast', 'AuthService', 'smoothScroll', 'TrackerService', function($scope, $state, Customer, ngToast, AuthService, smoothScroll, TrackerService) {

    $scope.loading = false;
    $scope.customer = {};
    $scope.accept = false;
    $scope.customerBirthday = null;
    $scope.customer = {
      gender: "male",
      city: "Bo"
    }
    $scope.data = {
      confirmationEmail: "",
      confirmationPassword: ""
    }

    $scope.calendarStatus = {
      opened: false
    };

    $scope.register = function(customer) {

      ngToast.dismiss();

      if (!$scope.accept) {
        ngToast.create({
          className: 'danger',
          content: "per poterti registrare devi accettare Termini e Condizioni",
          timeout: 5000
        });
        return;
      }

      if (!validate(customer)) {
        return;
      }

      $scope.loading = true;
			var element = document.getElementById("register");
      smoothScroll(element, {offset : 100});

      customer.username = customer.email;
      Customer.create(customer, function(result) {
        $scope.loading = false;
        ngToast.create({
          className: 'success',
          content: "Registrazione avvenuta con successo.<br>Controlla la tua casella di posta per completare la registrazione",
          timeout: 10000
        });


				TrackerService.completeRegistration("local", $scope.customer.email);
        $state.go("login");

      }, function(err) 	{
        $scope.loading = false;
        if (err) {
          if (err.data && err.data.error && err.data.error.details && err.data.error.details.codes && err.data.error.details.codes.email[0] == "uniqueness") {
            ngToast.create({
              className: 'danger',
              content: "Indirizzo email già utilizzato",
              timeout: 5000
            });

          } else {
            ngToast.create({
              className: 'danger',
              content: "Si è verificato un errore durante il salvataggio",
              timeout: 5000
            });
          }

          console.error(err);
          return;
        }
      });
    };

    $scope.toggle = function() {
      $scope.calendarStatus.opened = !$scope.calendarStatus.opened;
    }

    /********************* private functions **************************/

    var validate = function(user) {

      if (user.email != $scope.data.confirmationEmail) {

        ngToast.create({
          className: 'danger',
          content: "Le mail non coincidono",
          timeout: 5000
        });
        return false;
      }

      if (user.password != $scope.data.confirmationPassword) {
        ngToast.create({
          className: 'danger',
          content: "Le password non coincidono",
          timeout: 5000
        });
        return false;
      }

      if (!user.gender || user.gender == '') {
        ngToast.create({
          className: 'danger',
          content: "Seleziona un genere",
          timeout: 5000
        });
        return false;
      }

      return true;
    }

    var start = function() {
//      if (AuthService.isAuthenticated()) {
//        $state.go("home");
//      }
    }

    start();

  }]);
