/**
 * Created by me on 20/09/15.
 */
angular.module("ucookiApp")
  .controller("NavbarCtrl", ['$scope', '$stateParams', '$rootScope', '$location', 'AuthService',
    function($scope, $stateParams, $rootScope, $location, AuthService) {

      $scope.userCollapsed = false;

      $scope.cart = [];

      $scope.logout = AuthService.logout;

      $scope.closeMobileMenu = function() {
        $(".main-menu").toggleClass("show");
        $(".layer").toggleClass("layer-is-visible");
        $(".cmn-toggle-switch").removeClass("active");
      };

      $scope.getLocation = function() {
        return encodeURIComponent($location.url());
      }

    }
  ]);
