angular.module('ucookiApp')
  .controller('ResetPasswordCtrl', ['$scope', '$location', '$http', '$state', 'ngToast', function($scope, $location, $http, $state, ngToast) {

    $scope.loading = false;

    $scope.submit = function() {
      if ($scope.password != $scope.confirmation) {
        ngToast.create({
          className: 'danger',
          content: "Le password non coincidono",
          timeout: 5000
        });
        return;
      }

      $scope.loading = true;

      $http.post("/reset-password?access_token=" + $location.search().access_token, {
          password: $scope.password,
          confirmation: $scope.confirmation
        })
        .then(function(result) {
          $scope.loading = false;
          ngToast.create({
            className: 'success',
            content: "La password è stata aggiornata",
            timeout: 5000
          });
          $state.go("home");
        }).catch(function(err) {
          $scope.loading = false;
          ngToast.create({
            className: 'danger',
            content: "Si sono verificati dei problemi durante il reset della password",
            timeout: 5000
          });
          console.error(err);
        });

    };
  }]);
