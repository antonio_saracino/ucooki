var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;
var jsonfile = require('jsonfile');
const dataFile = __dirname + "/data.json";

exports.deleteAll = function(cb) {
  cb = cb || function() {};
  var conf = jsonfile.readFileSync(dataFile);

  var convertToObjectId = function (list) {
    var result = [];

    list.forEach(function (item) {
      result.push(new ObjectID(item));
    });

    return result;
  }

  conf.ob_users_ids = convertToObjectId(conf.users_ids);
  conf.ob_products_ids = convertToObjectId(conf.products_ids);
  conf.ob_orders_ids = convertToObjectId(conf.orders_ids);

  MongoClient.connect(conf.url, function (err, db) {

    db.collection('Customer').deleteMany({'_id': {'$in': conf.ob_users_ids}}).then(function (result) {
      console.log("Customer deleted: ", result.result);
      return db.collection('Product').deleteMany({'_id': {'$in': conf.ob_products_ids}});
    }).then(function (result) {
      console.log("Product deleted: ", result.result);

      db.collection('Order').find({'buyer_id': {'$in': conf.users_ids}}).toArray(function (err, docs) {
        if (err) {
          cb(err);
          return;
        }
        docs.forEach(function(doc) {
          conf.ob_orders_ids.push(doc._id);
        });

        db.collection('Order').deleteMany({'_id': {'$in': conf.ob_orders_ids}}).then(function (result) {
          console.log("Order deleted: ", result.result);
          console.log("Deleted all documents");
          db.close();
          cb();
        });
      });
    }).catch(function (error) {
      cb(error);
    });
  });
}
