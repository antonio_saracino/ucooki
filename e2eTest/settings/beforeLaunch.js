var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;
var q = require('q');
var jsonfile = require('jsonfile');
var util = require("./util");

const dataFile = __dirname + "/data.json";

/**
 *
 *  0    A                test1@ucooki.com
 *  1    B                test2@ucooki.com
 *  2    UserCookCC       test3@ucooki.com
 *  3    D                test4@ucooki.com
 *  4    UserCookBB       test5@ucooki.com
 *  5    C                test6@ucooki.com
 *  6    UserCookDD       test7@ucooki.com
 *
 * Spec 1:
 *      Attori ->   UserA
 *      Azioni ->   login
 *                  diventa cuoco
 *                  crea piatto
 *                  cancella piatto
 *                  logout
 *
 * Spec 2:
 *      Attori ->   UserB (utente)
 *                  UserCookBB (cuoco + piatto)
 *
 *      Azioni ->   login Utente
 *                  ordine di consegna
 *                  ordine di ritiro
 *                  logout
 *
 * Spec 3:
 *      Attori ->   UserC (utente)
 *                  UserCookCC (cuoco + piatto + ordine 1 e 2)
 *
 *      Azioni ->   login Cuoco
 *                  ordine 1: accettato
 *                  ordine 2: rifiutato
 *                  logout
 *
 * Spec 4:
 *      Attori ->   UserD (utente + ordine 3 e 4)
 *                  UserCookDD (cuoco + piatto)
 *
 *      Azioni ->   login Utente
 *                  ordine 3: cancellato
 *                  ordine 4: lascia commento
 *                  logout
 *
 *
 *
 *
 */


var conf = {
  url: "mongodb://ucooki:ucooki@13.70.201.192/dev",
  users: [
    {
      "_id": new ObjectID("008fc2337c98e6490cb2d4d6"),
      "name": "UserA",
      "lastname": "Test",
      "language": "it_IT",
      "gender": "male",
      "city": "Bo",
      "cooker": false,
      "provider": "local",
      "username": "test1@ucooki.com",
      "password": "$2a$10$PsMUeEAhg/iqLNKy1aNbD.eJ2PWRT98cFoXmnZfkFHVvwehC5YRZ6",
      "email": "test1@ucooki.com",
      "emailVerified": true,
      "phone": "3296914992",
    },
    {
      "_id": new ObjectID("008fc2337c98e6490cb2d4d7"),
      "name": "UserB",
      "lastname": "Test",
      "language": "it_IT",
      "gender": "male",
      "city": "Bo",
      "cooker": false,
      "provider": "local",
      "username": "test2@ucooki.com",
      "password": "$2a$10$PsMUeEAhg/iqLNKy1aNbD.eJ2PWRT98cFoXmnZfkFHVvwehC5YRZ6",
      "email": "test2@ucooki.com",
      "emailVerified": true,
    },
    {
      "_id": new ObjectID("008fc2337c98e6490cb2d4d8"),
      "name": "UserCookCC",
      "lastname": "Test",
      "language": "it_IT",
      "gender": "female",
      "city": "Bo",
      "cooker": true,
      "provider": "local",
      "username": "test3@ucooki.com",
      "password": "$2a$10$PsMUeEAhg/iqLNKy1aNbD.eJ2PWRT98cFoXmnZfkFHVvwehC5YRZ6",
      "email": "test3@ucooki.com",
      "emailVerified": true,
      "code": "rlltrs51a43b413i",
      "phone": "3296914992",
      "delivery_price": "Nessun costo",
      "kitchen_address": "Via Aurelio Saffi, 10, 40131 Bologna, Italia",
      "kitchen_location": {
        "lat": 44.5007825,
        "lng": 1.1322918500000014E1
      },
      "description": "Sono una mamma leccese trapiantata a Bologna per stare vicino ai miei pargoli (3 ragazzotti di 30 anni), a da vera donna del Sud non potevo concepire che mangiassero \"qualcosa al volo\" sempre di corsa tra lavoro e impegni quotidiani. Così ogni fine settimana, a pranzo, la famiglia è chiamata a rapporto per ritrovare i sapori del cuore e stare insieme. Alla nostra tavola fa sempre piacere avere tanti amici con cui condividere le specialità leccesi, e ora, qui su Ucooki, con chiunque abbia piacere di scoprire ricette tradizionali salentine e assaporare ingredienti di qualità.",
      "kitchen_location_info": {
        "address": "Via Aurelio Saffi, 40131 Bologna, Italia",
        "street_number": "10",
        "note": "Settimo piano, Campanello Saracino",
        "id": null
      },
      "productor_info": {
        "rate": 0,
        "certificated": true,
        "product_count": 0,
        "id": null
      },
      "_picture": {
        "id": "rielli.teresa@gmail.com/sld2lde7bob9wmcrzrxe",
        "url": "https://res.cloudinary.com/ucooki/image/upload/v1482753023/rielli.teresa%40gmail.com/sld2lde7bob9wmcrzrxe.jpg",
        "principal": true,
        "idOriginal": "rielli.teresa@gmail.com/cgpuac9qvopijaveykyn",
        "urlOriginal": "https://res.cloudinary.com/ucooki/image/upload/v1482753024/rielli.teresa%40gmail.com/cgpuac9qvopijaveykyn.jpg",
        "status": 1
      }
    },
    {
      "_id": new ObjectID("008fc2337c98e6490cb2d4d9"),
      "name": "UserD",
      "lastname": "Test",
      "language": "it_IT",
      "gender": "male",
      "city": "Bo",
      "cooker": false,
      "provider": "local",
      "username": "test4@ucooki.com",
      "password": "$2a$10$PsMUeEAhg/iqLNKy1aNbD.eJ2PWRT98cFoXmnZfkFHVvwehC5YRZ6",
      "email": "test4@ucooki.com",
      "emailVerified": true,
      "phone": "3296914992",
    },
    {
      "_id": new ObjectID("008fc2337c98e6490cb2d410"),
      "name": "UserCookBB",
      "lastname": "Test",
      "language": "it_IT",
      "gender": "male",
      "city": "Bo",
      "cooker": true,
      "provider": "local",
      "username": "test5@ucooki.com",
      "password": "$2a$10$PsMUeEAhg/iqLNKy1aNbD.eJ2PWRT98cFoXmnZfkFHVvwehC5YRZ6",
      "email": "test5@ucooki.com",
      "emailVerified": true,
      "code": "rlltrs51a43b413i",
      "phone": "3296914992",
      "delivery_price": "Nessun costo",
      "kitchen_address": "Via Aurelio Saffi, 10, 40131 Bologna, Italia",
      "kitchen_location": {
        "lat": 44.5007825,
        "lng": 1.1322918500000014E1
      },
      "kitchen_location_info": {
        "address": "Via Aurelio Saffi, 40131 Bologna, Italia",
        "street_number": "10",
        "note": "Settimo piano, Campanello Saracino",
        "id": null
      },
      "productor_info": {
        "rate": 0,
        "certificated": true,
        "product_count": 0,
        "id": null
      }
    },
    {
      "_id": new ObjectID("008fc2337c98e6490cb2d411"),
      "name": "UserC",
      "lastname": "Test",
      "language": "it_IT",
      "gender": "male",
      "city": "Bo",
      "cooker": false,
      "provider": "local",
      "username": "test6@ucooki.com",
      "password": "$2a$10$PsMUeEAhg/iqLNKy1aNbD.eJ2PWRT98cFoXmnZfkFHVvwehC5YRZ6",
      "email": "test6@ucooki.com",
      "emailVerified": true,
      "phone": "3296914992",
    },
    {
      "_id": new ObjectID("008fc2337c98e6490cb2d412"),
      "name": "UserCookDD",
      "lastname": "Test",
      "language": "it_IT",
      "gender": "male",
      "city": "Bo",
      "cooker": true,
      "provider": "local",
      "username": "test7@ucooki.com",
      "password": "$2a$10$PsMUeEAhg/iqLNKy1aNbD.eJ2PWRT98cFoXmnZfkFHVvwehC5YRZ6",
      "email": "test7@ucooki.com",
      "emailVerified": true,
      "code": "rlltrs51a43b413i",
      "phone": "3296914992",
      "delivery_price": "Nessun costo",
      "kitchen_address": "Via Aurelio Saffi, 10, 40131 Bologna, Italia",
      "kitchen_location": {
        "lat": 44.5007825,
        "lng": 1.1322918500000014E1
      },
      "kitchen_location_info": {
        "address": "Via Aurelio Saffi, 40131 Bologna, Italia",
        "street_number": "10",
        "note": "Settimo piano, Campanello Saracino",
        "id": null
      },
      "productor_info": {
        "rate": 0,
        "certificated": true,
        "product_count": 0,
        "id": null
      }
    }
  ],
  products: [{
    "_id": new ObjectID("008fc2337c98e6490cb2d413"),
    "customer_id": null,
    "name": "TEST Maccarruni freschi con polpette al sugo TEST",
    "active": true,
    "status": 1,
    "featured": true,
    "description": "Variente ancora più gustosa e golosa del piatto al pomodoro.",
    "notice_time": 3,
    "sale_type": 0,
    "price": 7.5,
    "min_portion": 1,
    "max_portion": 6,
    "pickup": true,
    "delivery": true,
    "delivery_price": 0,
    "tags": [
      "pasta fatta in casa",
      "primo",
      "salento",
      "pugliese",
      "polpette",
      "sugo"
    ],
    "_pictures": [
      {
        "id": "rielli.teresa@gmail.com/cnxlhorxllgiws4mp1hh",
        "url": "https://res.cloudinary.com/ucooki/image/upload/v1459803545/rielli.teresa%40gmail.com/cnxlhorxllgiws4mp1hh.jpg",
        "principal": true,
        "idOriginal": "rielli.teresa@gmail.com/wsldufwi7cgyw2p2ffrl",
        "urlOriginal": "https://res.cloudinary.com/ucooki/image/upload/v1459803556/rielli.teresa%40gmail.com/wsldufwi7cgyw2p2ffrl.png",
        "status": 1
      }
    ],
    "_ingredients": [
      {
        "id": 2,
        "name": "Semola di grano duro rimacinata",
        "description": "Mulino Ferri, Sasso Marconi (BO)"
      }
    ],
    "_timeRanges": [
      {
        "id": 1,
        "day": 1,
        "startLunch": -1,
        "endLunch": -1,
        "startDinner": 1800,
        "endDinner": 2130
      },
      {
        "id": 2,
        "day": 2,
        "startLunch": -1,
        "endLunch": -1,
        "startDinner": 1800,
        "endDinner": 2130
      },
      {
        "id": 3,
        "day": 3,
        "startLunch": -1,
        "endLunch": -1,
        "startDinner": 1800,
        "endDinner": 2130
      },
      {
        "id": 4,
        "day": 4,
        "startLunch": -1,
        "endLunch": -1,
        "startDinner": 1800,
        "endDinner": 2130
      },
      {
        "id": 5,
        "day": 5,
        "startLunch": -1,
        "endLunch": -1,
        "startDinner": 1800,
        "endDinner": 2130
      },
      {
        "id": 6,
        "day": 6,
        "startLunch": -1,
        "endLunch": -1,
        "startDinner": 1800,
        "endDinner": 2130
      },
      {
        "id": 7,
        "day": 7,
        "startLunch": -1,
        "endLunch": -1,
        "startDinner": 1800,
        "endDinner": 2130
      }
    ]
  },
    {
      "_id": new ObjectID("008fc2337c98e6490cb2d414"),
      "customer_id": null,
      "name": "TEST Maccarruni freschi con polpette al sugo TEST",
      "active": true,
      "status": 1,
      "featured": true,
      "description": "Variente ancora più gustosa e golosa del piatto al pomodoro.",
      "notice_time": 3,
      "sale_type": 0,
      "price": 7.5,
      "min_portion": 1,
      "max_portion": 6,
      "pickup": true,
      "delivery": true,
      "delivery_price": 0,
      "tags": [
        "pasta fatta in casa",
        "primo",
        "salento",
        "pugliese",
        "polpette",
        "sugo"
      ],
      "_pictures": [
        {
          "id": "rielli.teresa@gmail.com/cnxlhorxllgiws4mp1hh",
          "url": "https://res.cloudinary.com/ucooki/image/upload/v1459803545/rielli.teresa%40gmail.com/cnxlhorxllgiws4mp1hh.jpg",
          "principal": true,
          "idOriginal": "rielli.teresa@gmail.com/wsldufwi7cgyw2p2ffrl",
          "urlOriginal": "https://res.cloudinary.com/ucooki/image/upload/v1459803556/rielli.teresa%40gmail.com/wsldufwi7cgyw2p2ffrl.png",
          "status": 1
        }
      ],
      "_ingredients": [
        {
          "id": 2,
          "name": "Semola di grano duro rimacinata",
          "description": "Mulino Ferri, Sasso Marconi (BO)"
        }
      ],
      "_timeRanges": [
        {
          "id": 1,
          "day": 1,
          "startLunch": -1,
          "endLunch": -1,
          "startDinner": 1800,
          "endDinner": 2130
        },
        {
          "id": 2,
          "day": 2,
          "startLunch": -1,
          "endLunch": -1,
          "startDinner": 1800,
          "endDinner": 2130
        },
        {
          "id": 3,
          "day": 3,
          "startLunch": -1,
          "endLunch": -1,
          "startDinner": 1800,
          "endDinner": 2130
        },
        {
          "id": 4,
          "day": 4,
          "startLunch": -1,
          "endLunch": -1,
          "startDinner": 1800,
          "endDinner": 2130
        },
        {
          "id": 5,
          "day": 5,
          "startLunch": -1,
          "endLunch": -1,
          "startDinner": 1800,
          "endDinner": 2130
        },
        {
          "id": 6,
          "day": 6,
          "startLunch": -1,
          "endLunch": -1,
          "startDinner": 1800,
          "endDinner": 2130
        },
        {
          "id": 7,
          "day": 7,
          "startLunch": -1,
          "endLunch": -1,
          "startDinner": 1800,
          "endDinner": 2130
        }
      ]
    },
    {
      "_id": new ObjectID("008fc2337c98e6490cb2d415"),
      "customer_id": null,
      "name": "TEST Maccarruni freschi con polpette al sugo TEST",
      "active": true,
      "status": 1,
      "featured": true,
      "description": "Variente ancora più gustosa e golosa del piatto al pomodoro.",
      "notice_time": 3,
      "sale_type": 0,
      "price": 7.5,
      "min_portion": 1,
      "max_portion": 6,
      "pickup": true,
      "delivery": true,
      "delivery_price": 0,
      "tags": [
        "pasta fatta in casa",
        "primo",
        "salento",
        "pugliese",
        "polpette",
        "sugo"
      ],
      "_pictures": [
        {
          "id": "rielli.teresa@gmail.com/cnxlhorxllgiws4mp1hh",
          "url": "https://res.cloudinary.com/ucooki/image/upload/v1459803545/rielli.teresa%40gmail.com/cnxlhorxllgiws4mp1hh.jpg",
          "principal": true,
          "idOriginal": "rielli.teresa@gmail.com/wsldufwi7cgyw2p2ffrl",
          "urlOriginal": "https://res.cloudinary.com/ucooki/image/upload/v1459803556/rielli.teresa%40gmail.com/wsldufwi7cgyw2p2ffrl.png",
          "status": 1
        }
      ],
      "_ingredients": [
        {
          "id": 2,
          "name": "Semola di grano duro rimacinata",
          "description": "Mulino Ferri, Sasso Marconi (BO)"
        }
      ],
      "_timeRanges": [
        {
          "id": 1,
          "day": 1,
          "startLunch": -1,
          "endLunch": -1,
          "startDinner": 1800,
          "endDinner": 2130
        },
        {
          "id": 2,
          "day": 2,
          "startLunch": -1,
          "endLunch": -1,
          "startDinner": 1800,
          "endDinner": 2130
        },
        {
          "id": 3,
          "day": 3,
          "startLunch": -1,
          "endLunch": -1,
          "startDinner": 1800,
          "endDinner": 2130
        },
        {
          "id": 4,
          "day": 4,
          "startLunch": -1,
          "endLunch": -1,
          "startDinner": 1800,
          "endDinner": 2130
        },
        {
          "id": 5,
          "day": 5,
          "startLunch": -1,
          "endLunch": -1,
          "startDinner": 1800,
          "endDinner": 2130
        },
        {
          "id": 6,
          "day": 6,
          "startLunch": -1,
          "endLunch": -1,
          "startDinner": 1800,
          "endDinner": 2130
        },
        {
          "id": 7,
          "day": 7,
          "startLunch": -1,
          "endLunch": -1,
          "startDinner": 1800,
          "endDinner": 2130
        }
      ]
    }],
  orders: [
    {
      "_id": new ObjectID("008fc2337c98e6490cb2d416"),
      "buyer_id": null,
      "productor_id": null,
      "delivery_date": null,
      "date": new Date(),
      "status": 0,
      "total": 7.5,
      "delivery_price": 0,
      "delivery": true,
      "items": [
        {
          "product_id": null,
          "note": "",
          "quantity": 1,
          "id": "0",
          "price": 7.5,
          "delivery": true,
        }
      ],
      "location": {
        "address": "Via Aurelio Saffi, 40131 Bologna, Italia",
        "street_number": "10",
        "note": "Settimo piano, Campanello Saracino",
        "id": null,
        "city": "Bologna",
        "region": "Emilia-Romagna"
      },
      "total_grass": 7.5
    },
    {
      "_id": new ObjectID("008fc2337c98e6490cb2d417"),
      "buyer_id": null,
      "productor_id": null,
      "delivery_date": null,
      "date": new Date(),
      "status": 0,
      "total": 7.5,
      "delivery_price": 0,
      "delivery": false,
      "items": [
        {
          "product_id": null,
          "note": "",
          "quantity": 1,
          "id": "0",
          "price": 7.5,
          "delivery": false,
        }
      ],
      "location": {
        "address": "Via Aurelio Saffi, 40131 Bologna, Italia",
        "street_number": "10",
        "note": "Settimo piano, Campanello Saracino",
        "id": null,
        "city": "Bologna",
        "region": "Emilia-Romagna"
      },
      "total_grass": 7.5
    },
    {
      "_id": new ObjectID("008fc2337c98e6490cb2d418"),
      "buyer_id": null,
      "productor_id": null,
      "delivery_date": null,
      "date": new Date(),
      "status": 0,
      "total": 7.5,
      "delivery_price": 0,
      "delivery": false,
      "items": [
        {
          "product_id": null,
          "note": "",
          "quantity": 1,
          "id": "0",
          "price": 7.5,
          "delivery": false,
        }
      ],
      "location": {
        "address": "Via Aurelio Saffi, 40131 Bologna, Italia",
        "street_number": "10",
        "note": "Settimo piano, Campanello Saracino",
        "id": null,
        "city": "Bologna",
        "region": "Emilia-Romagna"
      },
      "total_grass": 7.5
    },
    {
      "_id": new ObjectID("008fc2337c98e6490cb2d419"),
      "buyer_id": null,
      "productor_id": null,
      "delivery_date": null,
      "date": new Date(),
      "status": 0,
      "total": 7.5,
      "delivery_price": 0,
      "delivery": false,
      "items": [
        {
          "product_id": null,
          "note": "",
          "quantity": 1,
          "id": "0",
          "price": 7.5,
          "delivery": false,
        }
      ],
      "location": {
        "address": "Via Aurelio Saffi, 40131 Bologna, Italia",
        "street_number": "10",
        "note": "Settimo piano, Campanello Saracino",
        "id": null,
        "city": "Bologna",
        "region": "Emilia-Romagna"
      },
      "total_grass": 7.5
    },
    {
      "_id": new ObjectID("008fc2337c98e6490cb2d420"),
      "buyer_id": null,
      "productor_id": null,
      "delivery_date": null,
      "date": new Date(),
      "status": 3,
      "total": 7.5,
      "delivery_price": 0,
      "delivery": false,
      "items": [
        {
          "product_id": null,
          "note": "",
          "quantity": 1,
          "id": "0",
          "price": 7.5,
          "delivery": false,
        }
      ],
      "location": {
        "address": "Via Aurelio Saffi, 40131 Bologna, Italia",
        "street_number": "10",
        "note": "Settimo piano, Campanello Saracino",
        "id": null,
        "city": "Bologna",
        "region": "Emilia-Romagna"
      },
      "total_grass": 7.5
    }
  ]
}

var deferred = q.defer();

util.deleteAll(function(err) {
  if (err) console.error(err);
  createModel();
});

function createModel() {
  MongoClient.connect(conf.url, function (err, db) {
    console.log("Connected successfully to mongodb");
    var Customer = db.collection('Customer');
    var Product = db.collection('Product');
    var Order = db.collection('Order');

    Customer.insert(conf.users).then(function (result) {

      console.log("Users saved:  \n", result.insertedIds);
      conf.users = result.ops;
      conf.users_ids = result.insertedIds;
      conf.products[0].customer_id = conf.users[2]._id;
      conf.products[1].customer_id = conf.users[4]._id;
      conf.products[2].customer_id = conf.users[6]._id;
      return Product.insert(conf.products);

    }).then(function (result) {

      console.log("Product saved: \n", result.insertedIds);
      conf.products = result.ops;
      conf.products_ids = result.insertedIds;

      // aggiorno i dati degli ordini di C
      var dataOrder = new Date();
      dataOrder.setDate(dataOrder.getDate() + 1);
      dataOrder.setHours(21);
      dataOrder.setMinutes(00);

      conf.orders[0].delivery_date = dataOrder;
      conf.orders[0].timeout = new Date(dataOrder.getTime() - 60*60*1000);
      conf.orders[0].productor_id = conf.users[2]._id.toString();
      conf.orders[0].buyer_id = conf.users[5]._id.toString();
      conf.orders[0].items[0].product_id = conf.products[0]._id.toString();
      conf.orders[0].items[0].product = conf.products[0];

      conf.orders[1].delivery_date = dataOrder;
      conf.orders[1].timeout = new Date(dataOrder.getTime() - 60*60*1000);
      conf.orders[1].productor_id = conf.users[2]._id.toString();
      conf.orders[1].buyer_id = conf.users[5]._id.toString();
      conf.orders[1].items[0].product_id = conf.products[0]._id.toString();
      conf.orders[1].items[0].product = conf.products[0];

      // aggiorno i dati degli ordini di D
      conf.orders[2].delivery_date = dataOrder;
      conf.orders[2].timeout = new Date(dataOrder.getTime() - 60*60*1000);
      conf.orders[2].productor_id = conf.users[6]._id.toString();
      conf.orders[2].buyer_id = conf.users[3]._id.toString();
      conf.orders[2].items[0].product_id = conf.products[2]._id.toString();
      conf.orders[2].items[0].product = conf.products[2];

      dataOrder.setDate(dataOrder.getDate() - 5);
      conf.orders[3].delivery_date = dataOrder;
      conf.orders[3].timeout = new Date(dataOrder.getTime() - 60*60*1000);
      dataOrder.setHours(dataOrder.getHours() - 6);
      conf.orders[3].date = dataOrder;
      conf.orders[3].status = 3;
      conf.orders[3].productor_id = conf.users[6]._id.toString();
      conf.orders[3].buyer_id = conf.users[3]._id.toString();
      conf.orders[3].items[0].product_id = conf.products[2]._id.toString();
      conf.orders[3].items[0].product = conf.products[2];

      dataOrder.setDate(dataOrder.getDate() - 5);
      conf.orders[4].delivery_date = dataOrder;
      conf.orders[4].timeout = new Date(dataOrder.getTime() - 60*60*1000);
      dataOrder.setHours(dataOrder.getHours() - 6);
      conf.orders[4].date = dataOrder;
      conf.orders[4].status = 3;
      conf.orders[4].productor_id = conf.users[2]._id.toString();
      conf.orders[4].buyer_id = conf.users[5]._id.toString();
      conf.orders[4].items[0].product_id = conf.products[0]._id.toString();
      conf.orders[4].items[0].product = conf.products[0];

      return Order.insert(conf.orders);

    }).then(function (result) {
      console.log("Orders saved: \n", result.insertedIds);
      conf.orders = result.ops;
      conf.orders_ids = result.insertedIds;

      jsonfile.writeFile(dataFile, conf, function (err) {
        db.close();
        if (err) {
          console.error(err);
          deferred.reject(err);
          return;
        }
        deferred.resolve();
      })

    }).catch(function (error) {
      db.close();
      deferred.reject(error);
    });

  });
}

module.exports = deferred.promise;
