var LoginPage = require('./pages/login-page');
var CommonPage = require('./pages/common-page');
var DialogPage = require('./pages/dialog-page');
var AddAddressDialog = require('./pages/dialog-add-address-page');

var login = new LoginPage;
var dialogPage = new DialogPage;
var commonPage = new CommonPage;
var addressDialog = new AddAddressDialog;

describe('[Cook actions]', function () {

    beforeAll(function () {
        login.login(conf.users[0].email, "anto");
    });

    it('should become a cook', function () {
        browser.setLocation("pages/dashboard/personal-info").then(function () {

            browser.sleep(500);

            browser.executeScript('window.scrollTo(0,0);');

            var becomeACook = element(by.model("data.user.cooker"));
            browser.wait(EC.elementToBeClickable(becomeACook), 20000);
            becomeACook.click();

            var phone = element(by.model("data.user.phone"));
            phone.sendKeys("2396914992");

            browser.sleep(1000);

            var addAddress = element(by.id("btn-cook-address"));
            browser.wait(EC.elementToBeClickable(addAddress), 20000);
            addAddress.click();

            addressDialog.addAddress();

            browser.wait(EC.presenceOf(element(by.css("div.address"))), 20000);

            var save = element(by.css("div.foot button.btn-save"));
            // browser.wait(EC.elementToBeClickable(save, 5000));

            save.click().then(function () {

                commonPage.successAlert(function (toasts) {
                    expect(toasts.count()).toBeGreaterThan(0);
                    toasts.each(function (t) {
                        t.click();
                    });
                });
            });

        });

    });

    it('should add new product', function () {
        browser.setLocation("/pages/dashboard/products/new").then(function () {

            element(by.model("product.name")).sendKeys("Piatto di prova");

            element(by.model("product.description")).sendKeys("Descrizione piatto di prova...");

            element(by.model("product.price")).sendKeys(5);

            browser.executeScript('window.scrollTo(0,0);');

            var btnSave = element(by.css("div.head a.btn-save"));
            browser.wait(EC.elementToBeClickable(btnSave, 20000));
            btnSave.click().then(function () {
                commonPage.dangerAlert(function (toasts) {
                    expect(toasts.count()).toBe(3);
                    toasts.each(function (t) {
                        t.click();
                    });

                    element(by.css(".tags input")).click();
                    var item = $("#ui-select-choices-row-0-0");
                    browser.wait(EC.presenceOf(item), 20000);
                    item.click();

                    $("div.ingredients a").click();
                    var ingredient = element(by.model("ingredient.name"));
                    browser.wait(EC.presenceOf(ingredient), 20000);
                    ingredient.sendKeys("farina");

                    browser.executeScript('window.scrollTo(0, 1300);');

                    $("#btn-lunch-start-1").click();
                    browser.wait(EC.presenceOf($("span.dropdown.open ul.dropdown-menu")), 20000);
                    $$("span.dropdown.open ul.dropdown-menu li").get(1).click();

                    browser.sleep(500);

                    $("#btn-lunch-end-1").click();
                    browser.wait(EC.presenceOf($("span.dropdown.open ul.dropdown-menu")), 20000);
                    $$("span.dropdown.open ul.dropdown-menu li").get(1).click();

                    browser.executeScript('window.scrollTo(0, 0);');
                    btnSave.click();

                    dialogPage.submit().then(function () {
                        commonPage.successAlert(function (toasts) {
                            expect(toasts.count()).toBeGreaterThan(0);
                            toasts.each(function (t) {
                                t.click();
                            });
                        });
                    });

                });
            });
        });
    })

    it('should delete product', function () {
        browser.setLocation("/pages/dashboard/products").then(function () {
            browser.wait(EC.presenceOf($("div.desc > a")), 20000);
            return $("div.desc > a").click();
        }).then(function () {
            var btnDelete = $("div.head a.btn-delete");
            browser.wait(EC.elementToBeClickable(btnDelete), 20000);
            return btnDelete.click();
        }).then(function () {
            return dialogPage.submit();
        }).then(function () {
            commonPage.successAlert(function (toasts) {
                expect(toasts.count()).toBeGreaterThan(0);
                toasts.each(function (t) {
                    t.click();
                });
            });
        });
    });

    afterAll(function () {
        commonPage.logout();
    });

});
