var LoginPage = require('./pages/login-page');
var MessageDialog = require('./pages/dialog-comment-page');
var CommonPage = require('./pages/common-page');
var DialogPage = require('./pages/dialog-page');

var login = new LoginPage;
var message = new MessageDialog;
var commonPage = new CommonPage;
var dialogPage = new DialogPage;

describe('[User order actions]', function () {
    beforeAll(function () {
        login.login(conf.users[3].email, "anto");
    });

    it('should cancel an order', function () {
        browser.setLocation("pages/dashboard/orders").then(function () {

            browser.wait(EC.visibilityOf($('order-box')), 5000);

            $$('order-box h4 a.accordion-toggle').get(0).click();

            browser.executeScript('window.scrollTo(0,350);');

            $$('button.btn-danger').get(0).click();

            dialogPage.submit().then(function () {
                commonPage.successAlert(function (toasts) {
                    expect(toasts.count()).toBeGreaterThan(0);
                    toasts.each(function (t) {
                        t.click();
                    });
                });
            });
        });

    });

    it('should leave a feedback to a cook', function () {
        browser.setLocation("pages/dashboard/orders").then(function () {
            browser.executeScript('window.scrollTo(0,500);');

            $$('order-box h4 a.accordion-toggle').get(1).click();

            $('button.btn-success').click();

            message.submit("five", "messaggio di test");

            var rates = element(by.model("item.rating.rate"));
            browser.wait(EC.presenceOf(rates), 5000);
            expect(rates.isPresent()).toBeTruthy();
        });

    });

    afterAll(function () {
        commonPage.logout();
    });
});
