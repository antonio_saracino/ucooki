var jsonfile = require('jsonfile');
var util = require("./settings/util");

const dataFile = __dirname + "/settings/data.json";


exports.config = {
  directConnect: true,
  framework: 'jasmine',
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: [
    'cook.spec.js',
    'user-order.spec.js',
    'cook-order.spec.js',
    'user-comment.spec.js'
  ],
  getPageTimeout: 60000,
  allScriptsTimeout: 60000,
  capabilities: {
    'browserName': 'chrome',
    shardTestFiles: false,
    maxInstances: 2
  },
  beforeLaunch: "./settings/beforeLaunch.js",
  onPrepare: function () {

    global.EC = protractor.ExpectedConditions;

    var SpecReporter = require('jasmine-spec-reporter');
    var env = jasmine.getEnv();
    env.clearReporters();
    // add jasmine spec reporter
    env.addReporter(new SpecReporter({displayStacktrace: 'all', displaySpecDuration: true}));

    // Disable animations so e2e tests run more quickly
    var disableNgAnimate = function () {
      angular.module('disableNgAnimate', []).run(['$animate', function ($animate) {
        $animate.enabled(false);
      }]);
    };

    browser.addMockModule('disableNgAnimate', disableNgAnimate);

    // Store the name of the browser that's currently being used.
    browser.getCapabilities().then(function (caps) {
      browser.params.browser = caps.get('browserName');
    });

    browser.get("http://localhost:3000/");
    browser.driver.manage().window().setSize(1200, 800);

    return browser.executeScript("alert('foreground trick');").then(function () {
      global.conf = jsonfile.readFileSync(dataFile);
      return browser.switchTo().alert().accept()
    });
  },
  afterLaunch: function () {

    var deferred = protractor.promise.defer();

    util.deleteAll(function(err) {
      if (err) {
        deferred.reject(err);
      } else {
        deferred.fulfill();
      }
    });

    return deferred.promise;

  },

  jasmineNodeOpts: {
    defaultTimeoutInterval: 60000,
    showTiming: true
  }
};
