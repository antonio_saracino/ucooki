var MongoClient = require('mongodb').MongoClient;
var LoginPage = require('./pages/login-page');
var CommonPage = require('./pages/common-page');
var OrderDialog = require('./pages/dialog-order-page');
var Checkout = require('./pages/checkout-page');

var login = new LoginPage;
var orderDialog = new OrderDialog;
var commonPage = new CommonPage;
var checkout = new Checkout;

describe('[User order actions]', function () {

  beforeAll(function () {
    login.login(conf.users[1].email, "anto");
  });

  beforeEach(function (done) {
    MongoClient.connect(conf.url, function (err, db) {
      db.collection('Customer').updateOne({email: conf.users[1].email}, {$unset: {"phone": ""}}, function (err, customer) {
        expect(err).toBeNull();
        db.close();
        done();
      })
    });
  });

  it('should make an delivery order', function () {

    browser.setLocation("pages/product/" + conf.products[1]._id.toString()).then(function () {

      browser.executeScript('window.scrollTo(0,1050);');

      $('#btn-add-to-cart').click();

      orderDialog.delivery();

      var btnCheckout = $("#cart_box button");
      browser.wait(EC.elementToBeClickable(btnCheckout), 20000);
      btnCheckout.click();

      browser.wait(EC.urlIs('http://localhost:3000/pages/checkout'), 20000);

      checkout.addPhone().then(function () {
        commonPage.successAlert(function (toasts) {
          expect(toasts.count()).toBeGreaterThan(0);
          toasts.each(function (t) {
            t.click();
          });
        });
      });

      checkout.addAddress();

      browser.executeScript('window.scrollTo(0,1050);');

      var btnConfirmOrder = $("#btn-order-confirm");
      browser.wait(EC.elementToBeClickable(btnConfirmOrder), 20000);

      btnConfirmOrder.click().then(function () {
        commonPage.successAlert(function (toasts) {
          expect(toasts.count()).toBeGreaterThan(0);
          toasts.each(function (t) {
            t.click();
          });
        });
      });

    });

  });

  it('should make an pickup order', function () {
    browser.setLocation("pages/product/" + conf.products[1]._id.toString()).then(function () {
      browser.refresh();

      browser.wait(EC.presenceOf($('#btn-add-to-cart')), 20000);
      browser.executeScript('window.scrollTo(0,1050);');
      $('#btn-add-to-cart').click();

      orderDialog.pickup();

      $("#cart_box button").click();

      expect(browser.getCurrentUrl()).toBe('http://localhost:3000/pages/checkout');

      checkout.addPhone().then(function () {
        commonPage.successAlert(function (toasts) {
          expect(toasts.count()).toBeGreaterThan(0);
          toasts.each(function (t) {
            t.click();
          });
        });
      });

      browser.executeScript('window.scrollTo(0,1050);');

      $("#btn-order-confirm").click().then(function () {
        commonPage.successAlert(function (toasts) {
          expect(toasts.count()).toBeGreaterThan(0);
          toasts.each(function (t) {
            t.click();
          });
        });
      });

    });
  });

  afterAll(function () {
    commonPage.logout();
  });

});
