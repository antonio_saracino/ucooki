var LoginPage = require('./pages/login-page');
var CommonPage = require('./pages/common-page');
var DialogPage = require('./pages/dialog-page');
var MessageDialog = require('./pages/dialog-comment-page');

var login = new LoginPage;
var commonPage = new CommonPage;
var dialogPage = new DialogPage;
var message = new MessageDialog;

describe('[Cook order actions]', function () {

  beforeAll(function () {
    login.login(conf.users[2].email, "anto");
  });

  it('should accept an order', function () {
    browser.setLocation("pages/dashboard/orders");

    $$('order-box h4 a.accordion-toggle').get(0).click();

    $$('button.btn-success').get(0).click();

    dialogPage.submit().then(function () {
      commonPage.successAlert(function (toasts) {
        expect(toasts.count()).toBeGreaterThan(0);
        toasts.each(function (t) {
          t.click();
        });
      });
    });

  });

  it('should reject an order', function () {
    browser.setLocation("pages/dashboard/orders");

    browser.executeScript('window.scrollTo(0,500);');

    $$('order-box h4 a.accordion-toggle').get(1).click();

    browser.executeScript('window.scrollTo(0,600);');

    $('button.btn-danger').click();

    dialogPage.submit().then(function () {
      commonPage.successAlert(function (toasts) {
        expect(toasts.count()).toBeGreaterThan(0);
        toasts.each(function (t) {
          t.click();
        });
      });
    });

  });

  it('should send a feedback to user', function () {
    browser.setLocation("pages/dashboard/orders");

    browser.executeScript('window.scrollTo(0,500);');

    $$('order-box h4 a.accordion-toggle').get(2).click();

    browser.executeScript('window.scrollTo(0,600);');

    $('button.btn-success').click();

    message.submit("five", "messaggio di test");

    var rates = element(by.model("order.rating.rate"));
    browser.wait(EC.presenceOf(rates), 5000);
    expect(rates.isPresent()).toBeTruthy();

  });

  afterAll(function () {
    commonPage.logout();
  });

});
