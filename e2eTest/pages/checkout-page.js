var DialogPage = require('./dialog-page');
var AddAddressDialog = require('./dialog-add-address-page');

var dialog = new DialogPage;
var addressDialog = new AddAddressDialog;

var Checkout = function () {
    this.addPhone = function () {
        browser.executeScript('window.scrollTo(0,900);');

        $("#btn-add-phone").click();

        var phone = element(by.model("data.phone"));
        browser.wait(EC.visibilityOf(phone), 5000);
        phone.sendKeys("123456789");

        return dialog.submit();

    };
    this.addAddress = function () {

        browser.executeScript('window.scrollTo(0,900);');
        $("#btn-add-address").click();

        return addressDialog.addAddress();

    }
}

module.exports = Checkout;