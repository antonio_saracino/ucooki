var DialogPage = require('./dialog-page');

var dialogPage = new DialogPage;

var AddAddressDialog = function () {

    this.addAddress = function () {

        var address = element(by.model("data.address"));
        address.getAttribute('value').then(function (text) {
            if (!text) {
                address.sendKeys("via vittorio bigari 14, bologna");
                browser.actions().sendKeys(protractor.Key.ENTER).perform();
            }
        });

        var streetNumber = element(by.model("data.streetNumber"));
        streetNumber.getAttribute('value').then(function(number) {
            if (!number) {
                streetNumber.sendKeys(14);
            }
        });

        return dialogPage.submit();
    }
}

module.exports = AddAddressDialog;