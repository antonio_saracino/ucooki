var DialogPage = require('./dialog-page');

var dialogPage = new DialogPage;

var OrderDialog = function () {
    this.delivery = function () {
        return this.makeAndOrder(true);
    };

    this.pickup = function () {
        return this.makeAndOrder(false);
    };

    this.makeAndOrder = function (delivery) {

        browser.wait(EC.visibilityOf($("div.btn-group")), 5000);
        if (delivery) {
            $$("div.btn-group label").get(1).click();
        }
        $("#deliverycalendar button").click();

        $$(".modal-popup .order-datepicker td button:enabled").get(0).click();

        $$("select option").get(1).click();

        return dialogPage.submit();
    }

}

module.exports = OrderDialog;