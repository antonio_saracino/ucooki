var DialogPage = function() {
    this.submit = function() {

        browser.wait(EC.visibilityOf($("div.modal-dialog")), 5000);
        // return promise
        return $("div.modal-dialog button.btn-submit").click();
    }
}

module.exports = DialogPage;