var LoginPage = function () {

    this.login = function (email, pass) {

        browser.waitForAngular();
        browser.setLocation('pages/login');
        expect(browser.getCurrentUrl()).toBe('http://localhost:3000/pages/login');

        var cookies = element(by.css('p.divascookies-accept-button-text'));
        cookies.isPresent().then(function (present) {
            if (present)
                cookies.click();
        });

        var username = element(by.model("credentials.username"));
        var password = element(by.model("credentials.password"));

        browser.wait(EC.visibilityOf(username), 5000);

        username.sendKeys(email);
        password.sendKeys(pass);

        var button = element(by.buttonText('Login'));
        button.click();

        browser.wait(EC.urlIs('http://localhost:3000/'), 30000);
        expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/');


    }
}

module.exports = LoginPage;