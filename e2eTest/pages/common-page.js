
var _toastAlert = function (type, cb) {
    var toast = null;
    var toasts = null;

    switch (type) {
        case "success":
            toast = $('ul.ng-toast__list div.alert-success');
            toasts = $$('ul.ng-toast__list div.alert-success');
            break;
        case "danger":
            toast = $('ul.ng-toast__list div.alert-danger');
            toasts = $$('ul.ng-toast__list div.alert-danger');
            break;
    }

    if (!toast) {
        throw new Error("Unable to determine the correct toast's type");
    }

    browser.ignoreSynchronization = true;
    browser.sleep(500);
    browser.wait(EC.presenceOf(toast), 10000).then(function () {
        cb(toasts);
        // toasts.each(function (t) {
        //     t.click();
        // });

        browser.ignoreSynchronization = false;
    })


}

var CommonPage = function () {
    this.successAlert = function (cb) {
        _toastAlert("success", cb);
    };
    this.dangerAlert = function (cb) {
        _toastAlert("danger", cb);
    };
    this.logout = function () {
        var menu = element(by.css('header.header-desktop nav div.main-menu.ng-scope')).element(by.css('ul li.submenu a.show-submenu.bar-link.ng-binding'));
        menu.click();

        var logout = element(by.linkText('Esci'));
        logout.click();

        var accedi = element(by.css('header.header-desktop a.access-link.bar-link'));
        expect(accedi.getText()).toEqual('Accedi');
    }
}

module.exports = CommonPage;