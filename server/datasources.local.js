var mongoUrl = process.env.MONGO_URL;


var url = "mongodb://ucooki:ucooki@13.70.201.192/dev"
// var url = "mongodb://localhost:27017/ucooki";
// var url = "mongodb://ucooki:%40uC00ki%23@13.70.201.192/prod"
// var url = "mongodb://ucookiMaster:%40uC00ki%23@13.70.201.192/prod"

if (mongoUrl) {
    url = mongoUrl;
}

// default - mongo local
var db = {
    "url": url,
    "defaultForType": "mongodb",
    "connector": "loopback-connector-mongodb",
    "enableGeoIndexing" : true
}

console.log("db: ", url);

module.exports = {
    "db": db
}
