var nodeEnv = (process.env.NODE_ENV || 'development');
var adminEmailEnv = (process.env.ADMIN_EMAIL);
var hostnameEnv = (process.env.HOSTNAME);

var config = {};

if ('production' !== nodeEnv) {
  config['admin-email'] = "anto.saracino@tiscali.it";
  config['hostname'] = "http://localhost:3000";
}

// imposto la mail di amministrazione
if (adminEmailEnv) {
  config['admin-email'] = adminEmailEnv;
}

if (hostnameEnv) {
  config['hostname'] = hostnameEnv;
}

console.log("config updated: ", JSON.stringify(config, null, 4));

module.exports = config;
