var moment = require('moment-timezone');
require('log-timestamp')(function() { return "[" + moment().tz("Europe/Rome").format("DD-MM-YYYY HH:mm:ss") + "]" });
var loopback = require('loopback');
var boot = require('loopback-boot');
var path = require('path');
var compression = require('compression');
var helmet = require('helmet');
var useragent = require('express-useragent');


// added ssl
// Load the necessary modules and middleware for HTTPS
var http = require('http');

process.title = "ucooki-app";

// update environment
process.env.NODE_ENV = process.env.NODE_ENV || 'development';
console.log("Start Ucooki App ...");
console.log("node_env: " + process.env.NODE_ENV);

var app = module.exports = loopback();

app.use(helmet());



// Set up the /favicon.ico
app.use(loopback.favicon());

// request pre-processing middleware
app.use(compression());

// The access token is only available after boot
app.middleware('auth', loopback.token({
  model: app.models.AccessToken
}));

// All middleware and routes defined below and on will be redirected to a HTTPS connection

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

app.use(useragent.express());


app.start = function() {
  // start the web server

  var port = process.env.PORT || app.get('port');

  console.log("port: " + port);

  http.createServer(app).listen(port, function() {
    app.emit('started');
    console.log('Web server listening at: http://' + app.get('host') + ":" + port);
  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) throw err;

  // aggiorna gli indici
  app.datasources.db.autoupdate('Customer', function(err) {});

  var value = require.main === module;

  // start the server if `$ node server.js`
  var onIISNode = require.main.filename.match(/iisnode/);
  if (onIISNode || require.main === module) {
    app.start();
  }

});
