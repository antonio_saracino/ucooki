var Q = require('q');
var cores = require('os').cpus().length;
var _ = require("lodash");
var phantom = require('phantom');
var azure = require('azure-storage');
var mkdirp = require('mkdirp');
var fs = require('fs');
var getDirName = require('path').dirname;
var app = require('../server');
var async = require('async');
// const baseDir = app.get("snapshots-dir");
const hostname = app.get("hostname");
var count = 0;
var _ph, _page;
const env = (process.env.NODE_ENV || 'development');

const AZURE_STORAGE_ACCOUNT = "ucookibackup";
const AZURE_STORAGE_KEY = "wQhOyUh0u4XxDWWTRbijDQtfkAUJnfWqbKn4xH7J/A7be7yc+p1p6TJCTmqRedBOnkOPTG4EUzTT7kecS3aGMA==";
const AZURE_STORAGE_CONTAINER = "snapshots";


var blobService = azure.createBlobService(AZURE_STORAGE_ACCOUNT, AZURE_STORAGE_KEY);


module.exports.pool = function (urls) {

  var deferred = Q.defer();

  var chunk = _.chunk(urls, Math.ceil(urls.length / cores));

  async.forEach(chunk, function (list, cb) {

    module.exports.create(list).then(function () {
      cb();
    }).catch(function (error) {
      cb(error);
    });

  }, function (err) {
    if (err) deferred.reject(err);
    else deferred.resolve();
  });

  return deferred.promise;

};

/**
 * Restituisce l'html renderizzato di uno specifico URL
 * @param url
 */
module.exports.render = function(url) {

  var deferred = Q.defer();

  createPhantom().then(function() {

    if (!url || typeof url !== "string") {
      deferred.resolve("");
      return;
    }

    load(url).then(function(content) {
      killPhantom();
      deferred.resolve(content);
    }).catch(function() {
      killPhantom();
      deferred.reject();
    });

  }).catch(function(error) {
    console.error("Error occurred during snapshots creation. ", error);
    killPhantom();
    deferred.reject(error);
  });


  return deferred.promise;

};

/**
 * Carica e salva nella cache di Azure una lista di URL renderizzati
 * @param urls
 */
module.exports.create = function (urls) {

  var deferred = Q.defer();

  if (!urls || !Array.isArray(urls)) {
    deferred.resolve();
  }

  initContainer().then(function() {
    return createPhantom();

  }).then(function () {


    // creo un sistema fault tolerance. se si verifica un errore
    // sul processo phantom, quello attuale viene chiuso e viene creato un altro


    async.forEachSeries(urls, function (url, cb) {

      if (!url || typeof url !== "string") {
        cb();
        return;
      }

      if (url.indexOf(hostname) > -1) {
        url = url.replace(hostname, "");
      }

      var filename = url;
      url = hostname + url;

      // nel caso in cui sia l'homepage creo un file di index
      if (url === hostname + "/") {
        filename = "index";
      }

      count++;

      load(url).then(function (content) {

        saveFile(filename, content, function (err) {

          if (err) {
            console.error("Error occurred while write file: ", filename);
          } else {
            // console.log("file " + filename + " saved!");
          }
          cb();
        });


      }).catch(function (error) {
        console.error("Error occurred while loading a snapshot. Recovery mode active! ", error);
        createPhantom().then(function () {
          cb();
        });
      });

    }, function (err) {
      killPhantom();

      if (err) {
        console.error("Error while create async snapshots", err);
        deferred.reject(err);
      } else {
        deferred.resolve();
      }

    });
  }).catch(function(error) {
    console.error("Error occurred during snapshots creation. ", error);
    killPhantom();
    deferred.reject(error);
  });

  return deferred.promise;

};

function killPhantom() {
  if (_page) _page.close().catch(function() {});
  if (_ph) _ph.exit().then(function() {
    console.log("phantom process killed!");
  }).catch(function() {});
}

function createPhantom() {
  killPhantom();

  // console.log("I'm going to create a new Phantom process...");

  return phantom.create(['--ignore-ssl-errors=yes', '--load-images=no', '--disk-cache=yes']).then(function (ph) {
    _ph = ph;
    return _ph.createPage();
  }).then(function (page) {
    _page = page;

    _page.on('onResourceRequested', true, function (requestData, request) {

      var re = /((google-analytics)|(zopim)|(facebook)|(mixpanel)|(addthis)|(youtube))\.com/;

      if (re.test(requestData['url'])) {
        request.abort();
      }
    }).catch(function() {

    });

    return _page.property('viewportSize', {width: 1200, height: 800});

  }).catch(function (error) {
    console.error("Error occurred during phantom creation. ", error);
  });
}

function load(url) {

  var deferred = Q.defer();
  var condition = {
    status: false
  };

  _page.on('onConsoleMessage', function (msg) {

    if (msg.indexOf("title updated") > -1) {
      _page.off('onConsoleMessage');
      condition.status = true;
    }
  }).catch(function (err) {
    console.error("Unable to get console message. ", err)
  });

  _page.open(url).then(function (status) {

    if (status !== "success") throw new Error("Unable to load " + url, " - status: " + status);
    var getContent = function () {
      deferred.resolve(_page.property('content'));
    };

    waitFor(getContent, getContent, condition);
  }).catch(function (error) {
    console.error("Something goes wrong on rendering page: " + url + "\n\t\t--> " + error);
    deferred.reject(error);
  });

  return deferred.promise;

}

function waitFor(onReady, onTimeout, condition, timeOutMillis) {
  var maxtimeOutMillis = timeOutMillis ? timeOutMillis : 6000, //< Default Max Timout is 3s
    start = new Date().getTime(),
    // condition = false,
    interval = setInterval(function () {

      if ((new Date().getTime() - start < maxtimeOutMillis) && !condition.status) {

        // aspetto...

      } else {
        clearInterval(interval); //< Stop this interval
        if (!condition.status) {
          // If condition still not fulfilled (timeout but condition is 'false')
          // console.log("'waitFor()' timeout");
          onTimeout();
        } else {
          // Condition fulfilled (timeout and/or condition is 'true')
          // console.log("'waitFor()' finished in " + (new Date().getTime() - start) + "ms.");
          typeof(onReady) === "string" ? eval(onReady) : onReady(); //< Do what it's supposed to do once the condition is fulfilled

        }
      }
    }, 250); //< repeat check every 250ms
}

function initContainer() {

  var deferred = Q.defer();

  blobService.createContainerIfNotExists(AZURE_STORAGE_CONTAINER, {
    publicAccessLevel: 'container'
  }, function (error, result, response) {
    if (error) {
      deferred.reject(error);
      return;
    }
    deferred.resolve();
  });

  return deferred.promise;
}

function saveFile(path, contents, cb) {

  path = path.replace(/^\//,"");

  if (env !== "production") {

    path = "./snapshots/"+path+".html";

      mkdirp(getDirName(path), function (err) {
        if (err) return cb(err);
        console.log("file snapshot saved: ", path);

        fs.writeFile(path, contents, cb);
      });

  } else {
    blobService.createBlockBlobFromText(AZURE_STORAGE_CONTAINER, path, contents, function(error, result, response) {
      if (error) {
        console.error("Error during upload snapshots");
        cb(error);
      } else {
        cb();
      }
    });
  }


}
