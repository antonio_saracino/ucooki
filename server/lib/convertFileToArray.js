var fs = require("fs");

fs.readFile(__dirname + '/fileWithStrings.txt', 'utf8', function (err,data) {
  if (err) {
    return console.log(err);
  }

	var result = "[";

	data.split(",").sort().forEach(function(string) {

		result = result + ("\n\t\"" + string.trim().toLowerCase() +"\",");

	});

	result = result.substring(0, result.length - 1);
	result += "\n]";

	console.log(result);

});
