var sm = require('sitemap');
var app = require('../server');
var q = require("q");

function buildSitemap(products, customers, cb) {

  var cb = cb || function () {
    };
  var productUrls = [];
  var customerUrls = [];

  products.forEach(function (product) {
    var url = {
      url: '/pages/product/' + product.__data.id,
      changefreq: 'weekly',
      priority: 0.8
    }
    productUrls.push(url);
  });

  customers.forEach(function (customer) {
    var url = {
      url: '/pages/customer/' + customer.__data.id,
      changefreq: 'weekly',
      priority: 0.8
    }
    customerUrls.push(url);
  });

  var staticUrls = [{
    url: '/',
    changefreq: 'daily'
  }, {
    url: '/pages/login',
    changefreq: 'monthly',
    priority: 0.3
  }, {
    url: '/pages/reset-password',
    changefreq: 'monthly',
    priority: 0.3
  }, {
    url: '/pages/contacts',
    changefreq: 'monthly',
    priority: 0.3
  }, {
    url: '/pages/terms-and-conditions',
    changefreq: 'monthly',
    priority: 0.3
  }, {
    url: '/pages/privacy',
    changefreq: 'monthly',
    priority: 0.3
  }, {
    url: '/pages/team',
    changefreq: 'monthly',
    priority: 0.3
  }, {
    url: '/pages/faq',
    changefreq: 'monthly',
    priority: 0.3
  }, {
    url: '/pages/mission',
    changefreq: 'monthly',
    priority: 0.3
  }, {
    url: '/pages/how-it-works',
    changefreq: 'monthly',
    priority: 0.3
  }, {
    url: '/pages/become-a-cooker',
    changefreq: 'monthly',
    priority: 0.3
  }, {
    url: '/pages/quality-control',
    changefreq: 'monthly',
    priority: 0.3
  }, {
    url: '/pages/scopri-la-community-della-cucina-tradizionale-da-asporto',
    changefreq: 'monthly',
    priority: 0.3
  }];

  var urls = productUrls.concat(customerUrls).concat(staticUrls);
  var hostname = app.get("hostname");
  var sitemap = sm.createSitemap({
    hostname: hostname,
    cacheTime: 600000, // 600 sec - cache purge period
    urls: urls
  });

  sitemap.toXML(function (err, xml) {
    if (err) {
      cb(err);
      return;
    }

    cb(null, xml);

  });
}

exports.create = function () {

  var deferred = q.defer();

  var Product = app.models.Product;
  var Customer = app.models.Customer;

  var filter = {
    where: {
      status: 1,
      active: true
    },
    fields: ["id"]
  };

  Product.find(filter, function (err, products) {
    if (err) {
      console.error("Error occurred while searching products: ", err);
      deferred.reject('Error while generating sitemap.xml');
    }

    filter = {
      where: {
        emailVerified: true
      },
      fields: ["id"]
    };

    Customer.find(filter, function (err, customers) {

      if (err) {
        console.error("Error occurred while searching customers: ", err);
        deferred.reject('Error while generating sitemap.xml');
      }

      try {

        buildSitemap(products, customers, function (err, xml) {

          if (err) {
            deferred.reject(err);
            return;
          }

          deferred.resolve(xml);

        });

      } catch (err) {
        deferred.reject(err);
      }
    });

  });

  return deferred.promise;

};
