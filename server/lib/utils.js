var app = require('../server');
var s = require("underscore.string");

module.exports.stringToRegex = function (string) {
    var texts = string.trim().split(" ");
    var textToSearch = "";
    texts.forEach(function (t) {
        if (t && t.length > 0) {
            textToSearch += ".*" + t;
        }
    })
    var regex = new RegExp(textToSearch, 'i');
    return regex;
}

module.exports.capitalizeSentence = function (sentence) {

    if (!sentence) {
        return "";
    }

    const THREE_DOT_CODE = "%+++%";

    function convert(string, sep, sub) {
        var result = [];
        var exprs = string.split(sep);
        exprs.forEach(function (expr) {
            result.push(s(expr).trim().clean().capitalize().value());
        });

        var sub = sub || sep + " ";
        var phrase = result.join(sub);
        return phrase;
    }

    sentence = sentence.toLowerCase();
    sentence = sentence.replace("...", THREE_DOT_CODE);

    sentence = convert(sentence, ".");
    sentence = convert(sentence, "?");
    sentence = convert(sentence, "!");

    sentence = sentence.replace(THREE_DOT_CODE , "...");

    sentence = convert(sentence, "...");

    return sentence;

};

module.exports.sendAdminNotification = function (html) {
  try {

    var Email = app.models.Email;
    Email.send({
      to: app.get('admin-email'),
      from: "info@ucooki.com",
      subject: "Notifica automatica",
      html: html
    });

  } catch (e) {
    console.error(e);
    console.trace();
  }

}
