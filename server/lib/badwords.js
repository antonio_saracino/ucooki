var badwords = require('bad-words');
var italianBadWords = require('./badwords-ita.json');

var filter = new badwords();
filter.addWords(italianBadWords);

module.exports = filter;
