var Client = require('node-rest-client').Client;
var secretApi = 'c92d0bc8f588827c6a8fa5720b4be913-us12';
var baseUrl = "https://us12.api.mailchimp.com/3.0";
var md5 = require('md5');

var client = new Client({
  user: "apisecret",
  password: secretApi
});

module.exports.customerList = "ccdd060460";

module.exports.getLists = function(args, success, fail) {

  client.get(baseUrl + "/lists/?fields=lists.id,lists.name", args, function(data, response) {

    if (response.statusCode) {
      if (success) {
        success(data);
      }
    } else {
      if (fail) {
        fail(response.statusCode, response.statusMessage);
      }
    }
  })
};

module.exports.addSubscriber = function(args, success, fail) {
  client.post(baseUrl + "/lists/${list_id}/members", args, function(data, response) {

    if (response.statusCode) {
      if (success) {
        success(data);
      }
    } else {
      if (fail) {
        fail(response.statusCode, response.statusMessage);
      }
    }
  });
};

module.exports.updateSubscriber = function(args, success, fail) {

	if (!args.path || !args.path.email) {
		fail();
		return;
	}
	args.path.email = md5(args.path.email);

  client.patch(baseUrl + "/lists/${list_id}/members/${email}", args, function(data, response) {

    if (response.statusCode) {
      if (success) {
        success(data);
      }
    } else {
      if (fail) {
        fail(response.statusCode, response.statusMessage);
      }
    }
  });
};

module.exports.deleteSubscriber = function(args, success, fail) {

  if (!args.path || !args.path.email) {
    fail();
    return;
  }
  args.path.email = md5(args.path.email);

  client.delete(baseUrl + "/lists/${list_id}/members/${email}", args, function(data, response) {

    if (response.statusCode) {
      if (success) {
        success(data);
      }
    } else {
      if (fail) {
        fail(response.statusCode, response.statusMessage);
      }
    }
  });
};
