var app = require("../server");
var qs = require('querystring');
var https = require('https');
var env = (process.env.NODE_ENV || 'development');
var username = "ucooki";
var password = "@uC00ki#";


module.exports.send = function (input, cb, cb_err) {

  var text = input.text;
  var sender_string = "Ucooki";
  var method = "classic";
  var recipients = input.recipients || [];
  var validity_period = 60;

  if (!method) {
    cb_err("No Method!");
    return;
  }

  switch (method) {
    case 'classic':
      method = 'send_sms_classic';
      break;
    case 'report':
      method = 'send_sms_classic_report';
      break;
    case 'basic':
    default:
      method = 'send_sms_basic';
  }

  var test = input.test || "production" !== env;

  // Check params
  if (recipients.length == 0) {
    cb_err("No recipient!");
    return;
  }

  if (!text) {
    cb_err("No text!");
    return;
  }


  var params = {
    method: method,
    username: username,
    password: password,
    "recipients[]": correctNumber(recipients),
    text: text,
    charset: "UTF-8",
    validity_period: validity_period
  };

  if (sender_string) {
    params.sender_string = sender_string;
  }

  if (test) {
    params.method = "test_" + params.method;
  }

  var res_done = false;
  var data = qs.stringify(params);
  if (test) {
    console.log("DEBUG sms data: ", data);
  }


  var client = https.request({
    port: 443,
    path: "/api/send/smseasy/advanced/http.php",
    host: "gateway.skebby.it",
    method: "POST",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      "Content-Length": data.length,
      "Content-Encoding": "utf8",
    }
  }, function (res) {
    var res_data = "";
    res.on('data', function (data) {
      res_data += data;
    });
    res.on("end", function () {
      if (!res_done) {
        var res_parsed = qs.parse(res_data);
        if (res_parsed.status == "success") {
          if (!test && parseInt(res_parsed.remaining_sms) <= 10) {
            sendMail("<p>Il numero di sms disponibili sta terminando. Sono attualmente disponibili solo <b> " + res_parsed.remaining_sms + " sms</b>.</p>");
          }

          cb({data: res_parsed});
        }
        else {
          // ------------------------------------------------------------------
          // Check the complete documentation at http://www.skebby.com/business/index/send-docs/
          // ------------------------------------------------------------------
          // For eventual errors see http:#www.skebby.com/business/index/send-docs/#errorCodesSection
          // WARNING: in case of error DON'T retry the sending, since they are blocking errors
          // ------------------------------------------------------------------
          sendMail("<p>Si è verificato un errore nell'invio dell'sms. I destinatari sono <b>" + params["recipients[]"] + "</b> ed il messaggio è \"" + params.text + "\"</p><pre>" + JSON.stringify(res_parsed, null, 4) + "</pre>");
          cb_err(res_parsed);
        }
        res_done = true;
      }
    });
  });

  client.end(data);
  client.on('error', function (e) {
    if (!res_done) {
      cb_err(e);
      res_done = true;
    }
  });
};


var correctNumber = function (list) {

  var result = [];

  if (!list || !Array.isArray(list)) {
    return result;
  }

  list.forEach(function (number) {

    var num = ("" + number).replace("+", "");
    num = num.replace(/^00/g, "");

    if (num.length <= 0) {
      return;
    }

    if (num.length == 12) {
      // ok è un formato valido. es 393296914992
      result.push(num);
    }

    if (num.length == 10) {
      // ok, è un numero scritto in forma semplice 3296914992
      result.push("39" + num);
    }
  });

  return result;
}

var sendMail = function (msg) {

  var Email = app.models.Email;

  // invio della notifica agli admin
  var options = {
    from: "Ucooki <info@ucooki.com>", // sender address
    to: app.get('admin-email'), // list of receivers
    subject: "Skebby Sms alert", // Subject line
    html: "<h2>Notifica SMS</h2>" + msg // html body
  }

  Email.send(options, function (err, mail) {
    if (err) console.error('error occurred while sending sms email to admin!', err, mail);
  });

}
