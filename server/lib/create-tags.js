var async = require('async');
var tags = require('./tags.json');
var filter = require('../lib/badwords.js');

function tag(name) {
  this.name = name;
}

module.exports = function(app) {

  var models = app.models();
  var Tag = app.models.Tag;

  async.each(tags, function(string, done) {
    var t = new tag(string.trim().toLowerCase());

		if (t.name != filter.clean(t.name)) {
			console.log("Found bad word: " + t);
			done();
			return;
		}

    Tag.create(t, function(err, obj) {

//      if (err) {
//        console.error("error: ", err.message);
//      }

      done();
    });
  }, function(err) {

    if (err) throw err;

    console.log("tags creation ended");

  });
}
