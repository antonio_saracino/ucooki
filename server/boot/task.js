var schedule = require('node-schedule');
var sitemap = require("../lib/sitemap");
var snapshot = require('../lib/snapshot');
var parseString = require('xml2js').parseString;
var schedulerActive = (process.env.SCHEDULER_ACTIVE || false);

module.exports = function (app) {

  var refuseInterval = 5 * 60 * 1000; // ogni 5 minuti
  var completeInterval = 60 * 60 * 1000; // ogni ora

  var orderToComplete = function () {

    var Order = app.models.Order;
    var now = new Date();
    // torno indietro di 24 ore
    now.setHours(now.getHours() - 24);

    // now.setMinutes(now.getMinutes() - 1);

    var filter = {
      and: [{
        status: 1
      }, {
        'delivery_date': {
          lte: now
        }
      }]
    };

    Order.find({
      where: filter
    }, function (err, orders) {

      if (!orders || orders.length <= 0) {
        return;
      }

      orders.forEach(function (order) {
        order.__data.status = 3;
        order.save();
      });

    });

  };

  // cerco gli ordini da rifiutare se hanno superato il timeout
  var orderToRefuse = function () {

    var Order = app.models.Order;

    var filter = {
      and: [{
        status: 0
      }, {
        timeout: {
          lte: new Date()
        }
      }]
    };

    Order.find({
      where: filter
    }, function (err, orders) {

      if (!orders || orders.length <= 0) {
        return;
      }

      orders.forEach(function (order) {
        order.__data.status = 6;
        order.__data.productor_note = "Ordine cancellato in automatico per attesa superiore al limite";
        order.save();
      });

    });

  };

  var snapshotCreation = function () {
    //day of week (0 - 7) (0 or 7 is Sun)
    if (!schedulerActive) {
      console.log("scheduler disabled!");
      return;
    }

    console.log("scheduler ACTIVATED!");
    schedule.scheduleJob('0 2 * * *', function () {

      var start = new Date().getTime();

      console.log("Start scheduled job: Snapshot creation...");

      sitemap.create().then(function (xml) {
        parseString(xml, function (err, result) {
          if (err) {
            console.error("Error occurred while generating snapshot");
            return;
          }

          if (result && result.urlset && result.urlset.url) {

            // result.urlset.url = result.urlset.url.slice(0,20);
            var urls = result.urlset.url.map(function (obj) {
              return obj.loc[0];
            });
            urls.push("/");
            snapshot.create(urls).fin(function () {
              console.log("Snapshots created in " + parseInt((new Date().getTime() - start) / 1000) + " s.");
            });
          }
        });

      }).catch(function (err) {
        console.error("Error occurred while generating sitemap.", err);
      })

    });
  };

  init();

  function init() {

    setInterval(function () {

      orderToRefuse();

    }, refuseInterval);

    setInterval(function () {

      orderToComplete();

    }, completeInterval);

    // snapshotCreation();

  }


};
