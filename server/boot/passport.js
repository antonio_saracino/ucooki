// Passport configurators..
var loopback = require('loopback');
var expSession = require('express-session');
var cookieParser = require('cookie-parser');
var loopbackPassport = require('loopback-component-passport');
var PassportConfigurator = loopbackPassport.PassportConfigurator;
var config = require('../../providers.json');


var env = (process.env.NODE_ENV || 'development');
if (env == 'development') {
  config = require('../../providers.develop.json');
  console.log("Providers Develop loaded");
}

module.exports = function (server) {

  var dataSource = server.datasources.db;
  var MongoDBStore = require('connect-mongodb-session')(expSession);
  var store = new MongoDBStore({
    "uri": dataSource.settings.url,
    "collection": "Sessions"
  });

  server.middleware('session:before', cookieParser(server.get('cookieSecret')));
  server.middleware('session', expSession({
    secret: server.get('cookieSecret'),
    saveUninitialized: true,
    resave: true,
    store: store
  }));


  server.get('/auth/facebook', function (req, res, next) {
    req.session.returnto = req.query.returnto;
    res.setHeader('Access-Control-Allow-Origin', '*');
    next();
  })

  var passportConfigurator = new PassportConfigurator(server);
  var passport = passportConfigurator.init();


  passportConfigurator.setupModels({
    userModel: server.models.Customer,
    userIdentityModel: server.models.CustomerIdentity,
    userCredentialModel: server.models.CustomerCredential
  });

  var optionsFacebook = config["facebook"];
  var session = optionsFacebook.session || true;

  var callback = function (req, res, next) {

    passport.authenticate('facebook', {
      session: session
    }, function (err, user, info) {

      if (err) {
        return next(err);
      }
      if (!user) {

        if (!!options.json) {
          return res.status(401).json("authentication error")
        }
        return res.redirect(optionsFacebook.failureRedirect);
      }

      // var userObj = user.toObject();
      if (user.verificationToken || !user.emailVerified) {
        // una volta fatto l'accesso tramite oassport non ha 
        // senso esserci questo valore
        user.emailVerified = true;
        user.unsetAttribute("verificationToken");
        user.save(function (err, updated) {
          if (err) console.error(err);
          return res.redirect(getRedirectUrl());
        })
        
      } else {
        return res.redirect(getRedirectUrl());
      }

      function isNew(user) {
        if (!user)
          return false;

        var created = new Date(user.toObject().created);
        var now = new Date();
        if ((now.getTime() - created.getTime()) < 1000 * 60 * 2) {
          return true;
        }

        return false;
      }

      function getRedirectUrl() {

        var url = optionsFacebook.successRedirect;
        if (optionsFacebook.successReturnToOrRedirect) {
          url = optionsFacebook.successReturnToOrRedirect;
          if (req.session.returnto) {
            url = req.session.returnto;
            delete req.session.returnto;
          }
        }

        return "/#/passport?access_token=" + info.accessToken.id + "&user_id=" + user.id.toString() + "&return=" + encodeURIComponent(url) + "&new_user=" + isNew(user) + "&email=" + encodeURIComponent(user.email);
      }
    })(req, res, next);
  };

  for (var name in config) {
    var options = config[name];
    options.session = options.session !== false;
    options.profileToUser = function (provider, profile, options) {
      return server.models.Customer.mapProfileToUser(provider, profile);
    }
    options.customCallback = callback;

    passportConfigurator.configureProvider(name, options);

  }
}
