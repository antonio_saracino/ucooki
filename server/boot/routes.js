var sitemap = require('../lib/sitemap');

module.exports = function(app) {
  var Customer = app.models.Customer;
  var AccessToken = app.models.AccessToken;

  //reset the user's pasword
  app.post('/reset-password', function(req, res, next) {
    if (!req.query || !req.query.access_token) return res.sendStatus(401);

    //verify passwords match
    if (!req.body.password ||
      !req.body.confirmation ||
      req.body.password !== req.body.confirmation) {
      return res.sendStatus(400, new Error('Passwords do not match'));
    }

    AccessToken.findById(req.query.access_token, function(err, acc) {

			if (err) return res.sendStatus(404, err);

      Customer.findById(acc.userId, function(err, user) {
        if (err) return res.sendStatus(404, err);
        user.updateAttribute('password', req.body.password, function(err, user) {
          if (err) return res.sendStatus(404, err);
          res.send({
            title: 'Password reset success',
            content: 'Your password has been reset successfully'
          });
        });
      });
    });
  });

  //invio messaggio
  app.post('/send-message', function(req, res, next) {

    // invio il messaggio ricevuto
    if (!req.body) {
      console.error("Cannot process message. Body empty.");
      return;
    }

    var options = {
      from: 'info@ucooki.com', // sender address
      to: app.get('admin-email'), // list of receivers
      subject: "Nuovo messaggio", // Subject line
      html: "<h2>Nome: " + req.body.name + "</h2><p>Email: <b>" + req.body.email + "</b></p><p> Telefono: <b>" + req.body.phone + "</b></p><p> Ha inviato questo messaggio: <br><hr>" + req.body.msg + "</p>" // html body
    }

    app.models.Email.send(options, function(err, mail) {
      if (err) {
        console.log('error occurred during sending email!', err);
        return res.sendStatus(err.responseCode, err);
      }
      return res.sendStatus(200);
    });

  });

  // sitemap generator
  app.get('/sitemap.xml', function(req, res) {

    sitemap.create().then(function(xml) {

      res.header('Content-Type', 'application/xml');
      res.send(xml);

    }).catch(function(err) {
      console.error("Error occurred while generating sitemap.", err);
      res.status(500).end();
    });
  });

};
