module.exports = function (server) {
    var router = server.loopback.Router();

    router.get('/robots.txt', function (req, res) {
        res.type('text/plain');
			res.send("User-Agent: *\nAllow: /\nSitemap: https://www.ucooki.com/sitemap.xml");
    });
    server.use(router);
};
