module.exports = function (app) {

    var Role = app.models.Role;
    var RoleMapping = app.models.RoleMapping;
    var Customer = app.models.Customer;

    Role.upsert({
        name: 'admin',
        id: "583af14c4eaf3e4c2cc9f536"
    }, function (err, role) {
        if (err) return console.error(err);
        // console.log('Created/updated role Admin');

        var emails = app.get("admin-email");
        if (!emails) {
            console.error("Unable to find admin emails. Cannot set admin users.");
            return;
        }
        var list = emails.split(";");
        emails = [];
        list.forEach(function(email) {
            emails.push(email.trim());
        })
        
        var filter = {
            where: {
                username: { inq: emails }
            }
        };

        Customer.find(filter, function (err, customers) {

            customers.forEach(function (customer) {

                role.principals.findOne({ where: { principalId: customer.id } }, function (err, mapping) {
                    
                    if (err) {
                        console.error(err);
                        return;
                    }

                    if (!mapping) {
                        role.principals.create({
                            principalType: RoleMapping.USER,
                            principalId: customer.id
                        }, function (err, principal) {
                            if (err) throw err;
                            console.log('Created/updated user ' + customer.username + ' as Admin');
                        });
                    } else {
                        console.log('User ' + customer.username + " already setted as Admin");
                    }
                });

            });

        });
    });

}