/**
 * https://github.com/strongloop/loopback-gateway/blob/master/server/private/ssl_cert.js
 **/

  var fs = require("fs"),
  path = require('path'),
  tls = require('tls');

var keysDir = '/etc/ssl/private';

exports.privateKey = fs.readFileSync(path.join(keysDir, 'ucooki-private.pem')).toString();
exports.certificate = fs.readFileSync(path.join(keysDir, 'ucooki-cert.pem')).toString();
exports.ca = [
	fs.readFileSync(path.join(keysDir, 'ucooki-bundle-ca-1.pem')).toString(),
	fs.readFileSync(path.join(keysDir, 'ucooki-bundle-ca-2.pem')).toString(),
	fs.readFileSync(path.join(keysDir, 'ucooki-bundle-ca-3.pem')).toString()
]

exports.credentials = tls.createSecureContext({
  key: exports.privateKey,
  cert: exports.certificate,
  ca: exports.ca
});