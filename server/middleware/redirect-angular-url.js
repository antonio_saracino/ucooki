module.exports = function() {
  return function(req, res, next) {

		if (req.url.indexOf("/pages/") > -1) {
//			console.log("Try to handle refresh angular page. Url: ", req.url);
			res.redirect("/#" + req.url);
			return;
		}

		next();

  }
}
