var _ = require('lodash');
var loopback = require('loopback');
var path = require('path');
var app = require('../server');

module.exports = function () {

  var createHtmlProduct = function (id, success, fail) {

    app.models.Product.findById(id, function (err, product) {
      if (err) {
        console.error("Error while fetching product: ", err);
        fail();
        return;
      }

      if (!product) {
        fail();
        return;
      }

      var product = product.toObject();
      var images = [];
      var principal;

      for (var i in product._pictures) {
        if (!product._pictures[i].principal) {
          images.push(product._pictures[i].url);
        } else {
          principal = product._pictures[i].url;
        }
      }

      images.unshift(principal);

      var description = "";
      description = _.truncate(product.description, {
        'length': 150,
        'separator': ' '
      });

      var render = loopback.template(path.resolve(__dirname, '../views/meta-tag/product-tag.ejs'));
      var html = render({
        images: images,
        title: product.name + " - Ucooki",
        price: product.price,
        description: description
      });

      success(html);
      return;

    });

  }


  var createHtmlProductor = function (id, success, fail) {


    app.models.Customer.findById(id, {
      include: {
        relation: 'products',
        scope: {
          skip: 0,
          limit: 6
        }
      }
    }, function (err, customer) {

      if (err) {
        console.error("Error while fetching customer: ", err);
        fail();
        return;
      }

      if (!customer) {
        fail();
        return;
      }

      var customer = customer.toObject();

      var images = [];

      for (var i in customer.products) {
        var product = customer.products[i];
        for (var j in product._pictures) {
          if (product._pictures[j].principal) {
            images.push(product._pictures[j].url);
            continue;
          }
        }
      }

      var description = "";
      description = _.truncate(customer.description, {
        'length': 150,
        'separator': ' '
      });


      var render = loopback.template(path.resolve(__dirname, '../views/meta-tag/productor-tag.ejs'));
      var html = render({
        images: images,
        title: customer.name + " " + customer.lastname + " - Cuoco per Ucooki",
        description: description
      });

      success(html);
      return;

    });


  }

  return function (req, res, next) {


    var ua = req.headers['user-agent'];

    //    console.log("ua: ", ua);
    //    console.log("req.url: ", req.url);
    //    console.log("________");


    if (/^(facebookexternalhit)|(Facebot)|(Twitterbot)|(Pinterest)/gi.test(ua)) {
      console.log("Serve social-page to "+ ua +" per URL: ", req.url);

      if (req.url.indexOf("pages") <= -1) {
        // nessuna richiesta da gestire
        next();
        return;
      }

      var params = req.url.split("/");

      if (params && params.length > 0) {

        var id = params[params.length - 1];

        if (req.url.indexOf("product") > -1) {

          createHtmlProduct(id, function (html) {
            if (html) {
              res.send(html);
              return;
            }
          }, function () {
            next();
            return;
          });

        } else if (req.url.indexOf("customer") > -1) {
          createHtmlProductor(id, function (html) {
            if (html) {
              res.send(html);
              return;
            }
          }, function () {
            next();
            return;
          });
        } else {
          next();
          return;
        }
      }
    } else {
      next();
      return;
    }
  }
}
