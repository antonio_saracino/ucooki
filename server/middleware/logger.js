module.exports = function() {
  return function logResponse(req, res, next) {
    // http://www.senchalabs.org/connect/responseTime.html
    var start = new Date;
    if (res._responseTime) {
      return next();
    }
    res._responseTime = true;

    // install a listener for when the response is finished
    res.on('finish', function() { // the request was handled, print the log entry
      var duration = new Date - start;
      var data = "";
      var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
      if (req.body && Object.keys(req.body).length > 0) {
        data = " === " + JSON.stringify(req.body);
      }

      console.log("[" + ip + "]" , "\t", req.method, "\t\t", res.statusCode, duration + 'ms', "\t\t", req.originalUrl, data);
    });

    // resume the routing pipeline,
    // let other middleware to actually handle the request
    next();
  };
};
