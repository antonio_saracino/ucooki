module.exports = function() {
  return function(err, req, res, next) {

    err.message = 'Message: ' + err.message;
    err.status = 404; // override the status

    // remove the statusCode property
    delete err.statusCode;

    // console.log("url not found: " + req.url);
//		next();
		res.redirect("/#/");
//		res.status(404).send("Not found: " + req.url);
  }
}
