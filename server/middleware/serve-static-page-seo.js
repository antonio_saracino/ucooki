var request = require('request');
var app = require('../server');
var snapshot = require('../lib/snapshot');
const hostname = app.get("hostname");
const snapshotsUrl = app.get("snapshots-url");


module.exports = function () {

  return function (req, res, next) {

    if (!req.useragent.isPhantomJS && (req.useragent.isBot || typeof(req.query._escaped_fragment_) !== "undefined")) {

      // console.log("received a request. Useragent is a bot: ", req.useragent.isBot, ", url: " + req.url);

      var url = req.url.replace("?_escaped_fragment_=", "");

      if (url.indexOf(hostname) <= -1) {
        url = hostname + url;
      }

      if (url.indexOf('dashboard') > -1) {
        // ridirezioni sull homepage
        url = hostname + "/";
      }

      if (url === hostname + "/" || /\/pages\/[\w/\-_]*(?!\.\w{2,})$/gi.test(url)) { // considero solo la home e tutto quello che ha /pages/ nell'url e che non termina con un . seguito da lettere
        snapshot.render(url).then(function (content) {
          console.log("Serve snapshot html for: ", url, " user agent: ", req.headers["user-agent"],". bot: ", req.useragent.isBot);
          res.send(content);
        }).catch(function () {
          console.error("Error while fetching snapshot (" + url + ")");
          next();
        });
      } else {
        console.log("Ignore request: ", url);
        next();
      }


      // var filename = url;
      // if (url === '/') {
      //   filename = "/index";
      // }

      // request(snapshotsUrl + filename, function (error, response, body) {
      //   if (error) {
      //     console.error("Error during fetch snapshot (" + filename + ")" , error);
      //     next();
      //   } else if (response && response.statusCode == 200) {
      //     // console.log("Serve snapshot html: ", filename, " for request: ", url, ". bot: ", req.useragent.isBot);
      //     res.send(body);
      //   } else {
      //     next();
      //   }
      // });

    } else {
      next();
    }

  }
};

