'use strict';

module.exports = function (grunt) {
  grunt.loadNpmTasks("grunt-run");
  grunt.loadNpmTasks("grunt-contrib-less");
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-concurrent');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-usemin');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-string-replace');
  grunt.loadNpmTasks('grunt-flexible-sitemap-builder');

  var pkg = grunt.file.readJSON('package.json');

  grunt.initConfig({
    pkg: pkg,

    concat: {
      generated: {
        options: {
          sourceMap: true
        }
      }
    },

    uglify: {
      generated: {
        options: {
          sourceMap: true,
          sourceMapIncludeSources: true,
          sourceMapIn: '.tmp/concat/assets/js/app.js.map'
        },
        files: [{
          expand: true,
          dest: 'client/assets/js/',
          cwd: '.tmp/concat/assets/js/',
          src: ['*.js']
        }]
      }

    },

    cssmin: {
      generated: {
        files: [{
          dest: 'client/assets/css/style.css',
          src: ['.tmp/concat/assets/css/style.css']
        }]
      }
    },

    // run loopback server
    run: {
      lb: {
        args: ['.']
      },
      forever: {
        cmd: '../restart.sh'
      },
      'update-git': {
        cmd: '../update_git.sh'
      },
      'npm-install': {
        cmd: 'npm',
        args: ['install']
      },
      'lbservices': {
        cmd: 'lb-ng',
        args: ['./server/server.js', './client-src/components/services/lb-service/lb-service.js']
      },
      'bower': {
        cmd: 'bower',
        args: ['install']
      }
    },

    less: {
      dev: {
        files: {
          "client-src/assets/css/style.css": "client-src/assets/less/style.less"
        }
      },
      dist: {
        options: {
          compress: true,
          yuicompress: true
        },
        files: {
          ".tmp/concat/assets/css/style.css": "client-src/assets/less/style.less"
        }
      }
    },

    watch: {
      options: {
        livereload: true
      },
      css: {
        files: 'client-src/assets/less/*.less',
        tasks: ['less:dev']
      },
      html: {
        files: [
          'client-src/*.html',
          'client-src/**/*.html'
        ]
      },
      js: {
        files: [
          'client-src/*.js',
          'client-src/**/*.js'
        ]
      },
      static: {
        files: [
          'client-static/landing/*.html',
          'client-static/landing/**/*.css',
          'client-static/landing/**/*.js'
        ]
      }
    },

    flexible_sitemap_builder: {
      sitemap: {
        options: {
          baseurl: 'http://www.ucooki.com/',
          compress: false,
          default_settings: "daily,0.5",
          indexes: ['index.html']
        },
        files: [{
          expand: false,
          cwd: 'client-static/landing',
          src: ['**/*.html', '!404.html', '!thankyou.html', '!partials/*.*'],
          dest: 'client/sitemap.xml'
        }]
      }
    },

    concurrent: {
      serve: ['run:lb', 'watch'],
      options: {
        logConcurrentOutput: true
      }
    },

    clean: {
      dev: ['client-src/assets/css/*'],
      dist: ['client/*', '!client/README.md'],
      tmp: ['.tmp'],
      landing: ['client/*'],
      clientpartials: ['client/partials']
    },

    useminPrepare: {
      html: 'client-src/index.html',
      options: {
        dest: 'client'
      }
    },

    usemin: {
      html: ['client/index.html']
    },

    copy: {
      index: {
        src: 'client-src/index.html',
        dest: 'client/index.html'
      },
      fonts: {
        files: [{
          expand: true,
          cwd: 'client-src/assets/fonts',
          src: ['**/*'],
          dest: 'client/assets/fonts/'
        }, {
          expand: true,
          cwd: 'client-src/assets/ucooki-theme/fonts',
          src: ['**/*'],
          dest: 'client/assets/ucooki-theme/fonts/'
        }]
      },
      components: {
        files: [{
          expand: true,
          cwd: 'client-src/components',
          src: ['**/*.html'],
          dest: 'client/components/'
        }]
      },
      images: {
        files: [{
          expand: true,
          cwd: 'client-src/assets/img',
          src: ['**/*'],
          dest: 'client/assets/img/'
        }, {
          expand: true,
          cwd: 'client-src/assets/ucooki-theme/img',
          src: ['**/*'],
          dest: 'client/assets/ucooki-theme/img/'
        }]
      },
      video: {
        files: [{
          expand: true,
          cwd: 'client-src/assets/ucooki-theme/video',
          src: ['**/*'],
          dest: 'client/assets/ucooki-theme/video/'
        }]
      },
      favicon: {
        src: 'client-src/favicon.ico',
        dest: 'client/favicon.ico'
      },
      landing: {
        files: [{
          expand: true,
          cwd: 'client-static/landing',
          src: ['**/*'],
          dest: 'client/'
        }]
      }
    },

    'string-replace': {
      'remove-analytics': {
        files: {
          'client/': 'client/*.html'
        },
        options: {
          replacements: [{
            pattern: '<!-- @import partials/google-analytics.html -->',
            replacement: ''
          }, {
            pattern: '<!-- @import partials/google-conversion-cooker.html -->',
            replacement: ''
          }, {
            pattern: '<!-- @import partials/facebook-pixel.html -->',
            replacement: ''
          }]
        }
      },
      'remove-contest-prod': {
        files: {
          'client/': 'client/contest.html'
        },
        options: {
          replacements: [{
            pattern: '<!-- @import partials/contest-css-prod.html -->',
            replacement: ''
          }, {
            pattern: '<!-- @import partials/contest-js-prod.html -->',
            replacement: ''
          }]
        }
      },
      'remove-contest-dev': {
        files: {
          'client/': 'client/contest.html'
        },
        options: {
          replacements: [{
            pattern: '<!-- @import partials/contest-css-dev.html -->',
            replacement: ''
          }, {
            pattern: '<!-- @import partials/contest-js-dev.html -->',
            replacement: ''
          }]
        }
      },
      'remove-header': {
        files: {
          'client/': 'client/*.html'
        },
        options: {
          replacements: [{
            pattern: '<!-- @import partials/header.html -->',
            replacement: ''
          }]
        }
      },
      'remove-facebook-pixel': {
        files: {
          'client/': 'client/*.html'
        },
        options: {
          replacements: [{
            pattern: '<!-- @import partials/facebook-pixel.html -->',
            replacement: ''
          }]
        }
      },
      'landing-import': {
        files: {
          'client/': 'client/*.html'
        },
        options: {
          replacements: [{
            pattern: /<!-- @import (.*?) -->/ig,
            replacement: function (match, p1) {
              return grunt.file.read('client/' + p1);
            }
          }]
        }
      },
      'disable-dev': {
        files: {
          'server/middleware.json': 'server/middleware.json'
        },
        options: {
          replacements: [{
            pattern: '{ "paths": "/dev", "params": "$!../client-src" },',
            replacement: ''
          }]
        }
      },
      'add-partials': {
        files: {
          'client/': 'client/**/*.html'
        },
        options: {
          replacements: [{
            pattern: /<!-- @import (.*?) -->/ig,
            replacement: function (match, p1) {
              return grunt.file.read('client/' + p1);
            }
          }]
        }
      }
    }

  });

  grunt.registerTask('createLbService', [
    'copy:favicon',
    'run:lbservices'
  ]);

  grunt.registerTask('copyDist', [
    'copy:index',
    'copy:fonts',
    'copy:components',
    'copy:images',
    'copy:video',
    'copy:favicon'
  ]);

  // development server
  grunt.registerTask('serve', [
    'run:npm-install',
    'run:bower',
    'copy:favicon',
    'clean:dev',
    'less:dev',
    'createLbService',
    'concurrent:serve'
  ]);

  grunt.registerTask('dist', [
    'run:npm-install',
    'run:bower',
    'clean:dist',
    'less:dist',
    'createLbService',
    'copyDist',
    'string-replace:add-partials',
    'useminPrepare',
    'concat',
    'uglify',
    'usemin',
    'cssmin',
    'clean:tmp'
  ]);

  grunt.event.on('watch', function (action, filepath, target) {
    grunt.log.writeln(target + ': ' + filepath + ' has ' + action);
  });

};
