var async = require('async');
var loopback = require('loopback');
var app = require('../../server/server');
var _ = require('lodash');
var moment = require('moment-timezone');
var cloudinary = require('cloudinary');
var path = require('path');
var utils = require('../../server/lib/utils.js');

module.exports = function (Product) {

  //  Product.disableRemoteMethod('prototype.__get__ratings', true);
  //  Product.disableRemoteMethod('prototype.__create__ratings', true);
  Product.disableRemoteMethod('prototype.__delete__ratings', true);
  Product.disableRemoteMethod('prototype.__destroyById__ratings', true);
  Product.disableRemoteMethod('prototype.__updateById__ratings', true);


  //************************ Custom remote FIND PRODUCT NEAR ****************************//

  Product.findProductNear = function (lat, lng, limit, skip, distance, day, meal, featured, cb) {

    Product.getList(1, 1, -1, null, lat, lng, distance, day, meal, limit, skip, function (err, result) {

      if (err) {
        cb(err);
        return;
      }

      result.docs.forEach(function (customer) {
        // elimino i ratings
        customer.product._ratings = undefined;

      });

      cb(null, result);

    })
  };

  Product.remoteMethod(
    'findProductNear', {
      accepts: [{
        arg: 'lat',
        type: 'number',
        required: true
      }, {
        arg: 'lng',
        type: 'number',
        required: true
      }, {
        arg: 'limit',
        type: 'number'
      }, {
        arg: 'skip',
        type: 'number'
      }, {
        arg: 'distance',
        type: 'number',
        description: "Distanza massima di ricerca. Il valore di default è 5000 metri. Il valore massimo è 10000 metri."
      }, {
        arg: 'day',
        type: 'number',
        description: "Giorno in cui vorremmo mangiare, -1 indica tutti i giorni, 0 il giorno corrente, 1 domani, etc. Valore massimo 7"
      }, {
        arg: 'meal',
        type: 'string',
        description: "Specifica se vogliamo i piatti per il pranzo o la cena, valori possibili: lunch, dinner o null che sta a significare entrambi"
      }, {
        arg: 'featured',
        type: 'object',
        description: "Specifica se vogliamo caricare piatti in evidenza"
      }],
      returns: {
        arg: 'result',
        type: 'object'
      },
      http: {
        path: '/near',
        verb: 'get',
        errorStatus: 400,
        status: 200
      },
      description: ["Metodo che permette di ricevere i prodotti in base alla posizione"]
    }
  );

  //************************ Custom remote FIND FEATURED PRODUCT NEAR ****************************//

  Product.findFeaturedNear = function (lat, lng, limit, day, meal, cb) {

    Product.findProductNear(lat, lng, limit, 0, -1, day, meal, true, function (err, result) {

      if (err) {
        console.error(err);
        cb(err);
        return;
      }

      var docs = result.docs;

      if (docs.length >= limit) {
        cb(null, {
          docs: docs,
          day: day,
          meal: meal
        });
        return;
      }

      var num = limit - docs.length;

      // non ho trovato featured a sufficienza
      Product.findProductNear(lat, lng, num, 0, -1, day, meal, false, function (err, result) {

        if (err) {
          console.error(err);
          cb(err);
          return;
        }

        docs = docs.concat(result.docs);

        if (docs.length >= limit) {
          cb(null, {
            docs: docs,
            day: day,
            meal: meal
          });
          return;
        }

        var num = limit - docs.length;

        // ancora non bastano, ne prendo a caso tra tutti i piatti disponibili
        Product.findProductNear(lat, lng, num, 0, -1, -1, meal, false, function (err, result) {

          if (err) {
            console.error(err);
            cb(err);
            return;
          }

          docs = docs.concat(result.docs);

          // di più non posso fare
          cb(null, {
            docs: docs,
            day: -1,
            meal: meal
          });
        });
      });
    });

  }


  Product.remoteMethod(
    'findFeaturedNear', {
      accepts: [{
        arg: 'lat',
        type: 'number',
        required: true
      }, {
        arg: 'lng',
        type: 'number',
        required: true
      }, {
        arg: 'limit',
        type: 'number'
      }, {
        arg: 'day',
        type: 'number',
        description: "Giorno in cui vorremmo mangiare, -1 indica tutti i giorni, 0 il giorno corrente, 1 domani, etc. Valore massimo 7"
      }, {
        arg: 'meal',
        type: 'string',
        description: "Specifica se vogliamo i piatti per il pranzo o la cena, valori possibili: lunch, dinner o null che sta a significare entrambi"
      }],
      returns: {
        arg: 'result',
        type: 'object'
      },
      http: {
        path: '/featured',
        verb: 'get',
        errorStatus: 400,
        status: 200
      },
      description: ["Metodo che permette selezionare i piatti in evidenza"]
    }
  );

  //************************ Custom remote GET PRODUCT LIST ****************************//

  Product.getList = function (status, active, featured, text, lat, lng, distance, day, meal, limit, skip, cb) {

    // var lat = 44.494887;
    // var lng = 11.342616;

    // lat= 45.465454;
    // lng= 9.186515999999983;

    if (!limit) {
      limit = 10;
    }

    if (!skip) {
      skip = 0;
    }

    if (!distance) {
      distance = 100000;
    }

    if (day === undefined || day === null || day < -1 || day > 7) {
      day = -1;
    }

    if (!meal) {
      meal = "lunch";
    }

    var pipeline = [];

    if (!!lat && !!lng) {
      var geoNear = {
        $geoNear: {
          spherical: true,
          near: {
            type: "Point",
            coordinates: [parseFloat(lat), parseFloat(lng)]
          },
          distanceField: "distance",
          maxDistance: distance
        }
      }

      pipeline.push(geoNear);
    }

    pipeline.push(
      {
        $match: {
          cooker: true
        }
      },
      {
        $project: {
          name: 1,
          lastname: 1,
          fullname: {
            $concat: ["$name", " ", "$lastname"]
          },
          _picture: 1,
          email: 1,
          cooker: 1,
          productor_info: 1,
          kitchen_location: 1,
          distance: 1
        }
      },
      {
        $lookup: {
          "from": "Product",
          "localField": "_id",
          "foreignField": "customer_id",
          "as": "product"
        }
      },
      {
        $unwind: {
          path: "$product"
        }
      }
    );

    if (day >= 0) {
      // sto effettuando una ricerca per giorno
      var mealType = meal == "lunch" ? "time_ranges.endLunch" : "time_ranges.endDinner";
      day = parseInt(day);

      var now = moment().tz("Europe/Rome");
      // giorno in cui è richiesto il servizio
      var targetDay = now.clone().add(day, "day");
      targetDay.hour(0);
      targetDay.minute(0);
      targetDay.second(0);

      var dayOfWeek = targetDay.day();
      if (dayOfWeek == 0) {
        dayOfWeek = 7;
      }

      var match = {
        "time_ranges.day": dayOfWeek
      };
      match[mealType] = {$gt: 0};

      pipeline.push(
        {
          $project: {
            name: 1,
            lastname: 1,
            fullname: {
              $concat: ["$name", " ", "$lastname"]
            },
            _picture: 1,
            email: 1,
            cooker: 1,
            productor_info: 1,
            kitchen_location: 1,
            distance: 1,
            product: 1,
            time_ranges: '$product._timeRanges'
          }
        },
        {
          $unwind: {
            path: "$time_ranges"
          }
        },
        {
          $match: match
        },
        {
          $project: {
            _id: 1,
            name: 1,
            lastname: 1,
            cooker: 1,
            _picture: 1,
            email: 1,
            kitchen_location: 1,
            productor_info: 1,
            fullname: 1,
            product: 1,
            distance: 1,
            time_ranges: 1,
            diff: {
              $let: {
                vars: {
                  hour: {$divide: ["$" + mealType, 100]},
                  min: {$subtract: ["$" + mealType, {$multiply: [{$floor: {$divide: ["$" + mealType, 100]}}, 100]}]}
                },
                in: {$floor: {$divide: [{$subtract: [{$add: [{$multiply: [3600, "$$hour"]}, {$multiply: [60, "$$min"]}, parseInt(targetDay.format("X"))]}, parseInt(now.format("X"))]}, 3600]}} // il primo timestamp è l'ultimo momento utile per ordinare, mentre il secondo rappresenta l'orario corrente
              }
            }
          }
        },
        {
          $project: {
            _id: 1,
            name: 1,
            lastname: 1,
            cooker: 1,
            _picture: 1,
            email: 1,
            kitchen_location: 1,
            productor_info: 1,
            fullname: 1,
            product: 1,
            distance: 1,
            diff: 1,
            in_time: {$gte: ["$diff", "$product.notice_time"]}
          }
        }
      );

    }

    var filter = {$match: {}};

    if (day >= 0) {
      filter.$match["in_time"] = true;
    }

    if (status >= 0) {
      filter.$match["product.status"] = status;
    }

    if (active >= 0) {
      filter.$match["product.active"] = active == 0 ? false : true;
    }

    if (featured >= 0) {
      filter.$match["product.featured"] = featured == 0 ? false : true;
    }

    if (text) {
      var regex = utils.stringToRegex(text);
      filter.$match["$or"] = [{"fullname": regex}, {"email": regex}, {"product.name": regex}];
    }

    pipeline.push(filter);

    var Customer = Product.app.models.Customer;
    var customerCollection = Product.getDataSource().connector.collection(Customer.modelName);

    var pipelineCount = pipeline.slice(0);
    pipelineCount.push({$group: {_id: null, count: {$sum: 1}}});

    var cursor = customerCollection.aggregate(pipeline).skip(skip).limit(limit);

    var result = {
      total: 0,
      docs: []
    };

    cursor.each(function (err, doc) {
      if (err) return errorHandler(err, cb);
      if (!doc) {
        cursor.close();
        customerCollection.aggregate(pipelineCount, function (err, res) {
          if (err) return errorHandler(err, cb);
          if (res && res.length > 0) {
            result.total = res[0].count;
          }
          cb(null, result);
          return;
        })
      } else {
        result.docs.push(doc);
      }

    });

  }

  Product.remoteMethod(
    'getList', {
      accepts: [{
        arg: 'status',
        type: 'number'
      }, {
        arg: 'active',
        type: 'number'
      }, {
        arg: 'featured',
        type: 'number'
      }, {
        arg: 'text',
        type: 'string',
        description: "Testo da filtrare sul nome, cognome o sull'email del cuoco o sul nome del piatto"
      }, {
        arg: 'lat',
        type: 'number'
      }, {
        arg: 'lng',
        type: 'number'
      }, {
        arg: 'distance',
        type: 'number',
        description: "Distanza massima di ricerca. Il valore di default è 5000 metri. Il valore massimo è 10000 metri."
      }, {
        arg: 'day',
        type: 'number',
        description: "Giorno in cui vorremmo mangiare, -1 indica tutti i giorni, 0 il giorno corrente, 1 domani, etc. Valore massimo 7"
      }, {
        arg: 'meal',
        type: 'string',
        description: "Specifica se vogliamo i piatti per il pranzo o la cena, valori possibili: lunch, dinner o null che sta a significare entrambi"
      }, {
        arg: 'limit',
        type: 'number'
      }, {
        arg: 'skip',
        type: 'number'
      }],
      returns: {
        arg: 'result',
        type: 'object'
      },
      http: {
        path: '/list',
        verb: 'get',
        errorStatus: 400,
        status: 200
      },
      description: ["Metodo che permette di ricevere tutti i piatti registrati e i rispettivi customer"]
    }
  );

  //************************ Custom remote CHANGE STATUS ****************************//


  Product.changeStatus = function (id, status, cb) {

    if (!id) {
      cb();
      return;
    }

    var include = {
      include: {
        relation: "customer",
        scope: {
          fields: ["name", "lastname", "email"]
        }
      }
    }

    Product.findById(id, include, function (err, product) {

      if (err) {
        cb(err);
        return;
      }

      if (product) {
        product.__data.status = status;
        product.save(function (err, entity) {
          if (err) {
            cb(err);
            return;
          }

          sendProductNotification(product);

          cb(null, entity);

        });
      } else {
        cb(null, product);
      }

    });

  }

  Product.remoteMethod(
    'changeStatus', {
      accepts: [{
        arg: 'id',
        type: 'string',
        required: true
      }, {
        arg: 'status',
        type: 'number',
        required: true
      }],
      returns: {
        arg: 'result',
        type: 'object'
      },
      http: {
        path: '/:id/change-status',
        verb: 'put',
        errorStatus: 400,
        status: 200
      },
      description: ["Determino lo stato del prodotto"]
    }
  );

  //************************ Model hook ****************************//

  Product.observe('before save', function (ctx, next) {

    //    console.log("bs ctx.data:", ctx.data);
    //    console.log("bs ctx.instance:", ctx.instance);
    //    console.log("bs ctx.currentInstance:", ctx.currentInstance);
    //    console.log("bs ctx.where:", ctx.where);
    //    console.log("bs ctx.isNewInstance:", ctx.isNewInstance);

    var product = null;

    if (ctx.instance && ctx.isNewInstance) {
      // nuova entità
      product = ctx.instance;
      product.created = new Date();
    } else if (ctx.data) {
      // sto aggiornando il prodotto
      product = ctx.data;
    }

    if (product) {
      product.updated = new Date();
      if (product.name) {
        product.name = _.capitalize(product.name);
      }

      if (product.description) {
        product.description = utils.capitalizeSentence(product.description);
      }

      if (product._ingredients) {
        product._ingredients.forEach(function (ingredient) {
          ingredient.description = utils.capitalizeSentence(ingredient.description);
          ingredient.name = _.capitalize(ingredient.name);
        });
      }

      if (product.tags) {
        var tags = [];
        product.tags.forEach(function (tag) {
          tags.push(_.lowerCase(tag));
        });
        product.tags = tags;
      }
    }

    next();

  });

  Product.observe('before delete', function(ctx, next) {

    Product.find({
      where: ctx.where,
      fields: {
        id: true,
        _pictures: true
      }
    }, function (err, products) {
      if (err) {
        console.error(err);
        next(err);
        return;
      }

      if (products && products.length > 0) {
        ctx.hookState.products = products;
      }

      next();
    });

  });

  Product.observe('after delete', function (ctx, next) {

    if (ctx.hookState.products) {
      ctx.hookState.products.forEach(function (product) {

        if (product._pictures) {
          product._pictures.forEach(function(picture) {
            cloudinary.uploader.destroy(picture.id, function (result) {
              // console.log("Picture deleted for product: ", product, " response: ", result);
            }, { invalidate: true });
          });
        }
      });
    }

    next();

  });

  //************************ Remote hook ****************************//

  //    Product.beforeRemote('**', function(ctx, user, next) {
  //      console.log(ctx.methodString, 'was invoked remotely'); // customers.prototype.save was invoked remotely
  //      next();
  //    });

  Product.beforeRemote('upsert', function (ctx, instance, next) {
    //    console.log("token: ", ctx.req.accessToken);
    //    console.log("instance: ", ctx.instance);
    //    console.log("data: ", ctx.args.data);

    if (!ctx || !ctx.args || !ctx.args.data) {
      err = new Error('No valid context');
      err.statusCode = 422;
      err.code = 'CONTEXT_ERROR';
      next(err)
      return;
    }

    var data = ctx.args.data;
    // NON devo aggiornare i miei ratings
    // Ne lo stato perchè non è consentito al singolo utente
    // Ne featured
    delete data._ratings;
    delete data.status;
    delete data.featured;
    // delete data.active;

    var Customer = Product.app.models.Customer;

    Customer.findById(ctx.req.accessToken.userId, function (err, user) {
      if (err) return errorHandler(err, next);
      if (!user) return errorHandler("unable to find who made request. Userid = " + ctx.req.accessToken.userId, next);

      user.isAdmin(function (result) {
        if (result) {
          // non c'è bisogno di verificare, sono l'admin
          ctx.hookState = {
            adminUpdated: true
          }
          next();
          return;
        }

        isToValidate(data, function (result) {

          if (result) {
            // devo verificarla, quindi lo stato va a zero;
            // altrimenti rimane come è
            data.status = 0;
            ctx.hookState = {
              notify: true
            }
          }

          next();
        });

      });

    });

  });

  Product.afterRemote('upsert', function (ctx, instance, next) {

    if (ctx && ctx.hookState) {
      if (ctx.hookState.notify) {
        sendValidateNotification(instance.toObject());
      } else if (ctx.hookState.adminUpdated) {
        instance.customer(function (err, customer) {
          sendAdminChangedNotification(instance);
        });
      }
    }

    next();

  });

  Product.beforeRemote('prototype.__create__ratings', function (ctx, instance, next) {

    //    console.log("token: ", ctx.req.accessToken);
    //    console.log("instance: ", ctx.instance);
    //    console.log("data: ", ctx.args.data);

    ctx.hookState = {test: true};

    if (!ctx.req.accessToken) {
      err = new Error('User not authenticated');
      err.statusCode = 401;
      err.code = 'USER_NOT_AUTHENTICATED';
      next(err);
    }

    var accessToken = ctx.req.accessToken.__data;
    var rating = ctx.args.data;
    var product = ctx.instance.__data;
    var reject = function (message) {
      err = new Error(message);
      err.statusCode = 422;
      err.code = 'REQUEST_ERROR';
      next(err);
    };

    rating.date = new Date();

    Product.app.models.Order.findById(rating.order_id, function (err, order) {

      if (err) {
        reject(err.message);
        return;
      }

      if (!order) {
        reject("Unable to find order by id");
        return;
      }

      var order = order.__data;
      //			console.log("order: ", order);

      if (order.buyer_id.toString() != accessToken.userId.toString()) {
        reject("Cannot publish a rating for someone else");
        return;
      }

      if (order.buyer_id.toString() != rating.buyer_id.toString()) {
        reject("Incompatible user with rating");
        return;
      }

      if (order.status != 3) {
        reject("Order is not completed");
        return;
      }

      var item;
      for (var i = 0; i < order.items.length; i++) {
        var current = order.items[i];
        if (current.product_id == product.id.toString()) {
          item = current;
          break;
        }
      }

      if (!item) {
        reject("Incompatible product with any order's item");
        return;
      }

      // tutto ok
      next();
    });
  });

  Product.afterRemote('prototype.__create__ratings', function (ctx, rating, next) {

    //    console.log("token: ", ctx.req.accessToken);
    //    console.log("instance: ", ctx.instance);
    //    console.log("data: ", ctx.args.data);
    //		console.log("rating: ", rating);

    if (!rating || !ctx.instance) {
      next();
      return;
    }

    var Customer = Product.app.models.Customer;
    var filter = {
      where: {
        or: [{
          "id": rating.buyer_id
        }, {
          "id": ctx.instance.__data.customer_id
        }]
      }
    }

    Customer.find(filter, function (err, customers) {

      if (err) {
        console.error("Error occurred while search customers.", err);
        next();
        return;
      }

      if (customers.length < 2) {
        console.error("Unable to find buyer and productor for product with id: " + ctx.instance.__data.id);
        next();
        return;
      }

      var buyer, productor;

      if (customers[0].__data.id.toString() == rating.buyer_id.toString()) {
        buyer = customers[0].toObject();
        productor = customers[1].toObject();
      } else {
        buyer = customers[1].toObject();
        productor = customers[0].toObject();
      }

      sendRatingNotification(rating, ctx.instance.toObject(), buyer, productor);
      next();

    })


  });

  Product.afterRemote('findById', function (context, product, next) {

    product.updateRate();

    product.customer(function (err, customer) {

      if (err || !customer) {
        console.error("Error occurred during productor fetch.", err, product);
        next(err);
        return;
      }

      customer = customer.toObject();

      product.__data.productor = {
        id: customer.id,
        name: customer.name,
        lastname: customer.lastname,
        email: customer.email,
        kitchen_address: customer.kitchen_address,
        kitchen_location: customer.kitchen_location,
        kitchen_location_info: customer.kitchen_location_info,
        _picture: customer._picture,
        productor_info: customer.productor_info
      }

      async.each(product._ratings, function (rating, done) {

        var Customer = app.models.Customer;

        Customer.findById(rating.buyer_id, {
          fields: {
            name: true,
            _picture: true
          }
        }, function (err, customer) {

          if (err) {
            console.error(err);
            done(err);
            return;
          }

          if (!customer) {
            console.error("User not found, id: " + rating.buyer_id);
            done();
            return;
          }

          rating.__data.buyer = customer.__data;
          done();
        });

      }, function (err) {
        if (err) console.error(err);
        next();
      });
    });
  });

  Product.afterRemote('find', function (context, products, next) {

    products.forEach(function (product) {
      product.updateRate();
    });

    next();

  });

  Product.prototype.updateRate = function () {
    var ratings = this.__data._ratings;
    var ratingScore = 0;
    ratings.forEach(function (rating) {
      ratingScore += rating.rate;
    });

    if (ratings.length) {
      ratingScore = ratingScore / ratings.length;
    }
    this.__data.rate = ratingScore;
  }

  /************************* Utility *******************************/

  var isToValidate = function (data, callback) {

    if (!data) {
      callback(false);
      return;
    }

    if (!data.id) {
      // è nuovo, è da controllare solo se ha delle foto
      if (data._pictures && data._pictures.length > 0) {
        callback(true);
        return;
      }

      callback(false);
      return;
    }

    Product.findById(data.id, function (err, product) {
      if (err) {
        console.error(err);
        callback(false);
        return;
      }

      if (!data._pictures || data._pictures.length <= 0) {
        // se non ha foto non è da valutare
        callback(false);
        return;
      }

      if (!product) {
        // è nuovo
        callback(true);
        return;
      }

      var p = product.toObject();

      // cerco se c'è qualche variazione
      // verifico se ci sono delle nuove foto

      for (var i in data._pictures) {
        var original = data._pictures[i];
        var found = false;

        for (var j in p._pictures) {
          var old = p._pictures[j];
          if (original.id == old.id) {
            found = true;
            continue;
          }
        }

        if (!found) {
          callback(true);
          return;
        }
      }

      callback(false);

    })

  }


  var sendValidateNotification = function (product) {

    if (!product || product.status == 1) {
      return;
    }

    try {

      var adminSubject = "Notifica";

      switch (product.status) {
        case 0:
          adminSubject += " : prodotto in attesa di approvazione";
          break;
      }

      // invio della notifica agli admin
      var options = {
        from: "Ucooki <info@ucooki.com>", // sender address
        to: Product.app.get('admin-email'), // list of receivers
        subject: adminSubject, // Subject line
        html: "<h2>Prodotto in attesa di approvazione</h2><pre>" + JSON.stringify(product, null, 4) + "</pre>" // html body
      }

      Product.app.models.Email.send(options, function (err, mail) {
        if (err) console.log('error occurred during sending email to admin!', err);
      });

    } catch (e) {
      console.error(e);
      console.trace();
    }
  }

  var sendAdminChangedNotification = function (product) {
    if (!product) {
      return;
    }

    try {

      var render = loopback.template(path.resolve(__dirname, '../../server/views/notifications/product-admin-changed.ejs'));

      var prod = product.toObject();
      prod.customer = product.__cachedRelations.customer;

      var object = {
        product: prod,
        subject: "Miglioramento al piatto"
      }

      // invio della notifica al cuoco
      var options = {
        from: "Ucooki <info@ucooki.com>", // sender address
        to: prod.customer.email, // list of receivers
        subject: object.subject, // Subject line
        html: render(object)
      }

      // console.log(render(object));

      Product.app.models.Email.send(options, function (err, mail) {
        if (err) console.log('error occurred during sending email to admin!', err);
      });

    } catch (e) {
      console.error(e);
      console.trace();
    }

  }

  var sendRatingNotification = function (rating, product, buyer, producer) {

    if (!product || !product || !buyer || !producer) {
      return;
    }

    try {

      var Email = Product.app.models.Email;
      var subjectBuyer = "Commento Inviato";
      var subjectProductor = "Commento Ricevuto";
      var render = loopback.template(path.resolve(__dirname, '../../server/views/notifications/product-rating.ejs'));

      var htmlBuyer = render({
        subject: subjectBuyer,
        rating: rating,
        product: product,
        buyer: buyer,
        productor: producer,
        destination: 'buyer'
      });

      var htmlProductor = render({
        subject: subjectProductor,
        rating: rating,
        product: product,
        buyer: buyer,
        productor: producer,
        destination: 'productor'
      });

      var emailOptBuyer = {
        to: buyer.email,
        from: 'Ucooki <info@ucooki.com>',
        subject: subjectBuyer,
        html: htmlBuyer
      }

      var emailOptProductor = {
        to: producer.email,
        from: 'Ucooki <info@ucooki.com>',
        subject: subjectProductor,
        html: htmlProductor
      }

      //    console.log("sent to buyer:\n\n " + htmlBuyer, "\n\n, sent to productor:\n\n " + htmlProductor);

      var sendCallback = function (err, mail) {
        if (err) {
          console.log('error occurred during sending rating notification to client email!', err);
          return;
        }
      }

      Email.send(emailOptBuyer, sendCallback);
      Email.send(emailOptProductor, sendCallback);

      // invio della notifica agli admin

      Email.send({
        to: Product.app.get('admin-email'),
        from: "info@ucooki.com",
        subject: "Nuovo Commento inviato",
        html: "<h2>Nuovo Commento per un piatto</h2><p>L'utente: <b>" + buyer.name + " " + buyer.lastname + "</b> ha votato il piatto: <b>" + product.name + "</b> di " + producer.name + " " + producer.lastname + "</p><p>Voto: <b>" + rating.__data.rate + "</b>/5</p><p>Commento: <b>" + rating.message + "</b></p>" // html body
      }, sendCallback);

    } catch (e) {
      console.error(e);
      console.trace();
    }

  }

  var sendChangeNotification = function (product) {

    if (!product || product.status == 1) {
      return;
    }

    var adminSubject = "Notifica";

    switch (product.status) {
      case 0:
        adminSubject += " : prodotto aggiornato";
        break;
    }

    // invio della notifica agli admin
    var options = {
      from: "Ucooki <info@ucooki.com>", // sender address
      to: Product.app.get('admin-email'), // list of receivers
      subject: adminSubject, // Subject line
      html: "<h2>Prodotto aggiornato</h2><pre>" + JSON.stringify(product, null, 4) + "</pre>" // html body
    }

    Product.app.models.Email.send(options, function (err, mail) {
      if (err) console.log('error occurred during sending email to admin!', err);
    });

  }

  var filterProducts = function (day, meal, products) {
    var result = [];

    if (day === undefined || day === null || day <= -1) {
      // nessun filtro da applicare
      return products;
    }

    day = parseInt(day);

    var now = moment().tz("Europe/Rome");
    var targetDay = now.clone().add(day, "day");
    var oneHour = 1000 * 60 * 60;

    var dayOfWeek = targetDay.day();
    if (dayOfWeek == 0) {
      dayOfWeek = 7;
    }

    for (var i in products) {
      var product = products[i];
      var timeRange = product._timeRanges[dayOfWeek - 1];
      var startMeal, endMeal;
      if (meal == "lunch") {
        // verifico la disponibilità per il pranzo
        if (timeRange.endLunch == -1) {
          // non è un giorno valido
          continue;
        }
        endMeal = "" + timeRange.endLunch;

      } else {
        // verifico la disponibilità per la cena
        if (timeRange.endDinner == -1) {
          // non è un giorno valido
          continue;
        }
        endMeal = "" + timeRange.endDinner;
      }

      // ok, verifichiamo se anche il preavviso è congruo
      // setto l'orario di fine fascia
      targetDay.hours(parseInt(endMeal.substring(0, 2)));
      targetDay.minutes(parseInt(endMeal.substring(2, 4)));

      //calcolo se la differenza tra le due date è superiore al preavviso richiesto
      var diff = (parseFloat(targetDay.format("x")) - parseFloat(now.format("x")));
      var hours = Math.floor(diff / oneHour);

      if (hours >= product.notice_time) {
        // ok, il prodotto è valido
        result.push(product);
      }

    }
    ;

    return result;
  }

  var errorHandler = function (err, cb) {
    if (err) {
      console.error("Error products endpoint: ", err);
      console.trace();
      cb(err);
      return;
    }
  }

  var sendProductNotification = function (product) {

    if (!product || product.status == 0) {
      return;
    }

    try {
      var Email = Product.app.models.Email;

      //    console.log("invio email...");

      var template = '../../server/views/notifications/product-accepted.ejs';
      var subject = "Piatto ACCETTATO";
      if (product.status == 2) {
        subject = "Piatto RIFIUTATO";
        template = '../../server/views/notifications/product-rejected.ejs'
      }

      var render = loopback.template(path.resolve(__dirname, template));
      var prod = product.toObject();
      prod.customer = product.__cachedRelations.customer;

      var object = {
        product: prod,
        subject: subject
      }

      // invio della notifica al cuoco
      var options = {
        from: "Ucooki <info@ucooki.com>", // sender address
        to: product.__cachedRelations.customer.email, // list of receivers
        subject: subject, // Subject line
        html: render(object)
      }

      // console.log(render(object));

      Email.send(options, function (err, mail) {
        if (err) console.log('error occurred during sending email to admin!', err);
      });
    } catch (e) {
      console.error(e);
      console.trace();
    }

  }

};
