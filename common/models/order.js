var loopback = require('loopback');
var path = require('path');
var moment = require('moment-timezone');
var async = require("async");
var skebby = require("../../server/lib/skebby");

module.exports = function (Order) {

  var DELAY = 15 * 24 * 60 * 60 * 1000; // ultimi 15 giorni

  /********************* Model hook ********************/


  Order.observe('before save', function (ctx, next) {

    //    console.log("bs ctx.data:", ctx.data);
    //    console.log("bs ctx.instance:", ctx.instance);
    //    console.log("bs ctx.currentInstance:", ctx.currentInstance);
    //    console.log("bs ctx.where:", ctx.where);
    //    console.log("bs ctx.hookState:", ctx.hookState);

    if (ctx.instance && ctx.isNewInstance) {
      ctx.instance.date = new Date();
      ctx.instance.updateTimeout();
    }

    if (!ctx.instance && ctx.where && ctx.data) {
      // sto facendo un aggiornamento di più elementi
      // quindi devo caricare la lista, mi sarà utile nel prossimo passaggio 'after save'
      Order.find({
        where: ctx.where,
        fields: {
          id: true
        }
      }, function (err, orders) {
        if (err) {
          console.error(err);
          next(err);
          return;
        }

        if (!orders || orders.length <= 0) {
          next();
          return;
        }

        ctx.hookState.orders = orders;
        next();
        return;
      });
    } else {
      // in questo caso non devo fare nulla, se è presente l'oggetto è dentro ad instance oppure currentInstance
      next();
    }

  });

  Order.observe('after save', function (ctx, next) {

    //    console.log("as ctx.data:", ctx.data);
    //    console.log("as ctx.instance:", ctx.instance);
    //    console.log("as ctx.hookState:", ctx.hookState);

    if (ctx.instance) {
      // ho salvato un'istanza singola
      var order = ctx.instance;
      order.loadData(function (err) {

        if (err) {
          console.error(err);
          next(err);
          return;
        }

        sendOrderNotification(order);
        next();
        return;

      });


    } else if (ctx.hookState && ctx.hookState.orders && ctx.hookState.orders.length > 0) {
      // in questo caso ho modificato più oggetti
      var idList = [];
      ctx.hookState.orders.forEach(function (id) {
        idList.push("" + id.id);
      });

      var filter = {
        where: {
          '_id': {
            inq: idList
          }
        }
      };

      Order.find(filter, function (err, orders) {
        if (err) {
          console.error(err);
          next(err);
          return;
        }

        orders.forEach(function (order) {

          order.loadData(function (err) {

            if (!err) {
              sendOrderNotification(order);
            }

          });
        });

        next();
        return;
      });

    } else {
      next();
      return;
    }

  });


  /********************* Custom API ********************/

  Order.countDishes = function (cb) {

    var orderCollection = Order.getDataSource().connector.collection(Order.modelName);

    orderCollection.aggregate({
        $match: {
          "status": 3
        }
      }, {
        $unwind: "$items"
      }, {
        $group: {
          "_id": 1,
          "total": {
            "$sum": "$items.price"
          },
          "dishes": {
            "$sum": "$items.quantity"
          }
        }
      },
      function (err, result) {
        if (err) {
          console.error(err);
          cb(err);
        } else {
          var count = 0;
          if (result && result[0]) {
            count = result[0].dishes;
          }
          cb(null, count);
        }
      });
  }

  Order.remoteMethod('countDishes', {
    returns: {
      arg: 'count',
      type: 'number'
    },
    http: {
      path: '/count-dishes',
      verb: 'get',
      errorStatus: 400,
      status: 200
    },
    description: ["Restituisce il numero di piatti attualmente consegnati"]
  })

  Order.getLatestOrders = function (userId, cb) {
    getOrders(userId, false, cb);
  }

  Order.remoteMethod('getLatestOrders', {
    returns: {
      arg: 'orders',
      type: 'object'
    },
    accepts: {
      arg: 'userId',
      type: 'string',
      required: true
    },
    http: {
      path: '/lastOrders',
      verb: 'get',
      errorStatus: 400,
      status: 200
    },
    description: ["Restituisce gli ultimi ordini inviati o ricevuti da un utente"]
  })


  Order.getOldestOrders = function (userId, cb) {
    getOrders(userId, true, cb);
  }

  Order.remoteMethod('getOldestOrders', {
    returns: {
      arg: 'orders',
      type: 'object'
    },
    accepts: {
      arg: 'userId',
      type: 'string',
      required: true
    },
    http: {
      path: '/oldestOrders',
      verb: 'get',
      errorStatus: 400,
      status: 200
    },
    description: ["Restituisce gli ultimi ordini inviati o ricevuti da un utente"]
  })

  /************************* Model Functions *******************************/

  /**
   * Carica le informazioni relative al buyer e al productor
   * e, se è nello stato 3 completato, carica anche i relativi feedback
   *
   */
  Order.prototype.loadData = function (cb) {

    cb = cb || function () {};
    var Customer = Order.app.models.Customer;

    var order = this;

    Customer.findById(order.$buyer_id, function (err, buyer) {

      if (err) {
        console.error(err);
        cb(err);
        return;
      }

      buyer = buyer || {};

      order.__data.buyer = {
        id: buyer.id,
        email: buyer.email,
        name: buyer.name,
        lastname: buyer.lastname,
        phone: buyer.phone
      }

      Customer.findById(order.$productor_id, function (err, productor) {

        if (err) {
          console.error(err);
          cb(err);
          return;
        }

        productor = productor || {};

        order.__data.productor = {
          id: productor.id,
          email: productor.email,
          name: productor.name,
          lastname: productor.lastname,
          phone: productor.phone
        }

        // mi preparo a caricare i feedback
        if (order.$status != 3) {
          // non è completato
          cb();
          return;
        }

        var rating;

        if (buyer) {
          // cerco il rating del cooker (lo cerco all'interno del documento del buyer!!!)
          for (var i = 0; i < buyer._ratings.length; i++) {
            current = buyer._ratings[i];
            if (current.order_id.equals(order.$id)) {
              rating = current;
              break;
            }
          }

          if (rating) {
            // ho trovato il rating del cuoco
            rating.__data.saved = true;
            order.__data.rating = rating.__data;
          }
        }


        // ora cerco i rating del eater per ogni item dell'ordine
        var itemIdList = [];
        order.$items.forEach(function (item) {
          itemIdList.push(item.product_id);
        })

        var filter = {
          where: {
            id: {
              inq: itemIdList
            }
          }
        };


        Order.app.models.Product.find(filter, function (err, products) {
          if (err) {
            console.error(err);
            cb(err);
            return;
          }

          if (!products || products.length <= 0) {
            cb();
            return;
          }

          products.forEach(function (product) {

            product._ratings.forEach(function (rating) {

              if (rating.order_id.equals(order.$id)) {
                // ho trovato un rating al prodotto
                // lo collego al giusto item
                for (var i = 0; i < order.$items.length; i++) {
                  var item = order.$items[i];
                  if (item.product_id == product.id.toString()) {
                    // ho trovato l'item a cui fa riferimento il rating
                    rating.__data.saved = true;
                    item.__data.rating = rating.__data;
                    break;
                  }
                }
              }

            });

          });
          cb();
        })
      });

    });
  };

  Order.prototype.updateTimeout = function() {

    var order = this;

    if (!order) {
      return;
    }

    var delivery = new Date(order.delivery_date);
    var request = new Date(order.date);

    // se la differenza tra l'ordine e la consegna è inferiore di un'ora e mezza
    // setterò il timeout sui 30 minuti prima della consegna (per dare tempo al cuoco di rispondere)
    // altrimenti il timeout è impostato a 60 minuti prima della consegna (caso normale)
    var minutes = parseInt((delivery.getTime() - request.getTime())/1000) < 5400 ? 30 : 60;

    // calcolo la data di timeout più vicina all'eventuale consegna (nel caso normale è un'ora prima della consegna)
    var timeoutMin = new Date(order.delivery_date);
    timeoutMin = new Date(timeoutMin.setMinutes(timeoutMin.getMinutes() - minutes));

    // calcolo la data 24 ore in avanti rispetto all'ora dell'ordine
    var timeout24Hour = new Date();
    timeout24Hour = new Date(timeout24Hour.setHours(timeout24Hour.getHours() + 24));

    // determino quale delle due date è la più piccola, quella sarà il mio timeout
    order.timeout = timeoutMin.getTime() <= timeout24Hour.getTime() ? timeoutMin : timeout24Hour;

  };

  /************************* Utility *******************************/

  var getOrders = function (userId, old, cb) {

    var results = {
      sent: [],
      received: []
    };

    if (!userId || userId == "") {
      cb(null, results);
      return;
    }

    var filterNew = {
      where: {
        and: [{
          or: [{
            "buyer_id": userId
          }, {
            "productor_id": userId
          }]
        }, {
          "delivery_date": {
            gte: Date.now() - DELAY
          }
        }]
      },
      order: "delivery_date ASC"
    }

    var filterOld = {
      where: {
        and: [{
          or: [{
            "buyer_id": userId
          }, {
            "productor_id": userId
          }]
        }, {
          "delivery_date": {
            lt: Date.now() - DELAY
          }
        }]
      },
      order: "delivery_date DESC"
    };

    var filter = old ? filterOld : filterNew;


    Order.find(filter, function (err, orders) {

      if (err) {
        cb(err);
        console.error(err);
        return;
      }

      // aggiorno i dati dei singoli ordini
      async.each(orders, function (order, cb) {

        order.loadData(function (err) {
          if (err) {
            console.error(err);
          }
          cb();
        });

      }, function (err) {
        if (err) {
          cb(err);
          console.error(err);
          return;
        }

        orders.forEach(function (order) {

          // pulisco il risultato
          order.items.forEach(function (item) {
            delete item.product._ingredients;
            delete item.product._timeRanges;
          });

          if (order.buyer_id == userId) {
            results.sent.push(order);
          } else {
            results.received.push(order);
          }

        });

        cb(null, results);

      });


    })

  };

  var sendOrderNotification = function (order) {

    if (!order) {
      return;
    }

    try {

      order = order.toObject();

      var Email = Order.app.models.Email;

      //    console.log("invio email...");

      var render = loopback.template(path.resolve(__dirname, '../../server/views/notifications/order.ejs'));

      var expiration = new Date(order.timeout);
      expiration = moment.tz(expiration, "Europe/Rome").format("HH:mm [del] DD/MM");

      var delivery_date = new Date(order.delivery_date);
      delivery_date = moment.tz(delivery_date, "Europe/Rome").format("DD/MM/YY [alle] HH:mm");

      var subject = "Notifica";
      switch (order.status) {
        case 0:
          subject = "Nuovo Ordine Ricevuto";
          break;
        case 1:
          subject = "Ordine Confermato";
          break;
        case 2:
          subject = "Ordine Pagato";
          break;
        case 3:
          subject = "Ordine Completato";
          break;
        case 4:
          subject = "Ordine Rifiutato";
          break;
        case 5:
          subject = "Ordine Cancellato";
          break;
        case 6:
          subject = "Ordine Annullato";
          break;
        case -1:
          subject = "Errore Ordine";
          break;
      }

      var ejsCooker = {
        order: order,
        expiration: expiration,
        delivery_date: delivery_date,
        cooker: true,
        subject: subject
      }

      var ejsEater = {
        order: order,
        expiration: expiration,
        delivery_date: delivery_date,
        cooker: false,
        subject: subject
      }

      var emailOptCooker = {
        to: order.productor.email,
        from: 'Ucooki <info@ucooki.com>',
        subject: subject,
        text: "Il tuo ordine ha aggiornato il suo stato, verifica sul sito http://www.ucooki.com",
        html: render(ejsCooker)
      }

      var emailOptEater = {
        to: order.buyer.email,
        from: 'Ucooki <info@ucooki.com>',
        subject: subject,
        text: "Il tuo ordine ha aggiornato il suo stato, verifica sul sito http://www.ucooki.com",
        html: render(ejsEater)
      }

      //    console.log("email cooker\n", render(ejsCooker));
      //    console.log("\n\nemail eater\n", render(ejsEater));
      //    console.log("sent to cookers: " + emailOptCooker.to, ", sent to eaters: " + emailOptEater.to);

      var sendCallback = function (err, mail) {
        if (err) {
          console.log('error occurred during sending order notification to client email!', err);
        }
      };

      Email.send(emailOptCooker, sendCallback);
      Email.send(emailOptEater, sendCallback);

      if (order.status == 0) {
        // alla ricezione di un nuovo ordine invio un sms al cuoco
        var msg = "Ciao " + order.productor.name + ", hai ricevuto un ordine da " + order.buyer.name + ". Clicca qui per rispondere https://www.ucooki.com/pages/dashboard/orders - Il team di Ucooki.";
        skebby.send({
          recipients: [order.productor.phone],
          text: msg
        }, function (res) {}, function (err) {
          console.error("errore invio sms. ", err);
        });
      }

      if (order.status == 5) {

        // è stato cancellato un ordine da parte del cliente. notifico il cuoco.
        var msg = "Ciao " + order.productor.name + ", purtroppo " + order.buyer.name + " ha cancellato il suo ordine. Clicca qui per maggiori informazioni https://www.ucooki.com/pages/dashboard/orders - Il team di Ucooki.";
        skebby.send({
          recipients: [order.productor.phone],
          text: msg
        }, function (res) {}, function (err) {
          console.error("errore invio sms. ", err);
        });
      }

      if (order.status == 1) {
        // invio un sms al cliente se l'ordine viene accettato
        var msg = "Complimenti " + order.buyer.name + ", " + order.productor.name  + " ha accettato il tuo ordine. Buon appetito :) - Il team di Ucooki.";
        skebby.send({
          recipients: [order.buyer.phone],
          text: msg
        }, function (res) {}, function (err) {
          console.error("errore invio sms. ", err);
        });
      }

      if (order.status == 4 || order.status == 6) {

        // invio un sms al cliente se l'ordine viene rifiutato/cancellato/annullato
        var msg = "Purtroppo " + order.productor.name  + " ha rifiutato il tuo ordine. Clicca qui per maggiori informazioni https://www.ucooki.com/pages/dashboard/orders - Il team di Ucooki";
        skebby.send({
          recipients: [order.buyer.phone],
          text: msg
        }, function (res) {}, function (err) {
          console.error("errore invio sms. ", err);
        });
      }

      // invio della notifica agli admin

      if (order.status != 3) {

        var ejsAdmin = {
          order: order,
          expiration: expiration,
          delivery_date: delivery_date
        }

        var render = loopback.template(path.resolve(__dirname, '../../server/views/notifications/order-admin.ejs'));

        var options = {
          from: "Ucooki <info@ucooki.com>", // sender address
          to: Order.app.get('admin-email'), // list of receivers
          subject: subject, // Subject line
          html: render(ejsAdmin)
        }

        Email.send(options, function (err, mail) {
          if (err) console.log('error occurred during sending email to admin!', err);
        });

      }

    } catch (e) {
      console.error(e);
      console.trace();
    }

  }

};
