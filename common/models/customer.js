var async = require('async');
var env = (process.env.NODE_ENV || 'development');
var path = require('path');
var uuid = require('node-uuid');
var loopback = require('loopback');
var mailchimp = require('../../server/lib/mailchimp.js');
var cloudinary = require('cloudinary');
var utils = require('../../server/lib/utils.js');

module.exports = function (Customer) {

  //**************** MODEL LISTENERS *******************//

  function successfullRegistration(user) {

    if (!user) {
      return;
    }

    var render = loopback.template(path.resolve(__dirname, '../../server/views/welcome.ejs'));
    var html = render({
      user: user
    });
    var subject = "Benvenuto";
    if (user.gender == "female") {
      var subject = "Benvenuta";
    }

    var options = {
      to: user.email,
      from: 'Ucooki <info@ucooki.com>',
      subject: subject,
      text: "Ciao " + user.name + ". Grazie per aver effettuato la registrazione su ucooki. Ora puoi scoprire ed ordinare tutti i migliori piatti dei cooker intorno a te. Buon appetito e a presto! Ucooki Team",
      html: html
    };

    Customer.app.models.Email.send(options, function (err) {
      if (err) return console.log('> error sending welcome email: ', err);
      //      console.log("email sent to " + user.email);
    });

    if (user) {
      user = new Customer(user);
      user.saveOnMailchimp(true);
    }


  }

  Customer.observe('before save', function updateTimestamp(ctx, next) {

    var customer = null;
    // gestione date
    if (ctx.instance) {
      customer = ctx.instance;
      if (ctx.isNewInstance) {
        ctx.instance.created = new Date();
      }
    } else {
      customer = ctx.data;
    }

    if (customer) {
      customer.updated = new Date();
      // elimino admin se presente
      delete customer.admin;
    }

    next();
  });

  Customer.observe('after save', function (ctx, next) {


    // gestione nuove utente
    if (!ctx.instance || !ctx.isNewInstance) {
      // non è una nuova registrazione
      next();
      return;
    }

    var user = ctx.instance;

    if (user && user.provider && user.provider != "local") {
      // lo inviamo direttamente solo se è un utente iscritto da facebook
      successfullRegistration(user);
    }

    next();
  });

  Customer.observe('before delete', function (ctx, next) {

    Customer.find({
      where: ctx.where,
      fields: {
        id: true,
        _picture: true
      }
    }, function (err, users) {
      if (err) {
        console.error(err);
        next(err);
        return;
      }

      if (users && users.length > 0) {
        ctx.hookState.users = users;
      }

      next();
    });

  });

  Customer.observe('after delete', function (ctx, next) {

    if (ctx.hookState.users) {
      ctx.hookState.users.forEach(function (user) {
        cloudinary.uploader.destroy(user._picture.id, function (result) {
          // console.log("Picture deleted for user: ", user, " response: ", result);
        }, {invalidate: true});
      });
    }

    next();

  });

  //**************** ENDPOINT LISTENERS *******************//

  Customer.disableRemoteMethod('prototype.__delete__ratings', true);
  Customer.disableRemoteMethod('prototype.__destroyById__ratings', true);
  Customer.disableRemoteMethod('prototype.__updateById__ratings', true);

  //send password reset link when requested
  Customer.on('resetPasswordRequest', function (info) {

    var render = loopback.template(path.resolve(__dirname, '../../server/views/password-reset.ejs'));

    var html = render({
      accessToken: info.accessToken.id
    });

    Customer.app.models.Email.send({
      to: info.email,
      from: 'Ucooki <info@ucooki.com>',
      subject: 'Recupero della password',
      html: html
    }, function (err) {
      if (err) return console.log('> error sending password reset email: ', err);
    });
  });

  function hideProperties(model) {
    model.__data.code = undefined;
    model.__data.p_iva = undefined;
    model.__data.p_iva_address = undefined;
    model.__data.username = undefined;
    model.__data.emailVerified = undefined;
    model.__data.verificationToken = undefined;
    model.__data.lastUpdated = undefined;
    model.__data.updated = undefined;
    model.__data.phone = undefined;
  }

  //  Customer.beforeRemote('**', function(ctx, user, next) {
  //    console.log(ctx.methodString, 'was invoked remotely'); // customers.prototype.save was invoked remotely
  //    next();
  //  })

  Customer.beforeRemote('prototype.updateAttributes', function (ctx, instance, next) {
    //    console.log("__________________BEFORE: ");
    //    console.log("token: ", ctx.req.accessToken);
    //    console.log("instance: ", ctx.instance);
    //    console.log("data: ", ctx.args.data);

    if (!ctx || !ctx.args || !ctx.args.data) {
      err = new Error('No valid context');
      err.statusCode = 422;
      err.code = 'CONTEXT_ERROR';
      next(err)
      return;
    }

    var data = ctx.args.data;
    // non devo aggiornare i miei ratings
    delete data._ratings;

    if (ctx.instance && ctx.args.data) {
      if (!ctx.instance.__data.cooker && ctx.args.data.cooker) {
        ctx.isNewCooker = true;
      }

      if (!ctx.args.data.cooker && !ctx.instance.__data.cooker) {
        // non è un cuoco, posso eliminare questo dato
        delete data.productor_info;
      }

    }

    next();

  });

  Customer.afterRemote('prototype.updateAttributes', function (ctx, remoteMethodOutput, next) {

    //    console.log("__________________AFTER: ");
    //    console.log("token: ", ctx.req.accessToken);
    //    console.log("instance: ", ctx.instance);
    //    console.log("data: ", ctx.args.data);
    //    console.log("remoteMethodOutput: ", remoteMethodOutput);

    var customer = ctx.result;

    if (!customer) {
      next();
      return;
    }

    var user = ctx.instance.toObject();

    if (user) {
      user = new Customer(user);
      user.saveOnMailchimp(false);
    }


    if (ctx.isNewCooker) {
      // send notification
      sendNewCookerNotification(user);
    }

    next();

  });

  // remote method after hook
  Customer.afterRemote('find', function (ctx, remoteMethodOutput, next) {
    if (ctx.result) {
      if (Array.isArray(ctx.result)) {
        ctx.result.forEach(function (result) {
          hideProperties(result);
        });
      }
    }
    next();
  });

  Customer.afterRemote('findById', function (ctx, remoteMethodOutput, next) {

    var customer = ctx.result;

    if (!customer) {
      next();
      return;
    }

    hideProperties(customer);

    customer.updateRate();

    async.each(customer._ratings, function (rating, done) {

      Customer.findById(rating.productor_id, {
        fields: {
          name: true,
          _picture: true
        }
      }, function (err, customer) {

        if (err) {
          console.error(err);
          done(err);
          return;
        }

        rating.__data.productor = customer.__data;
        done();
      });

    }, function (err) {
      if (err) console.error(err);

      // aggiorno il rating del produttore (se è un produttore)
      customer.updateCookRate(function () {
        next();
      });

    });

  });

  Customer.afterRemote('confirm', function (context, user, next) {

    Customer.findById(context.args.uid, function (err, customer) {
      if (err) {
        console.error("unable to find customer with id: " + context.args.uid+ " after confirm!");
      } else {
        successfullRegistration(customer);
      }
    });

    next();
  });

  Customer.afterRemoteError('confirm', function (context, next) {
    context.res.redirect("/#/");
  });

  //send verification email after registration
  Customer.afterRemote('create', function (context, user, next) {

    if (!user) {
      var error = new Error("Unable to send verification token because user is null");
      next(error);
      return;
    }

    user.sendVerificationToken();
    next();
  });

  Customer.beforeRemote('prototype.__create__ratings', function (ctx, instance, next) {

    //    console.log("token: ", ctx.req.accessToken);
    //    console.log("instance: ", ctx.instance);
    //    console.log("data: ", ctx.args.data);

    if (!ctx.req.accessToken) {
      err = new Error('User not authenticated');
      err.statusCode = 401;
      err.code = 'USER_NOT_AUTHENTICATED';
      next(err);
      return;
    }

    var accessToken = ctx.req.accessToken.__data;
    var rating = ctx.args.data;
    var buyer = ctx.instance.__data;
    var reject = function (message) {
      err = new Error(message);
      err.statusCode = 422;
      err.code = 'REQUEST_ERROR';
      next(err);
    }

    rating.date = new Date();


    Customer.app.models.Order.findById(rating.order_id, function (err, order) {

      if (err) {
        reject(err.message);
        return;
      }

      if (!order) {
        reject("Unable to find order by id");
        return;
      }

      var order = order.__data;
      //      console.log("order: ", order);

      if (order.productor_id.toString() != accessToken.userId.toString()) {
        reject("Cannot publish a rating for someone else");
        return;
      }

      if (order.productor_id.toString() != rating.productor_id.toString()) {
        reject("Incompatible user with rating");
        return;
      }

      if (order.status != 3) {
        reject("Order is not completed");
        return;
      }

      if (order.buyer_id.toString() != buyer.id.toString()) {
        reject("Incompatible product with any order's item");
        return;
      }

      // tutto ok
      next();
    });
  });


  Customer.afterRemote('prototype.__create__ratings', function (ctx, rating, next) {

    //    console.log("token: ", ctx.req.accessToken);
    //    console.log("instance: ", ctx.instance);
    //    console.log("data: ", ctx.args.data);
    //    console.log("rating: ", rating);

    if (!rating || !ctx.instance || !ctx.req.accessToken) {
      next();
      return;
    }

    Customer.findById(ctx.req.accessToken.userId, function (err, productor) {

      if (err) {
        console.error("Error occurred while search productor.", err);
        next();
        return;
      }

      sendRatingNotification(rating, ctx.instance.toObject(), productor);
      next();

    })


  });

  Customer.afterRemote('prototype.__get__products', function (ctx, products, next) {

    //        console.log("token: ", ctx.req.accessToken);
    //        console.log("instance: ", ctx.instance);
    //        console.log("data: ", ctx.args.data);
    //        console.log("products: ", products);

    if (!products) {
      next();
      return;
    }

    products.forEach(function (product) {
      product.updateRate();
    });

    next();

  });

  //**************** ENDPOINT CUSTOM *******************//

  // Override del metodo ereditato
  Customer.logout = function (tokenId, cb) {
    var AccessToken = Customer.app.models.AccessToken;

    AccessToken.findById(tokenId, function (err, instance) {

      if (err) cb(err);

      if (instance) {
        AccessToken.destroyAll({
          userId: instance.userId
        }, function (err, info) {

          if (err) cb(err);

          cb(null, info);
        });
      }
    })
  }

  /******************_____ GetMe _____****************/
  Customer.getMe = function (id, cb) {

    Customer.findById(id, function (err, instance) {

      if (!instance) {
        cb(new Error("Unable to find user with id: ", id));
        return;
      }

      instance.isAdmin(function (isAdmin) {
        if (isAdmin) {
          instance.__data.admin = true;
        }
        cb(null, instance);
      });

    })

  }

  Customer.remoteMethod(
    'getMe', {
      accepts: [{
        arg: 'id',
        type: 'string',
        required: true
      }],
      returns: {
        arg: 'result',
        type: 'Customer'
      },
      http: {
        path: '/:id/me',
        verb: 'get',
        errorStatus: 400,
        status: 200
      },
      description: ["Metodo che permette di ricevere tutte le proprie informazioni senza filtri"]
    }
  );


  /******************_____Delete Customer_____****************/

  Customer.deleteMe = function (id, cb) {

    Customer.findById(id, function (err, instance) {

      if (!instance) {
        cb(new Error("Unable to find user with id: ", id));
        return;
      }

      Customer.app.models.Product.destroyAll({customer_id: id});
      Customer.app.models.CustomerIdentity.destroyAll({userId: id});
      Customer.app.models.AccessToken.destroyAll({userId: id});
      instance.deleteOnMailchimp();
      instance.destroy(function () {
        utils.sendAdminNotification("<h1>Utente Cancellato</h1><br><pre>" + JSON.stringify(instance, null, 4) + "</pre>");
        cb(null, true);
      });

    })

  };

  Customer.remoteMethod(
    'deleteMe', {
      accepts: [{
        arg: 'id',
        type: 'string',
        required: true
      }],
      returns: {
        arg: 'result',
        type: 'boolean'
      },
      http: {
        path: '/:id/me',
        verb: 'delete',
        errorStatus: 400,
        status: 200
      },
      description: ["Cancella definitivamente l'utente e tutto quello che è collegato a lui ad eccezione degli ordini"]
    }
  );

  /******************_____SendVerificationToken_____****************/

  Customer.sendTokenAgain = function(email, cb) {
    Customer.findOne({where : {email: email, verificationToken : {neq: null}}}, function (err, instance) {

      if (!instance) {
        console.log("Requested a new token for a NOT valid email: ", email);
        cb(null, false);
        return;
      }

      instance.sendVerificationToken(function(err, response) {

        if (err) {
          cb(new Error("Error occurred while sending token email to ", email));
          return;
        }

        console.log("Requested a new token for this email: ", email);

        cb(null, true);

      });

    })
  };

  Customer.remoteMethod(
    'sendTokenAgain', {
      accepts: [{
        arg: 'email',
        type: 'string',
        required: true
      }],
      returns: {
        arg: 'result',
        type: 'boolean'
      },
      http: {
        path: '/:email/send-verification',
        verb: 'get',
        errorStatus: 404,
        status: 200
      },
      description: ["Invia nuovamente la mail di verifica"]
    }
  );

  /******************_____ GetSimpleCookList _____****************/


  Customer.getActiveCookList = function (lat, lng, distance, limit, skip, cb) {

    // var distance = 20000
    // lat = lat || 44.49489; // bologna
    // lng = lng || 11.34262; // bologna

    if (!limit) {
      limit = 10;
    }

    if (!skip) {
      skip = 0;
    }

    if (!distance) {
      distance = 5000;
    }

    var customerCollection = Customer.getDataSource().connector.collection(Customer.modelName);

    var pipeline = [];

    if (!!lat && !!lng) {
      var geoNear = {
        $geoNear: {
          spherical: true,
          near: {
            type: "Point",
            coordinates: [lat, lng]
          },
          distanceField: "distance",
          maxDistance: distance
        }
      }

      pipeline.push(geoNear);
    }

    pipeline.push(
      {
        $lookup: {
          "from": "Product",
          "localField": "_id",
          "foreignField": "customer_id",
          "as": "products"
        }
      },
      {
        $project: {
          id: 1,
          name: 1,
          lastname: 1,
          email: 1,
          kitchen_address: 1,
          kitchen_location_info: 1,
          productor_info: 1,
          _picture: 1,
          products: {
            $filter: {
              input: "$products",
              as: "product",
              cond: {
                $and: [
                  {$eq: ["$$product.active", true]},
                  {$eq: ["$$product.status", 1]}
                ]
              }
            }
          }
        }

      },
      {
        $match: {
          "products.0": {"$exists": true}
        }
      }, {
        $project: {
          id: 1,
          name: 1,
          lastname: 1,
          email: 1,
          kitchen_address: 1,
          kitchen_location_info: 1,
          productor_info: 1,
          products: 1,
          _picture: 1,
          "products_size": {$size: "$products"}
        }
      },

      // Stage 6
      {
        $sort: {
          products_size: -1
        }
      }
    );

    var pipelineCount = pipeline.slice(0);
    pipelineCount.push({$group: {_id: null, count: {$sum: 1}}});

    var cursor = customerCollection.aggregate(pipeline).skip(skip).limit(limit);

    var result = {
      total: 0,
      cooks: []
    };

    cursor.each(function (err, doc) {
      if (err) return errorHandler(err, cb);
      if (!doc) {
        cursor.close();
        customerCollection.aggregate(pipelineCount, function (err, res) {
          if (err) return errorHandler(err, cb);
          if (res && res.length > 0) {
            result.total = res[0].count;
          }
          updateData(result.cooks);
        })
      } else {
        result.cooks.push(doc);
      }

    });

    var updateData = function (docs) {
      async.each(docs, function (doc, done) {
        // aggiorniamo la struttura dei singoli documenti (cuochi)
        doc.productor_info.product_count = doc.products_size;
        delete doc.products_size;

        var customer = new Customer(doc);
        customer.updateCookRate(function () {
          doc.productor_info.rate = customer.__data.productor_info.rate;
          done();
        })
      }, function (err) {
        if (err) return errorHandler(err, cb);
        cb(null, result);
      });
    }
  }

  Customer.remoteMethod(
    'getActiveCookList', {
      accepts: [{
        arg: 'lat',
        type: 'number'
      }, {
        arg: 'lng',
        type: 'number'
      }, {
        arg: 'distance',
        type: 'number',
        description: "Distanza massima di ricerca. Il valore di default è 5000 metri. Il valore massimo è 10000 metri."
      }, {
        arg: 'limit',
        type: 'number'
      }, {
        arg: 'skip',
        type: 'number'
      }],
      returns: {
        arg: 'result',
        type: 'Customer'
      },
      http: {
        path: '/active-cooks',
        verb: 'get',
        errorStatus: 400,
        status: 200
      },
      description: ["Recupera tutti i cooker che sono nelle vicinanze"]
    }
  );


  /******************_____ GetList _____****************/

  Customer.getCookList = function (certificated, text, lat, lng, distance, limit, skip, cb) {

    if (!limit) {
      limit = 10;
    }

    if (!skip) {
      skip = 0;
    }

    if (!distance) {
      distance = 5000;
    }

    var pipeline = [];

    if (!!lat && !!lng) {
      var geoNear = {
        $geoNear: {
          spherical: true,
          near: {
            type: "Point",
            coordinates: [lat, lng]
          },
          distanceField: "distance",
          maxDistance: distance
        }
      }

      pipeline.push(geoNear);
    }

    pipeline.push(
      // Stage 2
      {
        $lookup: {
          "from": "Product",
          "localField": "_id",
          "foreignField": "customer_id",
          "as": "products"
        }
      },

      // Stage 3
      {
        $match: {
          cooker: true
        }
      },

      // Stage 4
      {
        $project: {
          id: 1,
          name: 1,
          lastname: 1,
          fullname: {$concat: ["$name", " ", "$lastname"]},
          email: 1,
          kitchen_address: 1,
          kitchen_location_info: 1,
          productor_info: 1,
          _picture: 1,
          products: 1,
          productor_info: 1

        }
      },

      // Stage 5
      {
        $sort: {
          name: 1
        }
      });

    var filter = {$match: {}};

    if (certificated >= 0) {
      var cert = certificated == 0 ? false : true;
      filter.$match["productor_info.certificated"] = cert;
    }

    if (text) {
      var regex = utils.stringToRegex(text);
      filter.$match["$or"] = [{"fullname": regex}, {"email": regex}];
    }

    pipeline.push(filter);

    var customerCollection = Customer.getDataSource().connector.collection(Customer.modelName);

    var pipelineCount = pipeline.slice(0);
    pipelineCount.push({$group: {_id: null, count: {$sum: 1}}});

    var cursor = customerCollection.aggregate(pipeline).skip(skip).limit(limit);

    var result = {
      total: 0,
      cooks: []
    };

    cursor.each(function (err, doc) {
      if (err) return errorHandler(err, cb);
      if (!doc) {
        cursor.close();
        customerCollection.aggregate(pipelineCount, function (err, res) {
          if (err) return errorHandler(err, cb);
          if (res && res.length > 0) {
            result.total = res[0].count;
          }
          cb(null, result);
          return;
        })
      } else {
        result.cooks.push(doc);
      }

    });

  }

  Customer.remoteMethod(
    'getCookList', {
      accepts: [{
        arg: 'certificated',
        type: 'number'
      }, {
        arg: 'text',
        type: 'string',
        description: "Testo da filtrare sul nome, cognome o sull'email del cuoco"
      }, {
        arg: 'lat',
        type: 'number'
      }, {
        arg: 'lng',
        type: 'number'
      }, {
        arg: 'distance',
        type: 'number',
        description: "Distanza massima di ricerca. Il valore di default è 5000 metri. Il valore massimo è 10000 metri."
      }, {
        arg: 'limit',
        type: 'number'
      }, {
        arg: 'skip',
        type: 'number'
      }],
      returns: {
        arg: 'result',
        type: 'object'
      },
      http: {
        path: '/cooks',
        verb: 'get',
        errorStatus: 400,
        status: 200
      },
      description: ["Metodo che permette di ricevere tutti i cuochi registrati"]
    }
  );

  //**************** Prototype *******************//

  Customer.prototype.updateRate = function () {
    var ratings = this.__data._ratings;
    var ratingScore = 0;
    ratings.forEach(function (rating) {
      ratingScore += rating.rate;
    });

    if (ratings.length) {
      ratingScore = ratingScore / ratings.length;
    }
    this.__data.rate = ratingScore;
  }

  Customer.prototype.updateCookRate = function (cb) {

    function _updateRate(products) {
      var ratingScore = 0;
      var ratingTotal = 0;
      products.forEach(function (product) {
        product.updateRate();
        if (product.rate > 0) {
          ratingScore += product.rate;
          ratingTotal++;
        }
      });

      if (ratingTotal > 0) {
        ratingScore = ratingScore / ratingTotal;
      }

      if (ratingScore > 0) {
        customer.__data.productor_info.rate = ratingScore;
      }

      cb();
    }

    cb = cb || function () {
      };
    var customer = this;


    if (!customer.__data || !customer.__data.productor_info) {
      // non è un cooker
      cb();
      return;
    }

    if (customer.__cachedRelations && customer.__cachedRelations.products) {
      // ha già i prodotti caricati, non serve ricaricarli
      var products = [];
      customer.__cachedRelations.products.forEach(function (product) {
        products.push(new Customer.app.models.Product(product));
      });
      _updateRate(products);
    } else {

      this.products({
        where: {
          active: true,
          status: 1
        }
      }, function (err, products) {
        if (err) {
          console.error("error:", err);
          cb(err);
          return;
        }

        _updateRate(products);

      })
    }
  }

  Customer.prototype.isAdmin = function (cb) {

    var Role = Customer.app.models.Role;
    var RoleMapping = Customer.app.models.RoleMapping;

    Role.isInRole('admin', {principalType: RoleMapping.USER, principalId: this.id}, function (err, isInRole) {
      cb(isInRole);
    });
  }

  Customer.prototype.saveOnMailchimp = function (isNew, callback) {

    if (!callback || typeof(callback) !== "function") {
      callback = function () {
      };
    }

    if (env !== "production") {
      console.log("ignore SAVE mailchimp on not production. User: " + this.email);
      return;
    }

    var mpArgs = {
      data: {
        email_address: this.email,
        status: "subscribed",
        merge_fields: {
          EMAIL: this.email,
          FNAME: this.name,
          LNAME: this.lastname,
          GENDER: this.gender,
          COOKER: "" + this.cooker,
          PROVIDER: this.provider,
          CITY: this.city
        }
      },
      path: {
        "list_id": mailchimp.customerList,
        "email": this.email
      },
      headers: {
        "Content-Type": "application/json"
      }
    };

    var cb = function (code, message) {
      console.log("Error occurred during mailchimp registration.\nCode: ", code, "Message: ", message);
    }

    if (isNew) {
      mailchimp.addSubscriber(mpArgs, function (data) {
        // console.log("mailchimp add response: ", data);
        callback(data);
      }, cb);
      // console.log("saved: ", mpArgs);
    } else {
      mailchimp.updateSubscriber(mpArgs, function (data) {
        // console.log("mailchimp update response: ", data);
        callback(data);
      }, cb);
      // console.log("updated: ", mpArgs);
    }

  };

  Customer.prototype.deleteOnMailchimp = function (callback) {

    if (!callback || typeof(callback) !== "function") {
      callback = function () {
      };
    }

    if (env !== "production") {
      console.log("ignore DELETE mailchimp on not production. User: " + this.email);
      return;
    }

    var mpArgs = {
      path: {
        "list_id": mailchimp.customerList,
        "email": this.email
      },
      headers: {
        "Content-Type": "application/json"
      }
    };

    mailchimp.deleteSubscriber(mpArgs, function (data) {
      callback(data);
    }, function (code, message) {
      console.log("Error occurred during mailchimp deletion.\nCode: ", code, "Message: ", message);
    });

  };

  Customer.prototype.sendVerificationToken = function(callback) {

    var user = this;
    callback = callback || function() {};

    var options = {
      type: 'email',
      to: user.email,
      from: 'Ucooki <info@ucooki.com>',
      subject: 'Richiesta registrazione.',
      template: path.resolve(__dirname, '../../server/views/verify.ejs'),
      redirect: '/',
      user: user,
      host: "www.ucooki.com",
      port: 80
    };

    user.verify(options, function (err, response) {
      if (err) {
        console.error("Unable to send verification token email", err);
        callback(err);
        return;
      }

      callback(null, response);

    });
  };

  //**************** Utility *******************//

  var errorHandler = function (err, cb) {
    if (err) {
      console.error("Error customer endpoint: ", err);
      cb(err);
    }
  };


  // funzione di mapping con il profilo scaricato da passport
  Customer.mapProfileToUser = function (provider, profile) {

    //    console.log("profile: ", profile);

    var email = profile.emails && profile.emails[0] && profile.emails[0].value;
    var gender = profile.gender;
    var username = provider + "_" + profile.id;
    var password = uuid.v4();
    var pictureUrl = "https://graph.facebook.com/v2.5/" + profile.id + "/picture?type=large";

    var userObj = {
      username: username,
      password: password,
      name: profile._json.first_name,
      lastname: profile._json.last_name,
      gender: gender,
      created: new Date(),
      updated: new Date(),
      emailVerified: true,
      provider: provider,
      _picture: {
        id: pictureUrl,
        url: pictureUrl,
        principal: true,
        status: 1
      }
    };

    if (email) {
      userObj.email = email;
      userObj.username = email;
    }

    return userObj;
  }

  var sendRatingNotification = function (rating, buyer, producer) {

    if (!rating || !buyer || !producer) {
      return;
    }

    try {

      var Email = Customer.app.models.Email;
      var subjectBuyer = "Commento Ricevuto";
      var subjectProductor = "Commento Inviato";
      var render = loopback.template(path.resolve(__dirname, '../../server/views/notifications/customer-rating.ejs'));

      var htmlBuyer = render({
        subject: subjectBuyer,
        rating: rating,
        buyer: buyer,
        productor: producer,
        destination: 'buyer'
      });

      var htmlProductor = render({
        subject: subjectProductor,
        rating: rating,
        buyer: buyer,
        productor: producer,
        destination: 'productor'
      });

      var emailOptBuyer = {
        to: buyer.email,
        from: 'Ucooki <info@ucooki.com>',
        subject: subjectBuyer,
        html: htmlBuyer
      };

      var emailOptProductor = {
        to: producer.email,
        from: 'Ucooki <info@ucooki.com>',
        subject: subjectProductor,
        html: htmlProductor
      };

      //    console.log("sent to buyer:\n\n " + htmlBuyer, "\n\n, sent to productor:\n\n " + htmlProductor);

      var sendCallback = function (err, mail) {
        if (err) {
          console.log('error occurred during sending rating client email!', err);
          return;
        }
      }

      Email.send(emailOptBuyer, sendCallback);
      Email.send(emailOptProductor, sendCallback);


      // invio della notifica agli admin

      Email.send({
        to: Customer.app.get('admin-email'),
        from: "info@ucooki.com",
        subject: "Nuovo Commento inviato",
        html: "<h2>Nuovo Commento per un cliente</h2><p>Il cuoco: <b>" + producer.name + " " + producer.lastname + "</b> ha votato il suo cliente: <b>" + buyer.name + " " + buyer.lastname + "</b></p><p>Voto: <b>" + rating.__data.rate + "</b>/5</p><p>Commento: <b>" + rating.message + "</b></p>" // html body
      }, sendCallback);

    } catch (e) {
      console.error(e);
      console.trace();
    }
  }

  var sendNewCookerNotification = function (cooker) {

    if (!cooker) {
      return;
    }

    try {

      var Email = Customer.app.models.Email;
      var render = loopback.template(path.resolve(__dirname, '../../server/views/welcome-cooker.ejs'));

      var html = render({
        user: cooker
      });

      //    console.log("sent to cooker:\n\n " + html);

      var sendCallback = function (err, mail) {
        if (err) {
          console.log('error occurred during sending new cook mail to client email!', err);
          return;
        }
      };

      Email.send({
        to: cooker.email,
        from: 'Ucooki <info@ucooki.com>',
        subject: "Attivato profilo Cuoco",
        html: html
      }, sendCallback);

      // send notification to admin

      // Email.send({
      //   to: Customer.app.get('admin-email'),
      //   from: "info@ucooki.com",
      //   subject: "Nuovo Cooker registrato",
      //   html: "<h2>Nuovo Cooker</h2><pre>" + JSON.stringify(cooker, null, 4) + "</pre>" // html body
      // }, sendCallback);

    } catch (e) {
      console.error(e);
      console.trace();
    }
  };

};
