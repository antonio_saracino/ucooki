var mailchimp = require('../../server/lib/mailchimp.js');
var loopback = require('loopback');
var path = require('path');

module.exports = function(Subscriber) {

  Subscriber.validatesUniquenessOf('email');

  Subscriber.validatesFormatOf('email', {
    with: /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i,
    message: "Email incorrect"
  });

  Subscriber.afterRemote('create', function(context, remoteMethodOutput, next) {
    var data = context.result.__data;

    data.lastname = "";

    var names = data.name.split(" ");
    if (names.length > 1) {
      data.name = names[0];
      data.lastname = names[1];
    }

    data.name = data.name.charAt(0).toUpperCase() + data.name.slice(1);
    if (data.lastname !== "") {
      data.lastname = data.lastname.charAt(0).toUpperCase() + data.lastname.slice(1);
    }

		// invio Benvenuto
    var render = loopback.template(path.resolve(__dirname, '../../server/views/welcome-subscriber.ejs'));
    var html = render({
      user: data
    });

    options = {
      to: data.email,
      from: 'Ucooki <info@ucooki.com>',
      subject: 'Registrazione avvenuta con successo',
      html: html
    };

    Subscriber.app.models.Email.send(options, function(err, mail) {
      if (err) console.log('error occurred during sending registration notification to client email!', err);
    });

    // aggiunta all'interno di mailchimp
    var mpArgs = {
      data: {
        email_address: data.email,
        status: "subscribed",
        merge_fields: {
          EMAIL: data.email,
          FNAME: data.name,
          LNAME: data.lastname,
          CITY: data.city,
					PROVINCE: data.province,
          COOKER: "" + data.cooker
        }
      },
      path: {
        "list_id": "52edf014c7"
      },
      headers: {
        "Content-Type": "application/json"
      }
    };

    mailchimp.addSubscriber(mpArgs, function(data) {
//				console.log("mailchimp add response: ", data);
			}, function(code, message) {
      console.log("Error occurred during mailchimp registration.\nCode: ", code, "Message: ", message);
    });

    next();
  });

};
